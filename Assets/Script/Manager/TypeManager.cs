﻿using System.Collections.Generic;

using UnityEngine;

public class TypeManager : ManagerParent
{
	#region Variables
	public objTypeInf [ ] AllObjT;
	Dictionary<PhysicMaterial, ElementType> AllObject;

	[System.Serializable]
	public struct objTypeInf
	{
		public ElementType GroupeType;
		public PhysicMaterial [ ] ThisObj;
	}
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public void UpdateType (PhysicMaterial ThisObjectID, ElementType NewType)
	{
		ElementType thisType;
		if (AllObject.TryGetValue (ThisObjectID, out thisType))
		{
			AllObject [key : ThisObjectID] = NewType;
		}
		else
		{
			AllObject.Add (ThisObjectID, NewType);
		}
	}

	public ElementType GetTypeObj (PhysicMaterial ThisObjectID)
	{
		ElementType thisType;
		if (AllObject.TryGetValue (ThisObjectID, out thisType))
		{
			return thisType;
		}

		return ElementType.Nothing;
	}
	#endregion

	#region Private Methodes
	protected override void InitializeManager ( )
	{
		int getLength = AllObjT.Length;
		int cal = 0;

		for (int a = 0; a < getLength; a++)
		{
			cal += AllObjT [a].ThisObj.Length;
		}

		AllObject = new Dictionary<PhysicMaterial, ElementType> (cal);
		PhysicMaterial [ ] getObjects;
		ElementType thisType;

		int getLength2;
		int b;
		for (int a = 0; a < getLength; a++)
		{
			getObjects = AllObjT [a].ThisObj;
			getLength2 = getObjects.Length;

			for (b = 0; b < getLength2; b++)
			{
				if (!AllObject.TryGetValue (getObjects [b], out thisType))
				{
					AllObject.Add (getObjects [b], AllObjT [a].GroupeType);
				}
				else if (thisType != AllObjT [a].GroupeType)
				{
					Debug.LogError ("Same objects with multiple Type : " + AllObjT [a].GroupeType);
				}
			}
		}

		AllObjT = null;
	}
	#endregion
}
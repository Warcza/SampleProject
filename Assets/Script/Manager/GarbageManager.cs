﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class GarbageManager : ManagerParent
{
	#region Variables
	Dictionary<Type, IList> garbageList;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public void AddGarbageClass<T> (T thisClass)
	{
		List<T> getList = getListGarbage<T> (thisClass.GetType ( ));
		getList.Add (thisClass);
	}

	public T GetGarbaClass<T> (Type thisType)
	{
		List<T> getList = getListGarbage<T> (thisType);
		if (getList.Count > 0)
		{
			T getLast = getList [getList.Count - 1];
			getList.RemoveAt (getList.Count - 1);
			return getLast;
		}

		return (T)Activator.CreateInstance (thisType);;
	}

	#region Interface Create Array element
	public List<T> ReInitList<T> (Type thisType)
	{
		List<T> thisList = getListGarbage<T> (thisType);
		return thisList;
	}

	public List<T> ReInitList<T> (Type thisType, IList<T> copy)
	{
		List<T> thisList = getListGarbage<T> (thisType, copy);
		return thisList;
	}

	public List<T> ReInitList<T> (Type thisType, int length)
	{
		List<T> thisList = getListGarbage<T> (thisType, length);
		return thisList;
	}

	public Dictionary<T, T2> ReInitDict<T, T2> (Type thisType)
	{
		Dictionary<T, T2> thisDictionnary = getDictionnaryGarbage<T, T2> (thisType);
		return thisDictionnary;
	}
	#endregion
	#endregion

	#region Private Methodes
	#region ListGarbage
	List<T> getListGarbage<T> (Type thisType)
	{
		IList getList;
		List<T> saveList;
		if (!garbageList.TryGetValue (thisType, out getList))
		{
			saveList = new List<T> ( );
			getList = new List<List<T>> ( );
			garbageList.Add (thisType, getList);
		}
		else if (getList.Count > 0)
		{
			saveList = (List<T>)getList [getList.Count - 1];
			getList.RemoveAt (getList.Count - 1);
			saveList.Clear ( );
		}
		else
		{
			saveList = new List<T> ( );
		}

		return saveList;
	}

	List<T> getListGarbage<T> (Type thisType, IList<T> copy)
	{
		IList getList;
		List<T> saveList;
		if (!garbageList.TryGetValue (thisType, out getList))
		{
			saveList = new List<T> (copy);
			getList = new List<List<T>> ( );
			garbageList.Add (thisType, getList);
		}
		else if (getList.Count > 0)
		{
			saveList = (List<T>)getList [getList.Count - 1];
			getList.RemoveAt (getList.Count - 1);
			saveList.Clear ( );
			saveList.AddRange (copy);
		}
		else
		{
			saveList = new List<T> (copy);
		}

		return saveList;
	}

	List<T> getListGarbage<T> (Type thisType, int length)
	{
		IList getList;
		List<T> saveList;
		if (!garbageList.TryGetValue (thisType, out getList))
		{
			saveList = new List<T> (length);
			getList = new List<List<T>> ( );
			garbageList.Add (thisType, getList);
		}
		else if (getList.Count > 0)
		{
			saveList = (List<T>)getList [getList.Count - 1];
			getList.RemoveAt (getList.Count - 1);
			saveList.Clear ( );
			saveList.Capacity = length;
		}
		else
		{
			saveList = new List<T> (length);
		}

		return saveList;
	}
	#endregion

	#region DictionnaryGarbage
	Dictionary<T, T2> getDictionnaryGarbage<T, T2> (Type thisType)
	{
		IList getList;
		Dictionary<T, T2> saveDict;
		if (!garbageList.TryGetValue (thisType, out getList))
		{
			saveDict = new Dictionary<T, T2> ( );
			getList = new List<Dictionary<T, T2>> ( );
			garbageList.Add (thisType, getList);
		}
		else if (getList.Count > 0)
		{
			saveDict = (Dictionary<T, T2>)getList [getList.Count - 1];
			getList.RemoveAt (getList.Count - 1);
			saveDict.Clear ( );
		}
		else
		{
			saveDict = new Dictionary<T, T2> ( );
		}

		return saveDict;
	}
	#endregion

	protected override void InitializeManager ( )
	{
		garbageList = new Dictionary<Type, IList> ( );
	}
	#endregion
}
﻿using UnityEngine;
using UnityEngine.UI;
public class Manager : MonoBehaviour
{
	#region Variables
	static Manager mainManagerInstance;
	//Add new managers here
	static EventManager evnt;
	public static EventManager Event
	{
		get
		{
			return evnt;
		}
	}

	static ScnManager scene;
	public static ScnManager Scene
	{
		get
		{
			return scene;
		}
	}

	static GameManger game;
	public static GameManger Game
	{
		get
		{
			return game;
		}
	}

	static UiManager ui;
	public static UiManager UI
	{
		get
		{
			return ui;
		}
	}

	static AudioManager aud;
	public static AudioManager Aud
	{
		get
		{
			return aud;
		}
	}

	static JobsManager job;
	public static JobsManager Job
	{
		get
		{
			return job;
		}
	}

	static TypeManager type;
	public static TypeManager Type
	{
		get
		{
			return type;
		}
	}

	static GarbageManager garbage;
	public static GarbageManager Garbage
	{
		get
		{
			return garbage;
		}
	}

	static TimeManager time;
	public static TimeManager Time
	{
		get
		{
			return time;
		}
	}

	static AsyncManager async;
	public static AsyncManager Async
	{
		get
		{
			return async;
		}
	}

	static PrefabManager prefab;
	public static PrefabManager Prefab
	{
		get
		{
			return prefab;
		}
	}
	#endregion

	#region Mono
	void Awake ( )
	{
		PlayerPrefs.DeleteAll ( );

		//Cursor.lockState = CursorLockMode.Locked;
		Application.targetFrameRate = 60;

		//Keep manager a singleton
		if (mainManagerInstance != null)
		{
			Destroy (gameObject);
		}
		else
		{
			DontDestroyOnLoad (gameObject);
			mainManagerInstance = this;
			InitializeManagers ( );
		}
	}
	#endregion

	#region Public Methods

	#endregion

	#region Private Methods
	void InitializeManagers ( )
	{
		InitializeManager (ref garbage);
		InitializeManager (ref time);
		InitializeManager (ref evnt);
		InitializeManager (ref async);
		InitializeManager (ref job);
		InitializeManager (ref scene);
		InitializeManager (ref type);
		InitializeManager (ref prefab);

		InitializeManager (ref ui);
		InitializeManager (ref game);
		InitializeManager (ref aud);
	}

	void InitializeManager<T> (ref T manager)where T : ManagerParent
	{
		Debug.Log ("Initializing managers");
		T [ ] managers = GetComponentsInChildren<T> ( );

		if (managers.Length == 0)
		{
			Debug.LogError ("No manager of type: " + typeof (T) + " found.");
			return;
		}

		//Set to first manager
		manager = managers [0];
		manager.Initialize ( );

		if (managers.Length > 1) //Too many managers
		{
			Debug.LogError ("Found " + managers.Length + " managers of type " + typeof (T));
			for (int i = 1; i < managers.Length; i++)
			{
				Destroy (managers [i].gameObject);
			}
		}
	}
	#endregion
}
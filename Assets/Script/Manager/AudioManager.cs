﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;

public class AudioManager : ManagerParent
{
	#region Variable
	public MusicFX [ ] AllMF;
	AllInfo [ ] AllMusics;

	Dictionary<int, AudioObject> audComp;
	List<sourceInfo> ableSource;
	List<AllAudio> currCatAllAudio;

	CustomString currCatName;

	[System.Serializable]
	class AllInfo
	{
		public float VolumeAudio;
		public Dictionary<int, Dictionary<int, AllAudio>> MusicTarget;

		public List<sourceInfo> MusicRunning;
		public GameObject audioParent;
	}

	[System.Serializable]
	struct sourceInfo
	{
		public CancellationTokenSource CancelToke;
		public AudioSource ThisAudio;
		public bool ForceDest;
	}
	#endregion

	#region Mono
	#endregion

	#region Public
	#region InterfaceAudio
	public AudioSource UseAudioFromObject (int idObject, AudioPlaying typeAud, bool playAudio)
	{

		AudioObject getAud;
		if (audComp.TryGetValue (idObject, out getAud))
		{
			if (playAudio)
			{
				return getAud.PlayeAudio (typeAud);
			}
			else
			{
				getAud.StopAudio (typeAud);
			}
		}
		else
		{
			Debug.LogError ("No audio found for : " + typeAud);
		}

		return null;
	}

	public void AddAudioObject (int idObject, AudioObject thisAud)
	{
		audComp.Add (idObject, thisAud);
	}

	public void RemoveAudioObject (int idObject)
	{
		audComp.Remove (idObject);
	}
	#endregion

	#region AudioSystem
	public AudioSource OpenAudio (AudioToken ThisTok, Transform onTarget = null)
	{
		AudioType thisType = ThisTok.ThisType;
		AllInfo thisInfo;

		thisInfo = AllMusics [(int)thisType];

		CustomString categoriName = ThisTok.CategorieName;
		Dictionary<int, AllAudio> getAudio;

		if (!thisInfo.MusicTarget.TryGetValue (categoriName.StringValue, out getAudio))
		{
			return null;
		}

		AllAudio thisAudio;
		CustomString thisName = ThisTok.ThisName;
		if (thisName.StringValue == 0)
		{
			List<int> getKeys = Manager.Garbage.ReInitList<int> (typeof (int));
			getKeys.AddRange (getAudio.Keys);
			thisName.StringValue = getKeys [Random.Range (0, getKeys.Count)];
			Manager.Garbage.AddGarbageClass (getKeys);
		}

		if (!getAudio.TryGetValue (thisName.StringValue, out thisAudio))
		{
			return null;
		}

		System.Action thisAct = ThisTok.ThisAct;
		AudioAction audioAct = ThisTok.AudioAct;

		bool loopAudio = thisAudio.AudioLoop;
		int getLenght;

		if (audioAct != AudioAction.Nothing)
		{
			sourceInfo [ ] getAC = thisInfo.MusicRunning.ToArray ( );
			getLenght = getAC.Length;
			bool checkSameAud;

			for (int a = 0; a < getLenght; a++)
			{
				checkSameAud = false;
				if (getAC [a].ThisAudio.enabled)
				{
					int lengthAudio = thisAudio.Audio.Length;

					for (int b = 0; b < lengthAudio; b++)
					{
						if (thisAudio.Audio [b].name != getAC [a].ThisAudio.clip.name)
						{
							continue;
						}

						if (audioAct == AudioAction.PlayIfEmpty)
						{
							return null;
						}
						else if (audioAct == AudioAction.Renew)
						{
							CloseThisAudio (thisType, categoriName, thisAudio.AudioName);
							break;
						}

						checkSameAud = true;
					}
				}

				if (checkSameAud)
				{
					break;
				}
			}
		}

		AudioSource getAud;
#if UNITY_EDITOR
		if (onTarget != null)
		{
			getAud = onTarget.gameObject.AddComponent<AudioSource> ( );
		}
		else
		{
			getAud = thisInfo.audioParent.AddComponent<AudioSource> ( );
		}
#else

		if (onTarget != null)
		{
			getAud = onTarget.gameObject.AddComponent<AudioSource> ( );
		}
		else if (ableSource.Count > 0)
		{
			getAud = ableSource [ableSource.Count - 1].ThisAudio;
			ableSource.RemoveAt (ableSource.Count - 1);
			getAud.enabled = true;
		}
		else
		{
			getAud = thisInfo.audioParent.AddComponent<AudioSource> ( );
		}
#endif

		getAud.volume = thisAudio.Volume * thisInfo.VolumeAudio;
		getAud.pitch = thisAudio.Pitch;
		getAud.clip = thisAudio.Audio [Random.Range (0, thisAudio.Audio.Length)];
		getAud.loop = loopAudio;
		getAud.spatialBlend = 0.5f;
		getAud.enabled = true;

		sourceInfo getSource = new sourceInfo ( );
		getSource.ThisAudio = getAud;
		getSource.ForceDest = onTarget != null;
		getSource.CancelToke = new CancellationTokenSource ( );
		thisInfo.MusicRunning.Add (getSource);

		if (thisAudio.StartPlay)
		{
			getAud.Play ( );
		}

		if (!loopAudio)
		{
			waitEndAudio (getAud.clip.length, thisInfo, getSource, thisAct);
		}
		return getAud;
	}

	public AudioSource GetSource (AudioType thisType, CustomString categorieName, CustomString thisName, bool closeAudio = false)
	{
		AllInfo thisInfo = AllMusics [(int)thisType];

		Dictionary<int, AllAudio> getAudio;
		if (!thisInfo.MusicTarget.TryGetValue (categorieName.StringValue, out getAudio))
		{
			return null;
		}

		AllAudio thisAudio;
		if (!getAudio.TryGetValue (thisName.StringValue, out thisAudio))
		{
			return null;
		}

		sourceInfo [ ] allSource = thisInfo.MusicRunning.ToArray ( );
		int getLenght = allSource.Length;

		for (int a = 0; a < getLenght; a++)
		{
			if (allSource [a].ThisAudio.enabled && CustomString.GetHashFromString (allSource [a].ThisAudio.clip.name) == thisName.StringValue)
			{
				return allSource [a].ThisAudio;
			}
		}
		return null;
	}

	public void CloseThisAudio (AudioType thisType, CustomString categorieName, CustomString thisName)
	{
		AudioSource thisSource = GetSource (thisType, categorieName, thisName);
		if (thisSource != null)
		{
			AllInfo thisInfo = AllMusics [(int)thisType];

			int getLentgh = thisInfo.MusicRunning.Count;
			for (int a = 0; a < getLentgh; a++)
			{
				if (thisInfo.MusicRunning [a].ThisAudio == thisSource)
				{
					thisInfo.MusicRunning.RemoveAt (a);

					if (thisInfo.MusicRunning [a].CancelToke != null)
					{
						thisInfo.MusicRunning [a].CancelToke.Cancel ( );
						thisInfo.MusicRunning [a].CancelToke.Dispose ( );
					}

#if UNITY_EDITOR
					Destroy (thisSource);

#else
					if (thisInfo.MusicRunning [a].ForceDest)
					{
						Destroy (thisSource);
					}
					else
					{
						sourceInfo thisSourceinfo = new sourceInfo ( );
						thisSourceinfo.ForceDest = false;
						thisSourceinfo.ThisAudio = thisSource;
						ableSource.Add (thisSourceinfo);
						thisSourceinfo.ThisAudio.clip = null;
						thisSourceinfo.ThisAudio.enabled = false;
					}
#endif

					break;
				}
			}
		}
	}

	public void CloseAudio (AudioType thisType)
	{
		AllInfo thisInfo = AllMusics [(int)thisType];

		sourceInfo [ ] allSource = thisInfo.MusicRunning.ToArray ( );
		int getLenght = allSource.Length;

		for (int a = 0; a < getLenght; a++)
		{
			if (thisInfo.MusicRunning [a].CancelToke != null)
			{
				thisInfo.MusicRunning [a].CancelToke.Cancel ( );
			}

#if UNITY_EDITOR
			Destroy (allSource [a].ThisAudio);

#else	
			allSource [a].ThisAudio.clip = null;
			allSource [a].ThisAudio.enabled = false;
			ableSource.Add (allSource [a]);
#endif
		}

		thisInfo.MusicRunning.Clear ( );
	}

	public void CloseAllAudio ( )
	{
		AudioType [ ] getTypes = (AudioType [ ])System.Enum.GetValues (typeof (AudioType));
		for (int a = 0; a < getTypes.Length; a++)
		{
			CloseAudio (getTypes [a]);
		}
	}

	public CustomString GetRandomMusic (AudioToken ThisTok, CustomString [ ] musicsUsed)
	{
		AudioType thisType = ThisTok.ThisType;

		AllInfo thisInfo = AllMusics [(int)thisType];

		CustomString categoriName = ThisTok.CategorieName;
		Dictionary<int, AllAudio> getAudio;

		if (!thisInfo.MusicTarget.TryGetValue (categoriName.StringValue, out getAudio) || getAudio.Count == 0)
		{
			return new CustomString ( );
		}

		if (currCatName.StringValue != categoriName.StringValue)
		{
			currCatName = categoriName;

			currCatAllAudio.Clear ( );
			List<AllAudio> setAudio = currCatAllAudio;
			foreach (var keyValue in getAudio)
			{
				setAudio.Add (keyValue.Value);
			}
		}

		int getLenght = musicsUsed.Length;
		int lengthAudio = currCatAllAudio.Count;

		if (getLenght > 0 && getLenght < lengthAudio)
		{
			List<AllAudio> newList = Manager.Garbage.ReInitList<AllAudio> (typeof (AllAudio), currCatAllAudio);

			int b;
			for (int a = 0; a < getLenght; a++)
			{
				for (b = lengthAudio; b > 0; b--)
				{
					if (musicsUsed [a].StringValue == newList [b].AudioName.StringValue)
					{
						newList.RemoveAt (b);
						break;
					}
				}
			}

			return newList [Random.Range (0, newList.Count - 1)].AudioName;
		}

		return currCatAllAudio [Random.Range (0, currCatAllAudio.Count - 1)].AudioName;
	}
	#endregion
	#endregion

	#region Private
	protected override void InitializeManager ( )
	{
		Object [ ] getAssets = Resources.LoadAll ("Scriptable/Audio", typeof (AudioScriptable));
		AudioScriptable thisAudScript;
		List<MusicFX> getAllMusicF = new List<MusicFX> ( );
		audComp = new Dictionary<int, AudioObject> ( );

		for (int a = 0; a < getAssets.Length; a++)
		{
			thisAudScript = (getAssets [a] as AudioScriptable);
			getAllMusicF.Add (thisAudScript.ThisMusic);
		}

		AllMF = getAllMusicF.ToArray ( );

		AudioType [ ] getTypes = (AudioType [ ])System.Enum.GetValues (typeof (AudioType));
		currCatAllAudio = new List<AllAudio> ( );
		AudioType getAud;

		AllInfo [ ] setMusicTarget = new AllInfo [getTypes.Length];
		AllInfo thisInfo = new AllInfo ( );

		Transform currT = transform;

		ableSource = new List<sourceInfo> ( );

		Transform thisPar = transform;
		GameObject currObj;

		for (int a = 0; a < getTypes.Length; a++)
		{
			thisInfo = new AllInfo ( );
			thisInfo.MusicRunning = new List<sourceInfo> ( );

			getAud = getTypes [a];
			currObj = new GameObject ( );
			currObj.name = getAud.ToString ( );
			currObj.transform.SetParent (thisPar);

			thisInfo.audioParent = currObj;
			thisInfo.MusicTarget = addAllMusic (getAud);
			thisInfo.VolumeAudio = AllPlayerPrefs.GetFloatValue (getTypes [a].ToString ( ), 1);

			setMusicTarget [a] = thisInfo;
		}

		AllMusics = setMusicTarget;
		AllMF = null;
	}

	Dictionary<int, Dictionary<int, AllAudio>> addAllMusic (AudioType thisType)
	{
		Dictionary<int, Dictionary<int, AllAudio>> SetAud = Manager.Garbage.ReInitDict<int, Dictionary<int, AllAudio>> (typeof (Dictionary<int, Dictionary<int, AllAudio>>));
		Dictionary<int, AllAudio> thisAudio;

		MusicFX thisMusic;
		AllAudio value;

		int getLenghtB;
		int getLenghtC;
		int b;
		int c;

		getLenghtB = AllMF.Length;

		for (b = 0; b < getLenghtB; b++)
		{
			thisMusic = AllMF [b];

			if (thisMusic.ThisType == thisType)
			{
				if (SetAud.TryGetValue (thisMusic.CategorieName.StringValue, out thisAudio))
				{
					getLenghtC = thisMusic.SetAudio.Length;
					for (c = 0; c < getLenghtC; c++)
					{
						if (!thisAudio.TryGetValue (thisMusic.SetAudio [c].AudioName.StringValue, out value))
						{
							thisAudio.Add (thisMusic.SetAudio [c].AudioName.StringValue, thisMusic.SetAudio [c]);
						}
						else
						{
							Debug.LogError ("Two audio with the same name on this Categorie : " + thisMusic.CategorieName);
						}
					}
				}
				else
				{
					thisAudio = Manager.Garbage.ReInitDict<int, AllAudio> (typeof (KeyValuePair<int, AllAudio>));
					getLenghtC = thisMusic.SetAudio.Length;
					for (c = 0; c < getLenghtC; c++)
					{
						thisAudio.Add (thisMusic.SetAudio [c].AudioName.StringValue, thisMusic.SetAudio [c]);
					}

					SetAud.Add (thisMusic.CategorieName.StringValue, new Dictionary<int, AllAudio> (thisAudio));
					thisAudio.Clear ( );
					Manager.Garbage.AddGarbageClass (thisAudio);
				}
			}
		}

		Dictionary<int, Dictionary<int, AllAudio>> saveAud = new Dictionary<int, Dictionary<int, AllAudio>> (SetAud);
		SetAud.Clear ( );
		Manager.Garbage.AddGarbageClass (SetAud);
		return saveAud;
	}

	async void waitEndAudio (float timeEnd, AllInfo thisInf, sourceInfo thisSource, System.Action thisAct = null)
	{
		CancellationToken newTok;

		newTok = thisSource.CancelToke.Token;

		try
		{
			thisSource.ThisAudio.Play ( );

			await Task.Delay ((int)(timeEnd * 1000), newTok);

			thisInf.MusicRunning.Remove (thisSource);

			if (thisAct != null)
			{
				thisAct.Invoke ( );
			}

#if UNITY_EDITOR
			Destroy (thisSource.ThisAudio);
#else
			if (thisSource.ForceDest)
			{
				Destroy (thisSource.ThisAudio);
			}
			else
			{
				ableSource.Add (thisSource);
				thisSource.ThisAudio.clip = null;
				thisSource.ThisAudio.enabled = false;
			}
#endif
			thisSource.CancelToke.Dispose ( );
		}
		catch
		{
			thisSource.CancelToke.Dispose ( );
		}
	}
	#endregion
}
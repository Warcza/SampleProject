﻿using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

public class TimeManager : ManagerParent
{
	#region Variables
	List<TimeSet> currTime;
	#endregion

	#region Mono
	void Update ( )
	{
		float getTime = Time.deltaTime;
		TimeSet currTimer;
		int length = currTime.Count;
		for (int a = 0; a < length; a++)
		{
			currTimer = currTime [a];
			currTimer.CurrentTime += getTime;

			if (currTimer.CurrentTime > currTimer.TimeTarget)
			{
				currTime.RemoveAt (a);
				a--;
				length--;

				currTimer.CurrentTime = currTimer.TimeTarget;
				currTimer.IsCompleted = true;
				if (currTimer.TargetAction != null)
				{
					int length2 = currTimer.TargetAction.Count;
					for (int b = 0; b < length2; b++)
					{
						currTimer.TargetAction [b].Invoke ( );
					}

					Manager.Garbage.AddGarbageClass (currTimer.TargetAction);
				}

				if (currTimer.DestroyWhenFinish)
				{
					Manager.Garbage.AddGarbageClass (currTimer);
				}

				if (currTime.Count == 0)
				{
					this.enabled = false;
				}
			}
		}
	}
	#endregion

	#region Public Methodes
	public TimeSet CreateTimer (float maxTime, bool DestroyWhenFinish = true, System.Action thisAction = null)
	{
		TimeSet newTimer = Manager.Garbage.GetGarbaClass<TimeSet> (typeof (TimeSet));
		newTimer.IsCompleted = false;

		if (thisAction != null)
		{
			List<System.Action> newListAction = Manager.Garbage.ReInitList<System.Action> (typeof (System.Action));
			newListAction.Add (thisAction);
			SetTimer (newTimer, maxTime, DestroyWhenFinish, newListAction);
		}
		else
		{
			SetTimer (newTimer, maxTime, DestroyWhenFinish, null);
		}

		return newTimer;
	}

	public TimeSet CreateTimer (float maxTime, bool DestroyWhenFinish = true, List<System.Action> thisAction = null)
	{
		TimeSet newTimer = Manager.Garbage.GetGarbaClass<TimeSet> (typeof (TimeSet));
		SetTimer (newTimer, maxTime, DestroyWhenFinish, thisAction);

		return newTimer;
	}

	public void ResetTimer (TimeSet thisTimer, float maxTime, bool DestroyWhenFinish = true, System.Action thisAction = null)
	{
		if (thisAction != null)
		{
			List<System.Action> newListAction = Manager.Garbage.ReInitList<System.Action> (typeof (System.Action));
			newListAction.Add (thisAction);
			SetTimer (thisTimer, maxTime, DestroyWhenFinish, newListAction);
		}
		else
		{
			SetTimer (thisTimer, maxTime, DestroyWhenFinish, null);
		}
	}

	public void ResetTimer (TimeSet thisTimer, float maxTime, bool DestroyWhenFinish = true, List<System.Action> thisAction = null)
	{
		SetTimer (thisTimer, maxTime, DestroyWhenFinish, thisAction);
	}

	public void DestroyTimer (TimeSet thisTimer)
	{
		int length = currTime.Count;
		for (int a = 0; a < length; a++)
		{
			if (currTime [a].Equals (thisTimer))
			{
				Manager.Garbage.AddGarbageClass (thisTimer.TargetAction);
				Manager.Garbage.AddGarbageClass (thisTimer);
				currTime.RemoveAt (a);

				return;
			}
		}
	}
	#endregion

	#region Private Methodes
	protected override void InitializeManager ( )
	{
		currTime = new List<TimeSet> ( );
	}

	void SetTimer (TimeSet thisTimer, float maxTime, bool DestroyWhenFinish, List<System.Action> thisAction)
	{
		thisTimer.TimeTarget = maxTime;
		thisTimer.CurrentTime = 0;
		thisTimer.DestroyWhenFinish = DestroyWhenFinish;
		thisTimer.TargetAction = thisAction;

		if (!this.enabled)
		{
			this.enabled = true;
		}

		currTime.Add (thisTimer);
	}
	#endregion
}

[System.Serializable]
public class TimeSet
{
	public float TimeTarget;
	public float CurrentTime;
	public bool DestroyWhenFinish;
	public bool IsCompleted = false;
	public List<System.Action> TargetAction;
}
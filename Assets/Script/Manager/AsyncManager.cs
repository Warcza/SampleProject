﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;

public class AsyncManager : ManagerParent
{
	#region Variables
	public Color [ ] AllColor;
	public AnimationCurve DefaultAnime;

	float getTime = 30 * 0.001f;
	int timeWait = 30;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public void UpdateScaleChild (Transform thisParent)
	{
		int getCountChild = thisParent.childCount;

		Vector3 RatioScreen = Manager.UI.RatioScreen;
		for (int a = 0; a < getCountChild; a++)
		{
			thisParent.GetChild (a).localScale = RatioScreen;
		}
	}
	#endregion
	#region TransformInterface
	public void randPos (IList<Transform> ArrayTrans, Vector2 sizeRect, bool forceBoth = false, bool randomSideX = false, bool randomSideY = false)
	{
		setRandPos (ArrayTrans, sizeRect, forceBoth, randomSideX, randomSideY);
	}

	public void randPos (Transform getTrans, Vector2 sizeRect, bool forceBoth = false, bool randomSideX = false, bool randomSideY = false)
	{
		setRandPos (getTrans, sizeRect, forceBoth, randomSideX, randomSideY);
	}

#if !UNITY_ANDROID
	public void UisPosition (IList<Transform> ArrayTrans, Vector3 [ ] getPos, float timePlacement, System.Action thisAct = null, float waitPourc = 100, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		changeUiPos (ArrayTrans, getPos, timePlacement, thisAct, waitPourc, thisAnime, thisTok);
	}

	public void UiPosition (Transform thisTrans, Vector3 thisPos, float timePlacement, System.Action thisAct = null, float waitPourc = 100, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		changeUiPos (thisTrans, thisPos, timePlacement, thisAct, waitPourc, thisAnime, thisTok);
	}

	public void UisScale (IList<Transform> ArrayTrans, Vector3 [ ] arraySize, float timePlacement, System.Action thisAct = null, bool backNormal = false, Vector3 [ ] saveArraySize = null, float waitPourc = 100, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		changeScaleTrans (ArrayTrans, arraySize, timePlacement, thisAct, backNormal, saveArraySize, waitPourc, thisAnime, thisTok);
	}

	public void UiScale (Transform thisTrans, Vector3 thisSize, Vector3 saveSize, float timePlacement, System.Action thisAct = null, bool backNormal = false, float waitPourc = 100, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		changeScaleTrans (thisTrans, thisSize, timePlacement, thisAct, backNormal, saveSize, waitPourc, thisAnime, thisTok);
	}

#else
	public IEnumerator UisPosition (IList<Transform> ArrayTrans, Vector3 [ ] getPos, float timePlacement, System.Action thisAct = null, float waitPourc = 100, AnimationCurve thisAnime = null)
	{
		IEnumerator currAnim = changeUiPos (ArrayTrans, getPos, timePlacement, thisAct, waitPourc, thisAnime);

		StartCoroutine (currAnim);

		return currAnim;
	}

	public IEnumerator UiPosition (Transform thisTrans, Vector3 thisPos, float timePlacement, System.Action thisAct = null, float waitPourc = 100, AnimationCurve thisAnime = null)
	{
		IEnumerator currAnim = changeUiPos (thisTrans, thisPos, timePlacement, thisAct, waitPourc, thisAnime);

		StartCoroutine (currAnim);

		return currAnim;
	}

	public IEnumerator UisScale (IList<Transform> ArrayTrans, Vector3 [ ] arraySize, float timePlacement, System.Action thisAct = null, bool backNormal = false, Vector3 [ ] saveArraySize = null, float waitPourc = 100, AnimationCurve thisAnime = null, IEnumerator thisTok = null)
	{
		IEnumerator currAnim = changeScaleTrans (ArrayTrans, arraySize, timePlacement, thisAct, backNormal, saveArraySize, waitPourc, thisAnime);

		StartCoroutine (currAnim);

		return currAnim;
	}

	public IEnumerator UiScale (Transform thisTrans, Vector3 thisSize, Vector3 saveSize, float timePlacement, System.Action thisAct = null, bool backNormal = false, float waitPourc = 100, AnimationCurve thisAnime = null)
	{
		IEnumerator currAnim = changeScaleTrans (thisTrans, thisSize, timePlacement, thisAct, backNormal, saveSize, waitPourc, thisAnime);

		StartCoroutine (currAnim);

		return currAnim;
	}
#endif

	#endregion

	#region ColorInterface
	public void ChangeColorInstant (IList<Transform> ArrayTrans, Color [ ] RemoveThis = null, Color [ ] OtherColor = null)
	{
		SetInstantColor (ArrayTrans, RemoveThis, OtherColor);
	}

	public void ChangeColorInstant (Transform getTrans, Color [ ] RemoveThis = null, Color [ ] OtherColor = null)
	{
		SetInstantColor (getTrans, RemoveThis, OtherColor);
	}

	public void ChangeColor (IList<Transform> ArrayTrans, float timePlacement, System.Action thisAct = null, float waitPourc = 100, Color [ ] RemoveThis = null, Color [ ] OtherColor = null, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		changeUiColor (ArrayTrans, timePlacement, thisAct, waitPourc, RemoveThis, OtherColor, thisAnime, thisTok);
	}

	public void ChangeColor (Transform thisTrans, float timePlacement, System.Action thisAct = null, float waitPourc = 100, Color [ ] RemoveThis = null, Color [ ] OtherColor = null, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		changeUiColor (thisTrans, timePlacement, thisAct, waitPourc, RemoveThis, OtherColor, thisAnime, thisTok);
	}

	public void GoThisColor (IList<Transform> ArrayTrans, Color thisColor, float timePlacement, System.Action thisAct = null, float waitPourc = 100)
	{
		goThisColor (ArrayTrans, thisColor, timePlacement, thisAct, waitPourc);
	}

	public void GoThisColor (Transform thisTrans, Color thisColor, float timePlacement, System.Action thisAct = null, float waitPourc = 100)
	{
		goThisColor (thisTrans, thisColor, timePlacement, thisAct, waitPourc);
	}
	#endregion

	#region Private Methodes
	protected override void InitializeManager ( )
	{

	}

	#region ColorUpdate
	void SetInstantColor (IList<Transform> ArrayTrans, Color [ ] RemoveThis = null, Color [ ] OtherColor = null)
	{
		int getLength;
		int getRand;
		int a;

		List<Color> getCol = Manager.Garbage.ReInitList<Color> (typeof (Color));
		if (OtherColor != null && OtherColor.Length > 0)
		{
			getCol.AddRange (OtherColor);
		}
		else
		{
			getCol.AddRange (AllColor);
		}

		if (RemoveThis != null)
		{
			getLength = RemoveThis.Length;
			for (a = 0; a < getLength; a++)
			{
				if (getCol.Contains (RemoveThis [a]))
				{
					getCol.Remove (RemoveThis [a]);
				}
			}
		}

		getLength = ArrayTrans.Count;

		for (a = 0; a < getLength; a++)
		{
			getRand = Random.Range (0, getCol.Count);
			ArrayTrans [a].GetComponent<Image> ( ).color = getCol [getRand];
			getCol.RemoveAt (getRand);
		}

		Manager.Garbage.AddGarbageClass (ArrayTrans);
		Manager.Garbage.AddGarbageClass (getCol);
	}

	void SetInstantColor (Transform ArrayTrans, Color [ ] RemoveThis = null, Color [ ] OtherColor = null)
	{
		int getLength;
		int getRand;

		List<Color> getCol = Manager.Garbage.ReInitList<Color> (typeof (Color));
		if (OtherColor != null && OtherColor.Length > 0)
		{
			getCol.AddRange (OtherColor);
		}
		else
		{
			getCol.AddRange (AllColor);
		}

		if (RemoveThis != null)
		{
			getLength = RemoveThis.Length;
			for (int a = 0; a < getLength; a++)
			{
				if (getCol.Contains (RemoveThis [a]))
				{
					getCol.Remove (RemoveThis [a]);
				}
			}
		}

		getRand = Random.Range (0, getCol.Count);
		ArrayTrans.GetComponent<Image> ( ).color = getCol [getRand];
		getCol.RemoveAt (getRand);

		Manager.Garbage.AddGarbageClass (getCol);
	}

	async void changeUiColor (IList<Transform> ArrayTrans, float timePlacement, System.Action thisAct, float waitPourc, Color [ ] RemoveThis, Color [ ] OtherColor, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		float time = 0;

		int getLength;
		int getRand;
		int a;

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		List<Color> getCol = Manager.Garbage.ReInitList<Color> (typeof (Color));
		if (OtherColor != null && OtherColor.Length > 0)
		{
			getCol.AddRange (OtherColor);
		}
		else
		{
			getCol.AddRange (AllColor);
		}

		if (RemoveThis != null)
		{
			getLength = RemoveThis.Length;
			for (a = 0; a < getLength; a++)
			{
				if (getCol.Contains (RemoveThis [a]))
				{
					getCol.Remove (RemoveThis [a]);
				}
			}
		}

		getLength = ArrayTrans.Count;
		List<Color> currCol = Manager.Garbage.ReInitList<Color> (typeof (Color), getLength);
		List<Image> currImage = Manager.Garbage.ReInitList<Image> (typeof (Image), getLength);

		for (a = 0; a < getLength; a++)
		{
			getRand = Random.Range (0, getCol.Count);

			currImage.Add (ArrayTrans [a].GetComponent<Image> ( ));
			currCol.Add (getCol [getRand]);
			getCol.RemoveAt (getRand);
		}

		try
		{
			while (time < 1)
			{
				if (thisTok != null)
				{
					await Task.Delay (timeWait, thisTok.Token);
				}
				else
				{
					await Task.Delay (timeWait);
				}

				time += getTime / timePlacement;

				for (a = 0; a < getLength; a++)
				{
					if (time < 1 && !checkColor (currImage [a].color, currCol [a]))
					{
						currImage [a].color = Color.LerpUnclamped (currImage [a].color, currCol [a], thisCurve.Evaluate (time));
					}
					else
					{
						currImage [a].color = currCol [a];
					}
				}

				if (thisAct != null && time * 100 >= waitPourc)
				{
					thisAct.Invoke ( );
					thisAct = null;
					Manager.Garbage.AddGarbageClass (currImage);
					Manager.Garbage.AddGarbageClass (currCol);
					Manager.Garbage.AddGarbageClass (getCol);
					Manager.Garbage.AddGarbageClass (ArrayTrans);
				}
			}
		}
		catch
		{
			Manager.Garbage.AddGarbageClass (ArrayTrans);
			Manager.Garbage.AddGarbageClass (currImage);
			Manager.Garbage.AddGarbageClass (currCol);
			Manager.Garbage.AddGarbageClass (getCol);
		}
	}

	async void changeUiColor (Transform ArrayTrans, float timePlacement, System.Action thisAct, float waitPourc, Color [ ] RemoveThis, Color [ ] OtherColor, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		float time = 0;

		int getLength;
		int getRand;
		int a;

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		List<Color> getCol = Manager.Garbage.ReInitList<Color> (typeof (Color));
		if (OtherColor != null && OtherColor.Length > 0)
		{
			getCol.AddRange (OtherColor);
		}
		else
		{
			getCol.AddRange (AllColor);
		}

		if (RemoveThis != null)
		{
			getLength = RemoveThis.Length;
			for (a = 0; a < getLength; a++)
			{
				if (getCol.Contains (RemoveThis [a]))
				{
					getCol.Remove (RemoveThis [a]);
				}
			}
		}

		getRand = Random.Range (0, getCol.Count);
		Color currCol = getCol [getRand];
		Image currImage = ArrayTrans.GetComponent<Image> ( );

		try
		{
			while (time < 1)
			{
				if (thisTok != null)
				{
					await Task.Delay (timeWait, thisTok.Token);
				}
				else
				{
					await Task.Delay (timeWait);
				}

				time += getTime / timePlacement;

				if (time < 1 && !checkColor (currImage.color, currCol))
				{
					currImage.color = Color.LerpUnclamped (currImage.color, currCol, thisCurve.Evaluate (time));
				}
				else
				{
					currImage.color = currCol;
				}

				if (thisAct != null && time * 100 >= waitPourc)
				{
					thisAct.Invoke ( );
					thisAct = null;
					Manager.Garbage.AddGarbageClass (currImage);
				}
			}
		}
		catch
		{
			Manager.Garbage.AddGarbageClass (getCol);
		}
	}

	async void goThisColor (IList<Transform> ArrayTrans, Color thisColor, float timePlacement, System.Action thisAct = null, float waitPourc = 100, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		float time = 0;

		int getLength = ArrayTrans.Count;
		List<Image> currImage = Manager.Garbage.ReInitList<Image> (typeof (Image), getLength);

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		for (int a = 0; a < getLength; a++)
		{
			currImage.Add (ArrayTrans [a].GetComponent<Image> ( ));
		}

		try
		{
			while (time < 1)
			{
				if (thisTok != null)
				{
					await Task.Delay (timeWait, thisTok.Token);
				}
				else
				{
					await Task.Delay (timeWait);
				}

				time += getTime / timePlacement;

				for (int a = 0; a < getLength; a++)
				{
					if (time < 1)
					{
						currImage [a].color = Color.LerpUnclamped (currImage [a].color, thisColor, thisCurve.Evaluate (time));
					}
					else
					{
						currImage [a].color = thisColor;
					}
				}

				if (thisAct != null && time * 100 >= waitPourc)
				{
					thisAct.Invoke ( );
					thisAct = null;
				}
			}

			Manager.Garbage.AddGarbageClass (currImage);
			Manager.Garbage.AddGarbageClass (ArrayTrans);
		}
		catch
		{
			Manager.Garbage.AddGarbageClass (currImage);
			Manager.Garbage.AddGarbageClass (ArrayTrans);
		}
	}

	async void goThisColor (Transform ArrayTrans, Color thisColor, float timePlacement, System.Action thisAct = null, float waitPourc = 100, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		float time = 0;

		Image currImage = ArrayTrans.GetComponent<Image> ( );

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		try
		{
			while (time < 1)
			{
				if (thisTok != null)
				{
					await Task.Delay (timeWait, thisTok.Token);
				}
				else
				{
					await Task.Delay (timeWait);
				}

				time += getTime / timePlacement;

				if (time < 1)
				{
					currImage.color = Color.LerpUnclamped (currImage.color, thisColor, thisCurve.Evaluate (time));
				}
				else
				{
					currImage.color = thisColor;
				}

				if (thisAct != null && time * 100 >= waitPourc)
				{
					thisAct.Invoke ( );
					thisAct = null;
				}
			}
		}
		catch
		{ }
	}

	bool checkColor (Color color1, Color color2)
	{
		if (Mathf.Abs (color1.r - color2.r) > 0.001f)
		{
			return false;
		}
		else if (Mathf.Abs (color1.b - color2.b) > 0.001f)
		{
			return false;
		}
		else if (Mathf.Abs (color1.g - color2.g) > 0.001f)
		{
			return false;
		}
		else if (Mathf.Abs (color1.a - color2.a) > 0.001f)
		{
			return false;
		}

		return true;
	}
	#endregion

	#region TransformUpdate
	void setRandPos (IList<Transform> ArrayTrans, Vector2 sizeRect, bool forceBoth = false, bool randomSideX = false, bool randomSideY = false)
	{
		Vector3 getCurrPos;
		Vector2 getNewPos;

		int getRand;
		int getLength = ArrayTrans.Count;

		for (int a = 0; a < getLength; a++)
		{
			ArrayTrans [a].GetComponent<Image> ( ).color = Color.black;
			getCurrPos = ArrayTrans [a].localPosition;
			getNewPos = newPos (getCurrPos, sizeRect);

			if (!forceBoth)
			{
				getRand = Random.Range (0, 2);

				if (getRand == 0)
				{
					getRand = Random.Range (0, 2);

					if (getRand == 0)
					{
						getNewPos = new Vector2 (0, getNewPos.y);
					}
					else
					{
						getNewPos = new Vector2 (getNewPos.x, 0);
					}
				}
			}

			if (randomSideX)
			{
				if (Random.Range (0, 2) == 0)
				{
					getNewPos = new Vector3 (-getNewPos.x, getNewPos.y);
				}
			}

			if (randomSideY)
			{
				if (Random.Range (0, 2) == 0)
				{
					getNewPos = new Vector3 (getNewPos.x, -getNewPos.y);
				}
			}

			getCurrPos = new Vector3 (getCurrPos.x + getNewPos.x, getCurrPos.y + getNewPos.y, getCurrPos.z);

			ArrayTrans [a].localPosition = getCurrPos;
		}
		Manager.Garbage.AddGarbageClass (ArrayTrans);
	}

	void setRandPos (Transform ArrayTrans, Vector2 sizeRect, bool forceBoth = false, bool randomSideX = false, bool randomSideY = false)
	{
		Vector3 getCurrPos;
		Vector2 getNewPos;

		int getRand;

		ArrayTrans.GetComponent<Image> ( ).color = Color.black;
		getCurrPos = ArrayTrans.localPosition;
		getNewPos = newPos (getCurrPos, sizeRect);

		if (!forceBoth)
		{
			getRand = Random.Range (0, 2);

			if (getRand == 0)
			{
				getRand = Random.Range (0, 2);

				if (getRand == 0)
				{
					getNewPos = new Vector2 (0, getNewPos.y);
				}
				else
				{
					getNewPos = new Vector2 (getNewPos.x, 0);
				}
			}
		}

		if (randomSideX)
		{
			if (Random.Range (0, 2) == 0)
			{
				getNewPos = new Vector3 (-getNewPos.x, getNewPos.y);
			}
		}

		if (randomSideY)
		{
			if (Random.Range (0, 2) == 0)
			{
				getNewPos = new Vector3 (getNewPos.x, -getNewPos.y);
			}
		}

		getCurrPos = new Vector3 (getCurrPos.x + getNewPos.x, getCurrPos.y + getNewPos.y, getCurrPos.z);

		ArrayTrans.localPosition = getCurrPos;
	}

	Vector2 newPos (Vector3 thisPos, Vector2 getRect)
	{
		Vector2 setRect = getRect;
		if (thisPos.y < 0)
		{
			setRect = new Vector2 (setRect.x, -getRect.y);
		}
		else
		{
			setRect = new Vector2 (setRect.x, getRect.y);
		}

		if (thisPos.x < 0)
		{
			setRect = new Vector2 (-getRect.x, setRect.y);
		}
		else
		{
			setRect = new Vector2 (getRect.x, setRect.y);
		}

		return setRect;
	}

#if !UNITY_ANDROID
	async void changeUiPos (IList<Transform> ArrayTrans, Vector3 [ ] getPos, float timePlacement, System.Action thisAct, float waitPourc, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		await Task.Delay (timeWait);

		List<Vector3> startPos = Manager.Garbage.ReInitList<Vector3> (typeof (Vector3));
		Vector3 currPos;

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		float time = 0;

		int getLength = ArrayTrans.Count;
		int a;

		for (a = 0; a < getLength; a++)
		{
			startPos.Add (ArrayTrans [a].localPosition);
		}

		try
		{
			while (time < 1)
			{
				if (thisTok != null)
				{
					await Task.Delay (timeWait, thisTok.Token);
				}
				else
				{
					await Task.Delay (timeWait);
				}

				time += getTime / timePlacement;

				for (a = 0; a < getLength; a++)
				{
					currPos = ArrayTrans [a].localPosition;

					if (time < 1 && (ArrayTrans [a].localPosition - getPos [a]).magnitude > 0.05f)
					{
						ArrayTrans [a].localPosition = Vector3.LerpUnclamped (startPos [a], getPos [a], thisCurve.Evaluate (time));
					}
					else
					{
						ArrayTrans [a].localPosition = new Vector3 (getPos [a].x, getPos [a].y, currPos.z);
					}
				}

				if (thisAct != null && time * 100 >= waitPourc)
				{
					thisAct.Invoke ( );
					thisAct = null;
				}
			}
			Manager.Garbage.AddGarbageClass (ArrayTrans);
			Manager.Garbage.AddGarbageClass (startPos);
		}
		catch
		{
			Manager.Garbage.AddGarbageClass (ArrayTrans);
			Manager.Garbage.AddGarbageClass (startPos);
		}
	}

	async void changeUiPos (Transform ArrayTrans, Vector3 getPos, float timePlacement, System.Action thisAct, float waitPourc, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		await Task.Delay (timeWait);

		Vector3 startPos = ArrayTrans.localPosition;
		Vector3 currPos;

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		float time = 0;

		try
		{
			while (time < 1)
			{
				if (thisTok != null)
				{
					await Task.Delay (timeWait, thisTok.Token);
				}
				else
				{
					await Task.Delay (timeWait);
				}

				time += getTime / timePlacement;

				currPos = ArrayTrans.localPosition;

				if (time < 1 && (ArrayTrans.localPosition - getPos).magnitude > 0.05f)
				{
					ArrayTrans.localPosition = Vector3.LerpUnclamped (startPos, getPos, thisCurve.Evaluate (time));
				}
				else
				{
					ArrayTrans.localPosition = new Vector3 (getPos.x, getPos.y, currPos.z);
				}

				if (thisAct != null && (time * 100 >= waitPourc))
				{
					thisAct.Invoke ( );
					thisAct = null;
				}
			}
		}
		catch
		{ }
	}

	async void changeScaleTrans (IList<Transform> ArrayTrans, Vector3 [ ] getSize, float timePlacement, System.Action thisAct, bool backNormal, Vector3 [ ] saveArraySize = null, float waitPourc = 100, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		Vector3 currPos;

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		float time = 0;

		int getLength = ArrayTrans.Count;
		int a;

		try
		{
			while (time < 1)
			{
				if (thisTok != null)
				{
					await Task.Delay (timeWait, thisTok.Token);
				}
				else
				{
					await Task.Delay (timeWait);
				}

				time += getTime / timePlacement;

				for (a = 0; a < getLength; a++)
				{
					currPos = ArrayTrans [a].localScale;

					if (time < 1 && (currPos - getSize [a]).magnitude > 0.05f)
					{
						ArrayTrans [a].localScale = Vector3.LerpUnclamped (saveArraySize [a], getSize [a], thisCurve.Evaluate (time));
					}
					else
					{
						ArrayTrans [a].localScale = new Vector3 (getSize [a].x, getSize [a].y, currPos.z);
					}
				}

				if (time * 100 >= waitPourc)
				{
					if (backNormal)
					{
						backNormal = false;
						time = 0;

						Vector3 [ ] defaultSize = saveArraySize;
						saveArraySize [a] = getSize [a];
						getSize = defaultSize;
					}
					else if (thisAct != null)
					{
						thisAct.Invoke ( );
						thisAct = null;
					}
				}
			}
			Manager.Garbage.AddGarbageClass (ArrayTrans);
		}
		catch
		{
			Manager.Garbage.AddGarbageClass (ArrayTrans);
		}
	}

	async void changeScaleTrans (Transform thisTrans, Vector3 getSize, float timePlacement, System.Action thisAct, bool backNormal, Vector3 saveArraySize, float waitPourc = 100, AnimationCurve thisAnime = null, CancellationTokenSource thisTok = null)
	{
		Vector3 currPos;

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		float time = 0;

		try
		{
			while (time < 1)
			{
				if (thisTok != null)
				{
					await Task.Delay (timeWait, thisTok.Token);
				}
				else
				{
					await Task.Delay (timeWait);
				}

				time += getTime / timePlacement;

				currPos = thisTrans.localScale;

				if (time < 1 && (currPos - getSize).magnitude > 0.05f)
				{
					thisTrans.localScale = Vector3.LerpUnclamped (saveArraySize, getSize, thisCurve.Evaluate (time));
				}
				else
				{
					thisTrans.localScale = new Vector3 (getSize.x, getSize.y, currPos.z);
				}

				if (time * 100 >= waitPourc)
				{
					if (backNormal)
					{
						backNormal = false;
						time = 0;
						Vector3 defaultSize = saveArraySize;
						saveArraySize = getSize;
						getSize = defaultSize;
					}
					else if (thisAct != null)
					{
						thisAct.Invoke ( );
						thisAct = null;
					}
				}
			}
		}
		catch
		{ }
	}

#else

	IEnumerator changeUiPos (IList<Transform> ArrayTrans, Vector3 [ ] getPos, float timePlacement, System.Action thisAct, float waitPourc, AnimationCurve thisAnime = null)
	{
		Vector3 [ ] startPos = new Vector3 [ArrayTrans.Count];
		Vector3 currPos;

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		float time = 0;

		int getLength = ArrayTrans.Count;
		int a;

		for (a = 0; a < getLength; a++)
		{
			startPos [a] = ArrayTrans [a].localPosition;
		}

		while (time < 1)
		{
			yield return thisF;

			time += Time.deltaTime / timePlacement;

			for (a = 0; a < getLength; a++)
			{
				currPos = ArrayTrans [a].localPosition;

				if (time < 1 && (ArrayTrans [a].localPosition - getPos [a]).magnitude > 0.05f)
				{
					ArrayTrans [a].localPosition = Vector3.LerpUnclamped (startPos [a], getPos [a], thisCurve.Evaluate (time));
				}
				else
				{
					ArrayTrans [a].localPosition = new Vector3 (getPos [a].x, getPos [a].y, currPos.z);
				}
			}

			if (thisAct != null && time * 100 >= waitPourc)
			{
				thisAct.Invoke ( );
				thisAct = null;
			}
		}

		Manager.Garbage.AddGarbageClass (ArrayTrans);
	}

	IEnumerator changeUiPos (Transform ArrayTrans, Vector3 getPos, float timePlacement, System.Action thisAct, float waitPourc, AnimationCurve thisAnime = null)
	{
		Vector3 startPos = ArrayTrans.localPosition;
		Vector3 currPos;

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		float time = 0;

		while (time < 1)
		{
			yield return thisF;

			time += Time.deltaTime / timePlacement;

			currPos = ArrayTrans.localPosition;

			if (time < 1 && (ArrayTrans.localPosition - getPos).magnitude > 0.05f)
			{
				ArrayTrans.localPosition = Vector3.LerpUnclamped (startPos, getPos, thisCurve.Evaluate (time));
			}
			else
			{
				ArrayTrans.localPosition = new Vector3 (getPos.x, getPos.y, currPos.z);
			}

			if (thisAct != null && (time * 100 >= waitPourc))
			{
				thisAct.Invoke ( );
				thisAct = null;
			}
		}
	}

	IEnumerator changeScaleTrans (IList<Transform> ArrayTrans, Vector3 [ ] getSize, float timePlacement, System.Action thisAct, bool backNormal, Vector3 [ ] saveArraySize = null, float waitPourc = 100, AnimationCurve thisAnime = null)
	{
		Vector3 currPos;

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		float time = 0;

		int getLength = ArrayTrans.Count;
		int a;

		while (time < 1)
		{
			yield return thisF;

			time += Time.deltaTime / timePlacement;

			for (a = 0; a < getLength; a++)
			{
				currPos = ArrayTrans [a].localScale;

				if (time < 1 && (currPos - getSize [a]).magnitude > 0.05f)
				{
					ArrayTrans [a].localScale = Vector3.LerpUnclamped (saveArraySize [a], getSize [a], thisCurve.Evaluate (time));
				}
				else
				{
					ArrayTrans [a].localScale = new Vector3 (getSize [a].x, getSize [a].y, currPos.z);
				}
			}

			if (time * 100 >= waitPourc)
			{
				if (backNormal)
				{
					backNormal = false;
					time = 0;

					Vector3 [ ] defaultSize = saveArraySize;
					saveArraySize [a] = getSize [a];
					getSize = defaultSize;
				}
				else if (thisAct != null)
				{
					thisAct.Invoke ( );
					thisAct = null;
				}
			}
		}
		Manager.Garbage.AddGarbageClass (ArrayTrans);
	}

	IEnumerator changeScaleTrans (Transform thisTrans, Vector3 getSize, float timePlacement, System.Action thisAct, bool backNormal, Vector3 saveArraySize, float waitPourc = 100, AnimationCurve thisAnime = null)
	{
		Vector3 currPos;

		AnimationCurve thisCurve = DefaultAnime;

		if (thisAnime != null)
		{
			thisCurve = thisAnime;
		}

		float time = 0;

		while (time < 1)
		{
			yield return thisF;

			time += Time.deltaTime / timePlacement;

			currPos = thisTrans.localScale;

			if (time < 1 && (currPos - getSize).magnitude > 0.05f)
			{
				thisTrans.localScale = Vector3.LerpUnclamped (saveArraySize, getSize, thisCurve.Evaluate (time));
			}
			else
			{
				thisTrans.localScale = new Vector3 (getSize.x, getSize.y, currPos.z);
			}

			if (time * 100 >= waitPourc)
			{
				if (backNormal)
				{
					backNormal = false;
					time = 0;
					Vector3 defaultSize = saveArraySize;
					saveArraySize = getSize;
					getSize = defaultSize;
				}
				else if (thisAct != null)
				{
					thisAct.Invoke ( );
					thisAct = null;
				}
			}
		}
	}
#endif

	#endregion

	#endregion
}
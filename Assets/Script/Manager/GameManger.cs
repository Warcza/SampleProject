﻿using UnityEngine;

public class GameManger : ManagerParent
{
	#region Variables
	public static int LAYER_GROUND;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public void GameReady (GameMode thisMode)
	{

	}

	#endregion

	#region Private Methodes
	protected override void InitializeManager ( )
	{
		LAYER_GROUND = 1 << LayerMask.NameToLayer ("Ground");
	}
	#endregion
}
﻿using System.Collections.Generic;

using UnityEngine;

public class PrefabManager : ManagerParent
{
	#region Variables
	Dictionary<int, List<GameObject>> ObjectList;
	Dictionary<int, int> ObjStock;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public void StockObject (GameObject thisObj, int PrefabID)
	{
		thisObj.SetActive (false);
		List<GameObject> getList;
		if (ObjectList.TryGetValue (PrefabID, out getList))
		{
			getList.Add (thisObj);
		}
		else
		{
			getList = new List<GameObject> ( );
			getList.Add (thisObj);
			ObjectList.Add (PrefabID, getList);
		}
	}

	public void StockObjectSaved (GameObject thisObj)
	{
		thisObj.SetActive (false);
		int getId;
		int idObj = thisObj.GetInstanceID ( );
		if (ObjStock.TryGetValue (idObj, out getId))
		{
			StockObject (thisObj, getId);
			ObjStock.Remove (idObj);
		}
	}

	public GameObject GetObject (GameObject PrefabObj, Transform defaultParent = null, bool stockObj = false)
	{
		List<GameObject> getList;
		if (ObjectList.TryGetValue (PrefabObj.GetInstanceID ( ), out getList))
		{
			int getLength = getList.Count;
			for (int a = getLength - 1; a > 0; a--)
			{
				if (getList [a] != null)
				{
					GameObject getObj = getList [a];
					getObj.SetActive (true);
					getObj.transform.SetParent (defaultParent);

					getList.RemoveAt (a);
					Debug.Log (getObj.name);
					return getObj;
				}
				else
				{
					getList.RemoveAt (a);
				}
			}
		}

		if (stockObj)
		{
			GameObject getInstance = Instantiate (PrefabObj, defaultParent);

			ObjStock.Add (getInstance.GetInstanceID ( ), PrefabObj.GetInstanceID ( ));
			return getInstance;
		}

		return Instantiate (PrefabObj, defaultParent);
	}
	#endregion

	#region Private Methodes
	protected override void InitializeManager ( )
	{
		ObjectList = new Dictionary<int, List<GameObject>> ( );
		ObjStock = new Dictionary<int, int> ( );
	}
	#endregion
}
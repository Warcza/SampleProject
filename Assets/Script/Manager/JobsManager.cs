﻿using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;

using UnityEngine;
using UnityEngine.Jobs;

public class JobsManager : ManagerParent
{
	#region Variables
	NativeArray<Vector3> thisArray;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public NativeArray<Vector3> IniVectorArray (Vector3 MinValue, Vector3 MaxValue, RandomType RandType, int Lenght)
	{
		if (thisArray.IsCreated)
		{
			thisArray.Dispose ( );
		}
		thisArray = new NativeArray<Vector3> (Lenght, Allocator.Temp);

		for (int a = 0; a < Lenght; a++)
		{
			switch (RandType)
			{
				case RandomType.Standard:
					thisArray [a] = new Vector3 (Random.Range (MinValue.x, MaxValue.x), Random.Range (MinValue.y, MaxValue.y), Random.Range (MinValue.z, MaxValue.z));
					break;
				case RandomType.Clamp:
					if (Random.Range (0, 2) == 0)
					{
						thisArray [a] = MinValue;
					}
					else
					{
						thisArray [a] = MaxValue;
					}
					break;
				case RandomType.Nothing:
					thisArray [a] = MaxValue;
					break;
			}
		}
		return thisArray;
	}

	public void DisposeVector3 ( )
	{
		thisArray.Dispose ( );
	}
	#endregion

	#region Private Methodes
	void OnApplicationQuit ( )
	{
		if (thisArray.IsCreated)
		{
			thisArray.Dispose ( );
		}
	}
	protected override void InitializeManager ( )
	{ }
	#endregion
}

#region JobRegion
[ComputeJobOptimization]
public struct J_RotateAround : IJobParallelForTransform
{
	public NativeArray<Vector3> ValueFollow;

	[ReadOnly] public NativeArray<Vector3> DistFollow;
	[ReadOnly] public NativeArray<Vector3> MoveSpeed;

	public Vector3 TargetCenter;
	public float DeltaTime;
	public bool AutoMove;

	public void Execute (int ID, TransformAccess ThisTrans)
	{
		Vector3 getVect = MathFunctions.CubeFollow (ValueFollow [ID], DistFollow [ID]);

		if (AutoMove)
		{
			ValueFollow [ID] += new Vector3 (MoveSpeed [ID].x, MoveSpeed [ID].y, MoveSpeed [ID].z) * DeltaTime;
		}

		ThisTrans.position = Vector3.Lerp (ThisTrans.position, getVect + TargetCenter, DeltaTime);
	}
}

[ComputeJobOptimization]
public struct J_MoveTransform : IJobParallelForTransform
{
	public Vector3 TargetPos;

	[ReadOnly] public NativeArray<Vector3> GapPos;
	[ReadOnly] public NativeArray<float> MoveSpeed;

	public float DeltaTime;
	public void Execute (int ID, TransformAccess ThisTrans)
	{
		ThisTrans.position = Vector3.Lerp (ThisTrans.position, TargetPos + GapPos [ID], MoveSpeed [ID] * DeltaTime);
	}
}

[ComputeJobOptimization]
public struct J_RotateTransform : IJobParallelForTransform
{
	public Quaternion TargetRot;

	[ReadOnly] public NativeArray<Quaternion> GapRot;
	[ReadOnly] public NativeArray<float> RotateSpeed;
	public float DeltaTime;

	public void Execute (int ID, TransformAccess ThisTrans)
	{
		ThisTrans.rotation = Quaternion.Lerp (ThisTrans.rotation, TargetRot * GapRot [ID], RotateSpeed [ID] * DeltaTime);
	}
}
#endregion

#region Entity Region
class MoveSystem : ComponentSystem
{
	struct Components
	{
		public RotateCube ThisMove;
	}

	protected override void OnUpdate ( )
	{
		ComponentGroupArray<Components> getComps = GetEntities<Components> ( );
		if (getComps.Length == 0)
		{
			getComps.Dispose ( );
			return;
		}
		JobHandle thisJob;
		J_RotateAround rotCube;
		J_RotateAround otherRotCube;
		RotateCube ThisMove;

		float getTime = Time.deltaTime;
		int lenght = getComps.Length;

		ThisMove = getComps [0].ThisMove;

		rotCube = new J_RotateAround ( )
		{
			AutoMove = true,
				DeltaTime = getTime,
				TargetCenter = ThisMove.target.position,
				ValueFollow = ThisMove.ValueFollow,
				DistFollow = ThisMove.DistFollow,
				MoveSpeed = ThisMove.MoveSpeed,
		};

		thisJob = rotCube.Schedule (ThisMove.TransArray);

		otherRotCube = rotCube;
		ThisMove.ValueFollow = otherRotCube.ValueFollow;

		for (int a = 1; a < lenght; a++)
		{
			ThisMove = getComps [a].ThisMove;

			rotCube = new J_RotateAround ( )
			{
				AutoMove = true,
					DeltaTime = getTime,
					TargetCenter = ThisMove.target.position,
					ValueFollow = ThisMove.ValueFollow,
					DistFollow = ThisMove.DistFollow,
					MoveSpeed = ThisMove.MoveSpeed,
			};

			thisJob = rotCube.Schedule (ThisMove.TransArray, thisJob);

			ThisMove.ValueFollow = otherRotCube.ValueFollow;
			otherRotCube = rotCube;
		}

		thisJob.Complete ( );
		ThisMove.ValueFollow = rotCube.ValueFollow;
		getComps.Dispose ( );
	}
}
#endregion
﻿using UnityEngine;

public abstract class UiParent : MonoBehaviour
{
	#region Variables
	public abstract MenuType ThisType
	{
		get;
	}

	public bool IsActive;

	Canvas thisCanv;
	#endregion

	#region Mono
	#endregion

	#region Public Methods
	public void Initialize ( )
	{
		thisCanv = GetComponent<Canvas> ( );
		InitializeUi ( );
	}

	public virtual void OpenThis (MenuTokenAbstract GetTok = null)
	{
		gameObject.SetActive (true);
		setBasicStuff (true);
	}

	public virtual void CloseThis ( )
	{
		setBasicStuff (false);
		gameObject.SetActive (false);
	}
	#endregion

	#region Private Methods
	protected abstract void InitializeUi ( );
	protected void setBasicStuff (bool isOpen)
	{
		IsActive = isOpen;
		if (isOpen)
		{
			thisCanv.sortingOrder = Manager.UI.LastCanvasOrder ( );
		}
		else
		{
			thisCanv.sortingOrder = 0;
		}
	}

		
	protected void setButton (bool active, Transform[] allButton)
	{
		int getLength = allButton.Length;

		for (int a = 0; a < getLength; a++)
		{
			allButton[a].GetComponent<UnityEngine.UI.Image>().raycastTarget = active;
		}
	}

	protected void setButton (bool active, Transform allButton)
	{
		allButton.GetComponent<UnityEngine.UI.Image>().raycastTarget = active;
	}
	#endregion
}
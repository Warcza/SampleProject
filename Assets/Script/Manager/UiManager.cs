﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using UnityEngine;
using UnityEngine.UI;

public class UiManager : ManagerParent
{
	#region Variables
	Dictionary<MenuType, UiParent> AllMenu;
	List<UiParent> openMenu;

	[Header ("Info debug")]
	public Vector2 RectScreenSize;
	public Vector3 RatioScreen;
	[SerializeField] bool displayMenuOnStart;
	[SerializeField] GameObject background;

	#endregion

	#region Mono
	#endregion

	#region Public Methods
	public void DisplayMenu (MenuType MT, MenuTokenAbstract Source = null)
	{
		UiParent getCurrentOM;
		if (AllMenu.TryGetValue (MT, out getCurrentOM))
		{
			if (openMenu.Contains (getCurrentOM))
			{
				CloseMenu (MT);
			}

			if(openMenu.Count > 0)
			{
				openMenu[openMenu.Count -1].IsActive = false;
			}

			openMenu.Add (getCurrentOM);

			getCurrentOM.OpenThis (Source);
		}
	}

	public void CloseMenu (int indexLast = 0)
	{
		if (openMenu.Count > indexLast)
		{
			UiParent getCurrentOM = openMenu [openMenu.Count - 1 - indexLast];

			getCurrentOM.CloseThis ( );
			openMenu.RemoveAt (openMenu.Count - 1 - indexLast);

			if (openMenu.Count > indexLast)
			{
				openMenu [openMenu.Count - 1 - indexLast].IsActive = true;
			}
		}
	}

	public void CloseAllMenu (MenuType activateBeforeType = MenuType.None, bool before = true)
	{
		UiParent getCurrentOM;

		if (before)
		{
			for (int a = 0; a < openMenu.Count - 1; a++)
			{
				getCurrentOM = openMenu [a];

				if (getCurrentOM.ThisType != activateBeforeType)
				{
					getCurrentOM.CloseThis ( );
					openMenu.RemoveAt (a);
					a--;
				}
				else
				{
					getCurrentOM.CloseThis ( );
					openMenu.RemoveAt (a);
					a--;
					return;
				}
			}
		}
		else
		{
			for (int a = openMenu.Count - 1; a >= 0; a--)
			{
				getCurrentOM = openMenu [a];

				if (getCurrentOM.ThisType != activateBeforeType)
				{
					getCurrentOM.CloseThis ( );
					openMenu.RemoveAt (a);
				}
				else
				{
					getCurrentOM.CloseThis ( );
					openMenu.RemoveAt (a);
					return;
				}
			}
		}
	}

	// Close spec menu
	public void CloseMenu (MenuType MT)
	{
		UiParent getCurrentOM;

		if (AllMenu.TryGetValue (MT, out getCurrentOM))
		{
			getCurrentOM.CloseThis ( );

			if (openMenu.Contains (getCurrentOM))
			{
				if(openMenu.IndexOf(getCurrentOM) == openMenu.Count - 1 && openMenu.Count > 1)
				{
					openMenu[openMenu.Count - 2].IsActive = true;
				}
				
				openMenu.Remove (getCurrentOM);
			}
		}
	}

	// Return the top of one canvas order + 1
	public int LastCanvasOrder ( )
	{
		int getMaxOrder = 0;

		for (int a = 0; a < openMenu.Count; a++)
		{
			try
			{
				if (openMenu [a].GetComponent<Canvas> ( ).sortingOrder >= getMaxOrder)
				{
					getMaxOrder = openMenu [a].GetComponent<Canvas> ( ).sortingOrder + 1;
				}
			}
			catch
			{ }
		}

		return getMaxOrder;
	}
	#endregion

	#region Private Methods
	protected override void InitializeManager ( )
	{
		RectScreenSize = new Vector2 (Screen.width, Screen.height) * 1.01f;
		RatioScreen = Vector3.one * (RectScreenSize.x / 1920);

		Object [ ] getAllMenu = Resources.LoadAll ("MenuUI");
		Dictionary<MenuType, UiParent> setAllMenu = new Dictionary<MenuType, UiParent> (getAllMenu.Length);
	 	openMenu = new List<UiParent>(getAllMenu.Length);

		GameObject thisMenu;
		UiParent thisUi;
		Transform getParent = transform;

		for (int a = 0; a < getAllMenu.Length; a++)
		{
			thisMenu = (GameObject)Instantiate (getAllMenu [a], getParent);
			thisUi = thisMenu.GetComponent<UiParent> ( );
			thisUi.Initialize ( );
			thisMenu.SetActive (false);
			setAllMenu.Add (thisUi.ThisType, thisUi);
		}

		AllMenu = setAllMenu;

		background.SetActive(displayMenuOnStart);
		if(displayMenuOnStart)
		{
			DisplayMenu(MenuType.Home);
		}
	}
	#endregion
}
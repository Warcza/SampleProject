﻿using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.SceneManagement;

public class ScnManager : ManagerParent
{
	#region Variables
	public LevelManager LvlManager;
	
	public float LoadingPourc
	{
		get 
		{
			return currentPourc;
		}
	}

	float currentPourc;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public void LoadScene (string thisScene, GameMode thisType)
	{
		currentPourc = 0;

		Manager.UI.CloseAllMenu ( );
		//TransitionToken tok = new TransitionToken();

		Manager.UI.DisplayMenu (MenuType.Transition);

		openThisScene (thisScene, thisType);
	}
	#endregion

	#region Private Methodes
	protected override void InitializeManager ( )
	{

	}

	async void openThisScene (string thisScene, GameMode thisType)
	{
		AsyncOperation thisOp = SceneManager.LoadSceneAsync (thisScene);

		do
		{
			await Task.Delay (10);
			currentPourc = thisOp.progress;
		} while (!thisOp.isDone);

		await Task.Delay (50);

		Manager.UI.CloseMenu (MenuType.Transition);
		Manager.Game.GameReady (thisType);
	}
	#endregion
}
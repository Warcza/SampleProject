﻿using System.Collections.Generic;

using UnityEngine;
public class AudioObject : MonoBehaviour, PlayAudio
{
   #region variable
   [SerializeField] private AudioSettingType [ ] AudioSetting = default;

   private Dictionary<AudioPlaying, AudioToken> dictAudio;
   private Transform thisTrans;
   private int idObject;

   [System.Serializable]
   public struct AudioSettingType
   {
      public AudioPlaying PlayingType;
      public AudioToken AudioSetting;
      public bool StopAudioOnDestroy;
   }
   #endregion

   #region Mono

   private void Awake ( )
   {
      dictAudio = Manager.Garbage.ReInitDict<AudioPlaying, AudioToken> (typeof (KeyValuePair<AudioPlaying, AudioToken>));
      thisTrans = transform;
      idObject = gameObject.GetInstanceID ( );

      for (int a = 0; a < AudioSetting.Length; a++)
      {
         if (!dictAudio.TryGetValue (AudioSetting [a].PlayingType, out AudioToken thisAud))
         {
            dictAudio.Add (AudioSetting [a].PlayingType, AudioSetting [a].AudioSetting);
         }
         else
         {
            Debug.LogError ("Two Audio Setting Type with same Enum : " + AudioSetting [a].PlayingType);
         }
      }

      Manager.Audio.AddAudioObject (idObject, this);
   }

   void OnDestroy ( )
   {
      int length = AudioSetting.Length;
      for (int a = 0; a < length; a++)
      {
         if (AudioSetting [a].StopAudioOnDestroy)
         {
            AudioToken _aud;

            if (dictAudio.TryGetValue (AudioSetting [a].PlayingType, out _aud))
            {
               Manager.Audio.CloseThisAudio (_aud.ThisType, _aud.CategorieName, _aud.ThisName);
            }
         }
      }

      Manager.Audio.RemoveAudioObject (idObject);
   }
   #endregion

   #region public
   #endregion

   #region private
   #endregion
   public AudioSource PlayeAudio (AudioPlaying thisAudio)
   {
      AudioToken _aud;

      if (dictAudio.TryGetValue (thisAudio, out _aud))
      {
         if (_aud.AttachOnTransform)
         {
            return Manager.Audio.OpenAudio (_aud, thisTrans);
         }
         else
         {
            return Manager.Audio.OpenAudio (_aud);
         }
      }

      return null;
   }

   public void StopAudio (AudioPlaying thisAudio)
   {
      AudioToken _aud;

      if (dictAudio.TryGetValue (thisAudio, out _aud))
      {
         Manager.Audio.CloseThisAudio (_aud.ThisType, _aud.CategorieName, _aud.ThisName);
      }
   }
}
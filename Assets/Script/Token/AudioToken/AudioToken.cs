﻿using UnityEngine;

public struct AudioToken
{
	#region Variables
	public AudioType ThisType;
	public AudioAction AudioAct;
	public System.Action ThisAct;

	public CustomString ThisName;
	public CustomString CategorieName;
	public bool LoopAudio;

	public bool AttachOnTransform;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes

	#endregion

	#region Private Methodes
	#endregion
}
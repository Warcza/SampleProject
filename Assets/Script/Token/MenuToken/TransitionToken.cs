﻿using UnityEngine;

public class TransitionToken : MenuTokenAbstract 
{
	#region Variables
	public override TokenType TokenT
	{
		get
		{
			return TokenType.Transition;
		}
	}
    #endregion

    #region Mono
    #endregion

    #region Public Methodes

    #endregion

    #region Private Methodes

    #endregion
}

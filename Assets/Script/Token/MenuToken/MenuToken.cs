﻿using UnityEngine;

public class MenuToken : MenuTokenAbstract 
{
	#region Variables
	public override TokenType TokenT
	{
		get
		{
			return TokenType.Menu;
		}
	}

	public System.Action ThisAction;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes

	#endregion

	#region Private Methodes

	#endregion
}

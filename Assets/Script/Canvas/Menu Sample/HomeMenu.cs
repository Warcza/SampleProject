﻿using UnityEngine;

public class HomeMenu : UiParent
{
	#region Variables
	public override MenuType ThisType
	{
		get
		{
			return MenuType.Home;
		}
	}

	[SerializeField] float timePlacement;

	[SerializeField] Transform [ ] AllButton;

	Vector3 [ ] getPos;
	Vector2 getRect;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public void PlayButton ( )
	{
		if (IsActive)
		{
			IsActive = false;
			Manager.UI.DisplayMenu (MenuType.Mode);
		}
	}

	public void CreditButton ( )
	{
		if (IsActive)
		{
			IsActive = false;
			Manager.UI.DisplayMenu (MenuType.Credit);
		}
	}

	public void ScoreButton ( )
	{
		if (IsActive)
		{
			IsActive = false;
			Manager.UI.DisplayMenu (MenuType.Score);
		}
	}

	public void NewMenuButton ( )
	{
		if (IsActive)
		{
			IsActive = false;
			Manager.UI.DisplayMenu (MenuType.Bonus);
		}
	}
	#endregion

	#region Private Methodes
	#region Public Methodes
	public override void OpenThis (MenuTokenAbstract GetTok = null)
	{
		base.OpenThis (GetTok);

		Manager.Async.randPos (AllButton, getRect);
		Manager.Async.UisPosition (AllButton, getPos, timePlacement, enableButton);

		if (GetTok != null)
		{
			MenuToken getMenuTok = (MenuToken)GetTok;
			Manager.Async.ChangeColor (AllButton, timePlacement, getMenuTok.ThisAction, 100, null, Manager.Async.AllColor);
			Manager.Garbage.AddGarbageClass (getMenuTok);
		}
		else
		{
			Manager.Async.ChangeColor (AllButton, timePlacement, null, 100, null, Manager.Async.AllColor);
		}
	}

	public override void CloseThis ( )
	{
		base.CloseThis ( );

		setButton (false, AllButton);
	}
	#endregion

	#region Private Methodes
	protected override void InitializeUi ( )
	{
		getPos = new Vector3 [AllButton.Length];
		getRect = Manager.UI.RectScreenSize * 0.51f;

		initPosRectButton ( );

		int getLength = AllButton.Length;
		for (int a = 0; a < getLength; a++)
		{
			getPos [a] = AllButton [a].localPosition;
		}

		setButton (false, AllButton);
	}

	void initPosRectButton ( )
	{
		Vector2 getRect = Manager.UI.RectScreenSize * 0.51f;
		for (int a = 0; a < 4; a++)
		{
			AllButton [a].GetComponent<RectTransform> ( ).sizeDelta = getRect;
			Manager.Async.UpdateScaleChild (AllButton [a]);
		}

		getRect *= 0.5f;
		AllButton [0].localPosition = new Vector2 (-getRect.x, getRect.y) * Constants.F_RatioLess;
		AllButton [1].localPosition = new Vector2 (getRect.x, getRect.y) * Constants.F_RatioLess;
		AllButton [2].localPosition = new Vector2 (-getRect.x, -getRect.y) * Constants.F_RatioLess;
		AllButton [3].localPosition = new Vector2 (getRect.x, -getRect.y) * Constants.F_RatioLess;
	}

	void enableButton ( )
	{
		setButton (true, AllButton);
	}
	#endregion
	#endregion
}
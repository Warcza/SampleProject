﻿using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;

public class OverMenu : UiParent
{
	#region Variables
	public override MenuType ThisType
	{
		get
		{
			return MenuType.Over;
		}
	}

	[SerializeField] float timePlacement;
	[SerializeField] GameObject Perfect;
	[SerializeField] Sprite NotPerfectImg;
	[SerializeField] Sprite PerfectImg;
	[SerializeField] Text TotalScore;

	[SerializeField] Transform [ ] AllButton;
	[SerializeField] Text [ ] ScoreType;

	Vector3 [ ] getPos;

	int [ ] currScore;
	int [ ] getScores;

	Vector2 getRect;
	MenuToken thisMenu;

	int totalScore;
	bool checkEnable;
	bool checkSearch;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public override void OpenThis (MenuTokenAbstract GetTok = null)
	{
		base.OpenThis (GetTok);

		//GameOverToken getOverTok = (GameOverToken)GetTok;
		getScores = new int[]{0,1,2,3,3,3,1,0,0,1,2,3,3,3};
		checkSearch = true;

		checkEnable = false;

		int getLength = currScore.Length;
		for (int a = 0; a < getLength; a++)
		{
			currScore [a] = 0;
			ScoreType [a].text = "0";
		}
		totalScore = 0;
		TotalScore.text = "0";

		Perfect.SetActive (false);

		Manager.Async.randPos (AllButton, getRect, true);
		Manager.Async.ChangeColor (AllButton, timePlacement, null, 100, null, Manager.Async.AllColor);
		Manager.Async.UisPosition (AllButton, getPos, timePlacement, enableButton);
	}

	public override void CloseThis ( )
	{
		base.CloseThis ( );
	}

	public void BackHome ( )
	{
		if (checkEnable)
		{
			checkEnable = false;
			Manager.UI.DisplayMenu (MenuType.Home, thisMenu);
		}
	}
	#endregion

	#region Private Methodes
	protected override void InitializeUi ( )
	{
		getPos = new Vector3 [AllButton.Length];
		currScore = new int [ScoreType.Length];
		thisMenu = new MenuToken ( );
		thisMenu.ThisAction = CloseThis;

		initPosRectButton ( );

		getRect = AllButton [0].GetComponent<RectTransform> ( ).sizeDelta;
		getRect = new Vector2 (0, getRect.y);

		int getLength = AllButton.Length;
		for (int a = 0; a < getLength; a++)
		{
			getPos [a] = AllButton [a].localPosition;
		}
	}

	void initPosRectButton ( )
	{
		getRect = Manager.UI.RectScreenSize;
		getRect = new Vector2 (getRect.x * 0.25f, getRect.y * 0.5f);

		for (int a = 0; a < 4; a++)
		{
			AllButton [a].GetComponent<RectTransform> ( ).sizeDelta = getRect;
			AllButton [a].localPosition = new Vector2 (getRect.x * (a - 1.5f), getRect.y * 0.5f) * Constants.F_RatioLess;
			Manager.Async.UpdateScaleChild (AllButton [a]);
		}

		getRect = new Vector2 (getRect.x * 4, getRect.y);
		AllButton [4].GetComponent<RectTransform> ( ).sizeDelta = getRect;
		AllButton [4].localPosition = new Vector2 (0, -getRect.y * 0.5f);

		int length = AllButton [4].childCount;
		for (int a = 0; a < length; a++)
		{
			AllButton [4].GetChild (a).localScale *= Manager.UI.RatioScreen.x;
		}
	}

	void enableButton ( )
	{
		checkEnable = true;
		Manager.UI.CloseMenu (MenuType.Mode);
		scoreCalcul ( );
	}

	void checkMaxScore ( )
	{
		if (!checkSearch)
		{
			return;
		}
	}

	async void scoreCalcul ( )
	{
		int getLength = Mathf.Clamp (getScores.Length, 1, getScores.Length);
		int getIndex;

		try
		{
			int getWait = Mathf.Clamp (1000 / getLength, 1, 100);
			int add = (int)(getLength * 0.01f);
			int countAdd = 0;

			for (int a = 0; a < getLength; a++)
			{
				if (countAdd < add)
				{
					countAdd++;
				}
				else
				{
					await Task.Delay (getWait);
					countAdd = 0;
				}
				getIndex = getScores [a];
				currScore [getIndex] += 1;
				ScoreType [getIndex].text = currScore [getIndex].ToString ( );

				totalScore += 25 * (getIndex + 1);
				TotalScore.text = totalScore.ToString ( );
			}

			checkMaxScore ( );
		}
		catch
		{
			getLength = getScores.Length;
			for (int a = 0; a < getLength; a++)
			{
				getIndex = getScores [a];
				currScore [getIndex] += 1;
				ScoreType [getIndex].text = currScore [getIndex].ToString ( );

				totalScore += 25 * (getIndex + 1);
				TotalScore.text = totalScore.ToString ( );
			}

			checkMaxScore ( );
		}
	}
	#endregion
}
﻿using UnityEngine;
using UnityEngine.UI;

public class ModeMenu : UiParent
{
	#region Variables
	public override MenuType ThisType
	{
		get
		{
			return MenuType.Mode;
		}
	}

	[SerializeField] Image ContainerImg;
	[SerializeField] Sprite ValideImg;
	[SerializeField] Sprite BackImg;
	[SerializeField] float timePlacement;

	[Space]
	[Header ("References")]
	[SerializeField] Transform [ ] AllButton;
	[SerializeField] GameObject [ ] LockMode;
	[SerializeField] ContainerDetail [ ] AllContainer;

	Vector3 [ ] getPos;
	Vector2 getRect;

	int saveIDLock;

	bool checkContainer;

	struct infoDate
	{
		public int Year;
		public int Month;
		public int Day;
	}
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public override void OpenThis (MenuTokenAbstract GetTok = null)
	{
		base.OpenThis (GetTok);

		Manager.Async.randPos (AllButton, getRect);
		Manager.Async.ChangeColor (AllButton, timePlacement, null, 100, null, Manager.Async.AllColor);
		Manager.Async.UisPosition (AllButton, getPos, timePlacement, enableButton);
	}

	public override void CloseThis ( )
	{
		base.CloseThis ( );

		ContainerImg.sprite = ValideImg;

		if (checkContainer)
		{
			OpenDetailMenu (true);
		}

		int getLength = AllButton.Length;

		for (int a = 0; a < getLength; a++)
		{
			AllButton [a].localPosition = getPos [a];
		}

		setButton (false, AllButton);
	}

	public void Standard ( )
	{
		if (IsActive)
		{
			IsActive = false;
			Manager.UI.DisplayMenu(MenuType.Over);
		}
	}

	public void GodMode ( )
	{
		if (IsActive)
		{
			IsActive = false;
			Manager.UI.DisplayMenu(MenuType.Over);
		}
	}

	public void WhiteBlack ( )
	{
		if (IsActive)
		{
			IsActive = false;
			Manager.UI.DisplayMenu(MenuType.Over);
		}
	}

	public void OpenDetailMenu (bool instant = false)
	{
		checkContainer = !checkContainer;

		if (checkContainer)
		{
			ContainerImg.sprite = BackImg;
		}
		else
		{
			ContainerImg.sprite = ValideImg;
		}

		for (int a = 0; a < AllContainer.Length; a++)
		{
			AllContainer [a].UseContainer (checkContainer, instant);
		}
	}
	#endregion

	#region Private Methodes
	protected override void InitializeUi ( )
	{
		getPos = new Vector3 [AllButton.Length];

		initPosRectButton ( );

		int getLength = LockMode.Length;

		getLength = AllButton.Length;
		getRect = AllButton [0].GetComponent<RectTransform> ( ).sizeDelta;

		for (int a = 0; a < getLength; a++)
		{
			getPos [a] = AllButton [a].localPosition;
		}

		setButton (false, AllButton);
	}

	void initPosRectButton ( )
	{
		Vector2 getSize = Manager.UI.RectScreenSize * 1.01f;
		AllButton [0].GetComponent<RectTransform> ( ).sizeDelta = new Vector2 (getSize.x * 0.5f, getSize.y * 0.5f);
		AllButton [1].GetComponent<RectTransform> ( ).sizeDelta = new Vector2 (getSize.x * 0.5f, getSize.y * 0.5f);
		AllButton [2].GetComponent<RectTransform> ( ).sizeDelta = new Vector2 (getSize.x, getSize.y * 0.5f);

		AllButton [0].localPosition = new Vector2 (-getSize.x * 0.25f, -getSize.y * 0.25f) * Constants.F_RatioLess;
		AllButton [1].localPosition = new Vector2 (getSize.x * 0.25f, -getSize.y * 0.25f) * Constants.F_RatioLess;
		AllButton [2].localPosition = new Vector2 (0, getSize.y * 0.25f) * Constants.F_RatioLess;

		Manager.Async.UpdateScaleChild (AllButton [0]);
		Manager.Async.UpdateScaleChild (AllButton [1]);
		Manager.Async.UpdateScaleChild (AllButton [2]);
	}

	void enableButton ( )
	{
		Manager.UI.CloseMenu (1);
		setButton (true, AllButton);
	}

	async void waitActive ( )
	{
		await System.Threading.Tasks.Task.Delay (50);

		IsActive = true;
	}
	#endregion
}
﻿using System.Threading;

using UnityEngine;
using UnityEngine.UI;

public class ContainerDetail : MonoBehaviour
{
	#region Variables
	[SerializeField] float timePlacement = 1;

	[Space]
	[SerializeField] int sampleNbrElement = 1;
	[SerializeField] ModeMenu parentMenu;

	[Space]
	[SerializeField] Slider thisSlider;
	[SerializeField] Sprite Standard;
	[SerializeField] Sprite Perfect;

	[SerializeField] float startBack = 1;

	infoButton [ ] buttonsInfo;
	Transform [ ] allImg;
	Vector3 StartPos;
	Vector3 NewPos;

	CancellationTokenSource tok;

	float getSize;
	float getSize2;

	int index;
	bool checkContainer = false;

	[System.Serializable]
	struct infoButton
	{
		public Button ThisButton;
		public GameObject LockObj;
		public Image PerfectImg;
		public Text Score;
		public Text NameMusic;

	}
	#endregion

	#region Mono
	void Awake ( )
	{

		Transform thisTrans = transform;
		tok = new CancellationTokenSource ( );

		getSize = Manager.UI.RectScreenSize.x * 0.5f;
		getSize2 = GetComponent<RectTransform> ( ).sizeDelta.x * thisTrans.localScale.x;
		int dir = 1;

		if (startBack > 0)
		{
			dir = -1;
		}

		thisTrans.localPosition = new Vector2 (getSize * startBack - getSize2 * 0.5f * dir, 0);

		Transform currButton;
		int length = 3;
		allImg = new Transform [length];
		buttonsInfo = new infoButton [length];

		for (int a = 0; a < length; a++)
		{
			currButton = thisTrans.GetChild (a);
			allImg [a] = currButton;

			buttonsInfo [a].NameMusic = currButton.GetChild (0).GetComponent<Text> ( );
			buttonsInfo [a].Score = currButton.GetChild (1).GetComponent<Text> ( );
			buttonsInfo [a].PerfectImg = currButton.GetChild (2).GetComponent<Image> ( );
			buttonsInfo [a].LockObj = currButton.GetChild (3).gameObject;

			buttonsInfo [a].ThisButton = currButton.GetComponent<Button> ( );
		}

		for (int a = 0; a < length; a++)
		{
			allImg [a] = buttonsInfo [a].ThisButton.transform;
		}

		thisSlider.minValue = 0;
		thisSlider.maxValue = sampleNbrElement - 3;

		StartPos = thisTrans.localPosition;
		NewPos = thisTrans.localPosition - new Vector3 (getSize * startBack - getSize2 * 0.5f * dir, 0, 0);
		gameObject.SetActive (false);
	}
	#endregion

	#region Public Methodes
	public void UseContainer (bool enable, bool instant = false)
	{
		index = 0;

		if (enable)
			gameObject.SetActive (true);

		if (tok != null && tok.Token.CanBeCanceled)
		{
			tok.Cancel ( );
		}

		if (instant && !enable)
		{
			transform.localPosition = StartPos;
			return;
		}

		tok = new CancellationTokenSource ( );

		if (enable)
		{
			setMusic (0);
			gameObject.SetActive (true);
			Manager.Async.ChangeColor (allImg, timePlacement, null, 100, null, Manager.Async.AllColor, null, tok);
		}

		if (enable)
		{
			Manager.Async.UiPosition (transform, NewPos, timePlacement, null, 100, null, tok);
		}
		else
		{
			if (!instant)
			{
				Manager.Async.UiPosition (transform, StartPos, timePlacement, disableObj, 100, null, tok);
			}
		}
		checkContainer = enable;
	}

	public void NextItem ( )
	{
		if (index < thisSlider.maxValue)
		{
			index++;
			setMusic (index);
		}
	}

	public void PreviousItem ( )
	{
		if (index > 0)
		{
			index--;
			setMusic (index);
		}
	}

	public void LaunchCustomMusic (int indexButton)
	{
		Manager.UI.DisplayMenu (MenuType.Over);
	}

	#endregion

	#region Private Methodes
	void setMusic (int index)
	{
		thisSlider.value = index;
		for (int a = 0; a < buttonsInfo.Length; a++)
		{
			if (index + a >= sampleNbrElement)
			{
				buttonsInfo [a].NameMusic.text = string.Empty;
				buttonsInfo [a].Score.text = string.Empty;
				buttonsInfo [a].PerfectImg.enabled = false;
				buttonsInfo [a].LockObj.SetActive (false);
				continue;
			}

			buttonsInfo [a].PerfectImg.enabled = true;
			buttonsInfo [a].NameMusic.text = "test " + (a + index).ToString ( );
			buttonsInfo [a].Score.text = "testB " + (a + index).ToString ( );
			buttonsInfo [a].LockObj.SetActive (false);

			if (true)
			{
				buttonsInfo [a].PerfectImg.sprite = Perfect;
			}
			else
			{
				buttonsInfo [a].PerfectImg.sprite = Standard;
			}
		}
	}

	void disableObj ( )
	{
		gameObject.SetActive (false);
		if (checkContainer)
		{
			checkContainer = false;
			transform.localPosition = StartPos;
		}
	}
	#endregion
}
﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;

using UnityEngine;
using UnityEngine.UI;

public class BonusMenu : UiParent
{
	#region Variables
	public override MenuType ThisType
	{
		get
		{
			return MenuType.Bonus;
		}
	}

	[SerializeField] float timePlacement;
	[SerializeField] Transform [ ] AllButton;
	[SerializeField] Transform BackButton;

	Vector3 [ ] getPos;
	Vector2 getRect;

	AudioClip saveClip;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public override void OpenThis (MenuTokenAbstract GetTok = null)
	{
		base.OpenThis (GetTok);

		Manager.Async.randPos (AllButton, getRect);
		Manager.Async.UisPosition (AllButton, getPos, timePlacement, enableButton);

		if (GetTok != null)
		{
			MenuToken getMenuTok = (MenuToken)GetTok;
			Manager.Async.ChangeColor (AllButton, timePlacement, getMenuTok.ThisAction, 100, null, Manager.Async.AllColor);
			Manager.Garbage.AddGarbageClass (getMenuTok);
		}
		else
		{
			Manager.Async.ChangeColor (AllButton, timePlacement, null, 100, null, Manager.Async.AllColor);
		}
	}

	public override void CloseThis ( )
	{
		base.CloseThis ( );

		setButton (false, AllButton);
	}

	public void BackHome ( )
	{
		MenuToken thisTok = new MenuToken ( );
		thisTok.ThisAction = closeThisMenu;
		Manager.UI.DisplayMenu (MenuType.Home, thisTok);
	}
	#endregion

	#region Private Methodes
	protected override void InitializeUi ( )
	{
		getPos = new Vector3 [AllButton.Length];
		getRect = AllButton [0].GetComponent<RectTransform> ( ).sizeDelta;

		initPosRectButton ( );

		int getLength = AllButton.Length;
		for (int a = 0; a < getLength; a++)
		{
			getPos [a] = AllButton [a].localPosition;
		}

		setButton (false, AllButton);
	}

	void initPosRectButton ( )
	{
		Vector2 currRect = Manager.UI.RectScreenSize;
		for (int a = 0; a < 3; a++)
		{
			Manager.Async.UpdateScaleChild (AllButton [a]);
		}

		AllButton [0].GetComponent<RectTransform> ( ).sizeDelta = new Vector2 (currRect.x * 0.5f, currRect.y);
		AllButton [1].GetComponent<RectTransform> ( ).sizeDelta = new Vector2 (currRect.x * 0.5f, currRect.y * 0.5f + 1);
		AllButton [2].GetComponent<RectTransform> ( ).sizeDelta = new Vector2 (currRect.x * 0.5f, currRect.y * 0.5f + 1);

		currRect *= 0.5f;
		AllButton [0].localPosition = new Vector2 (-currRect.x * 0.5f, 0) * Constants.F_RatioLess;
		AllButton [1].localPosition = new Vector2 (currRect.x * 0.5f, -currRect.y * 0.5f) * Constants.F_RatioLess;
		AllButton [2].localPosition = new Vector2 (currRect.x * 0.5f, currRect.y * 0.5f) * Constants.F_RatioLess;

		Vector2 sizeButton = BackButton.GetComponent<RectTransform> ( ).sizeDelta * 0.5f * BackButton.localScale.x;
		BackButton.localPosition = new Vector2 (currRect.x * 0.5f, -currRect.y * 0.5f) * 0.9f + new Vector2 (-sizeButton.x, sizeButton.y);
		getRect = AllButton [0].GetComponent<RectTransform> ( ).sizeDelta;
	}

	void enableButton ( )
	{
		Manager.UI.CloseMenu (MenuType.Home);
		setButton (true, AllButton);
	}

	void closeThisMenu ( )
	{
		Manager.UI.CloseMenu (MenuType.Bonus);
	}
	#endregion
}
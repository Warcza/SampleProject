﻿using UnityEngine;
using UnityEngine.UI;

public class TransitionMenu : UiParent
{
	#region Variables
	public override MenuType ThisType
	{
		get
		{
			return MenuType.Transition;
		}
	}

    public float PourcTransition { get => PourcTransition; set => PourcTransition = value; }

	[SerializeField] Slider thisSlider;
	#endregion

	#region Mono
	void Update ( )
	{
		if (IsActive)
		{
			thisSlider.value = Manager.Scene.LoadingPourc;
		}
	}
	#endregion

	#region Public Methodes
	public override void OpenThis (MenuTokenAbstract GetTok = null)
	{
		base.OpenThis(GetTok);
		//TransitionToken Ttok = (TransitionToken)GetTok;
	}
	#endregion

	#region Private Methodes
	protected override void InitializeUi ( )
	{

	}
	#endregion
}
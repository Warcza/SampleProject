﻿using UnityEngine;

public static class MathFunctions
{
	#region Simple Follow
	public static Vector3 CubeFollow (Vector3 Value, Vector3 Distance)
	{
		float calCosValueX = Mathf.Cos (Value.x);
		float calSinValueZ = Mathf.Sin (Value.z);
		float calSinValueY = Mathf.Sin (Value.y);

		return new Vector3 (Distance.x * calCosValueX, Distance.y * calSinValueY, Distance.z * calSinValueZ);
	}
	#endregion

	#region Use Cartesian
	public static Vector3 ToPolarPosition (Vector3 cartesianOrigin, Vector3 cartesianPosition)
	{
		return ToPolarPosition (cartesianPosition - cartesianOrigin);
	}

	public static Vector3 ToPolarPosition (Vector3 cartesianPosition)
	{
		if (cartesianPosition == Vector3.zero)
		{
			cartesianPosition = Vector3.one;
		}

		Vector3 result = Vector3.zero;

		result.x = cartesianPosition.magnitude;
		result.y = Mathf.Acos (-cartesianPosition.y / result.x);
		result.z = Mathf.Atan2 (cartesianPosition.z, cartesianPosition.x);
		return result;
	}

	public static Vector3 SphereToCartesian (Vector3 Origin, Vector3 Position, bool withVelocitie)
	{
		Vector3 getValue = Origin - Position;

		if (getValue == Vector3.zero)
		{
			return Position;
		}

		float x = getValue.x;
		float y = getValue.y;
		float z = getValue.z;

		float dist = Mathf.Sqrt (x * x + y * y + z * z);
		float teta = Mathf.Atan2 (y, x);
		float getO = Mathf.Asin (z / dist);

		x = dist * Mathf.Sin (getO) * Mathf.Cos (teta);
		y = dist * Mathf.Cos (getO) * Mathf.Sin (teta);
		z = dist * Mathf.Cos (getO);

		if (withVelocitie)
		{
			return VelocitieOnSphere (dist, teta, getO, new Vector3 (x, y, z), getValue);
		}

		return new Vector3 (x, y, z);
	}

	public static Vector3 VelocitieOnSphere (float dist, float teta, float getO, Vector3 cartesianPos, Vector3 velocitie)
	{
		float x = cartesianPos.x;
		float y = cartesianPos.y;
		float z = cartesianPos.z;

		float x2 = velocitie.x;
		float y2 = velocitie.y;
		float z2 = velocitie.z;

		float dist2 = (x * x2 + y * y2 + z * z2) / Mathf.Sqrt (x * x + y * y + z * z);
		float teta2 = (x2 * y - x * y2) / (x * x + y * y);
		float getO2 = (z * (x * x2 + y * y2) - (x * x + y * y) * z2) / ((x * x + y * y + z * z) * Mathf.Sqrt (x * x + y * y));

		x2 = Mathf.Cos (getO) * Mathf.Cos (teta * dist2) + dist * Mathf.Cos (teta2) * Mathf.Sin (teta * teta2) + dist * Mathf.Sin (getO) * Mathf.Cos (teta * getO2);
		y2 = Mathf.Cos (getO) * Mathf.Sin (teta * dist2) - dist * Mathf.Cos (teta2) * Mathf.Cos (teta * teta2) + dist * Mathf.Sin (getO) * Mathf.Sin (teta * getO2);
		z2 = Mathf.Sin (getO * dist2) - dist * Mathf.Cos (getO * getO2);

		return new Vector3 (x2, y2, z2);
	}

	public static Vector3 ToRealPosition (Vector3 cartesianOrigin, Vector3 sphericalPosition)
	{
		return ToRealPosition (sphericalPosition) + cartesianOrigin;
	}

	public static Vector3 ToRealPosition (Vector3 cartesianPosition)
	{
		Vector3 result = Vector3.zero;
		result.x = cartesianPosition.x * Mathf.Sin (cartesianPosition.y) * Mathf.Cos (cartesianPosition.z);
		result.y = -cartesianPosition.x * Mathf.Cos (cartesianPosition.y);
		result.z = cartesianPosition.x * Mathf.Sin (cartesianPosition.y) * Mathf.Sin (cartesianPosition.z);
		return result;
	}

	public static Vector3 ToSphericalPosition (Vector3 cartesianOrigin, Vector3 cartesianPosition)
	{
		return ToSphericalPosition (cartesianPosition - cartesianOrigin);
	}

	public static Vector3 ToSphericalPosition (Vector3 cartesianPosition)
	{
		Vector3 result = Vector3.zero;
		result.x = cartesianPosition.magnitude;
		result.y = Mathf.Atan2 (cartesianPosition.y, cartesianPosition.x);
		result.z = Mathf.Asin (cartesianPosition.z / result.x);
		return result;
	}

	public static Vector3 ToSphericalVelocity (Vector3 cartesianOrigin, Vector3 cartesianVelocity, Vector3 cartesianPosition)
	{
		return ToSphericalVelocity (cartesianVelocity, cartesianPosition - cartesianOrigin);
	}

	public static Vector3 ToSphericalVelocity (Vector3 cartesianVelocity, Vector3 cartesianPosition)
	{
		Vector3 result = Vector3.zero;

		float xxp = cartesianPosition.x * cartesianVelocity.x;
		float yyp = cartesianPosition.y * cartesianVelocity.y;
		float zzp = cartesianPosition.z * cartesianVelocity.z;

		float x2 = cartesianPosition.x * cartesianPosition.x;
		float y2 = cartesianPosition.y * cartesianPosition.y;
		float z2 = cartesianPosition.z * cartesianPosition.z;

		result.x = (xxp + yyp + zzp) / cartesianPosition.magnitude;
		result.y = (cartesianVelocity.x * cartesianPosition.y - cartesianPosition.x * cartesianVelocity.y) / (x2 + y2);
		result.z = (cartesianPosition.z * (xxp + yyp) - cartesianVelocity.z * (x2 + y2)) / ((x2 + y2 + z2) * Mathf.Sqrt (x2 + y2));
		return result;
	}

	public static Vector3 ToCartesianPosition (Vector3 cartesianOrigin, Vector3 sphericalPosition)
	{
		return ToCartesianPosition (sphericalPosition) + cartesianOrigin;
	}

	public static Vector3 ToCartesianPosition (Vector3 sphericalPosition)
	{
		Vector3 result = Vector3.zero;
		result.x = sphericalPosition.x * Mathf.Cos (sphericalPosition.y) * Mathf.Cos (sphericalPosition.z);
		result.y = sphericalPosition.x * Mathf.Sin (sphericalPosition.y) * Mathf.Cos (sphericalPosition.z);
		result.z = sphericalPosition.x * Mathf.Sin (sphericalPosition.z);
		return result;
	}

	public static Vector3 ToCartesianVelocity (Vector3 cartesianOrigin, Vector3 sphericalVelocity, Vector3 sphericalPosition)
	{
		return ToCartesianVelocity (sphericalVelocity, sphericalPosition);
	}

	public static Vector3 ToCartesianVelocity (Vector3 sphericalVelocity, Vector3 sphericalPosition)
	{
		Vector3 result = Vector3.zero;

		float cosPhiCosThe = Mathf.Cos (sphericalPosition.z) * Mathf.Cos (sphericalPosition.y);
		float cosPhiSinThe = Mathf.Cos (sphericalPosition.z) * Mathf.Sin (sphericalPosition.y);
		float sinPhiCosThe = Mathf.Sin (sphericalPosition.z) * Mathf.Cos (sphericalPosition.y);
		float sinPhiSinThe = Mathf.Sin (sphericalPosition.z) * Mathf.Sin (sphericalPosition.y);

		result.x = cosPhiCosThe * sphericalVelocity.x + sphericalPosition.x * sphericalVelocity.y * cosPhiSinThe + sphericalPosition.x * sphericalVelocity.z * sinPhiCosThe;
		result.y = cosPhiSinThe * sphericalVelocity.x - sphericalPosition.x * sphericalVelocity.y * cosPhiCosThe + sphericalPosition.x * sphericalVelocity.z * sinPhiSinThe;
		result.z = Mathf.Sin (sphericalPosition.z) * sphericalVelocity.x - sphericalPosition.x * Mathf.Cos (sphericalPosition.z) * sphericalVelocity.z;

		return result;
	}
	#endregion

	#region Test
	/* ---------------------------------------- TEST ---------------------------------------- */

	public static Vector3 SphereCordial (Vector3 origin, Vector3 pos, Vector3 addMove)
	{
		Vector3 getVect = MathFunctions.ToPolarPosition (origin, pos);
		getVect += addMove;
		getVect = MathFunctions.ToRealPosition (origin, getVect);

		return getVect;
	}
	#endregion
}
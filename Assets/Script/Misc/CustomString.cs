﻿#if UNITY_EDITOR
using UnityEditor;

#endif
using UnityEngine;

[System.Serializable]
public struct CustomString
{
#if UNITY_EDITOR
	public string SaveString;
#endif	

	public int StringValue;

	public static int GetHashFromString (string thisString)
	{
		return Animator.StringToHash (thisString);
	}
}

#if UNITY_EDITOR
[CustomPropertyDrawer (typeof (CustomString))]
public class CustomStringEditor : PropertyDrawer
{
	SerializedProperty text, newString;
	string getString;
	string name;
	bool cache = false;

	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		if (!cache)
		{
			//get the name before it's gone
			name = property.displayName;

			//get the X and Y values
			property.NextVisible (true);
			text = property.Copy ( );

			property.NextVisible (true);
			newString = property.Copy ( );

			getString = text.stringValue;

			cache = true;
		}
		SerializedObject so = property.serializedObject;

		Rect contentPosition = EditorGUI.PrefixLabel (position, new GUIContent (name));
		//Check if there is enough space to put the name on the same line (to save space)
		if (position.height > 18)
		{
			position.height = 18;
			EditorGUI.indentLevel += 1;
			contentPosition = EditorGUI.IndentedRect (position);
			contentPosition.y += 18;
		}

		float half = contentPosition.width / 2;

		EditorGUIUtility.labelWidth = 18;

		contentPosition.width *= 0.9f;
		EditorGUI.indentLevel = 0;

		// Begin/end property & change check make each field
		// behave correctly when multi-object editing.
		EditorGUI.BeginProperty (contentPosition, label, text);
		{
			EditorGUI.BeginChangeCheck ( );
			getString = EditorGUI.TextField (contentPosition, getString, new GUIStyle (EditorStyles.textArea));

			if (EditorGUI.EndChangeCheck ( ))
			{
				newString.intValue = Animator.StringToHash (getString);
				text.stringValue = getString;
			}
		}

		EditorGUI.EndProperty ( );

		//EditorGUI.PropertyField (contentPosition, text, label, true);
		so.ApplyModifiedProperties ( );
	}

	public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
	{
		return Screen.width < 333 ? (16f + 18f) : 16f;
	}
}
#endif
﻿public enum TokenType
{
	Deactivate,
	OpenAudio,
	Transition,
	Menu
}

public enum AudioAction
{
	Nothing,
	Renew,
	PlayIfEmpty
}

public enum RandomType
{
	Standard,
	Clamp,
	Nothing
}

public enum SceneType
{

}

public enum GameMode
{
	Standard
}

public enum RoomType
{
	Standard,
	Puzzle,
	Transition,
	Special,
	Observ
}

public enum MenuType
{
	None,
	Transition,
	RoomNav,

	// Sample menu
	Mode,
	Credit,
	Bonus,
	Score,
	Over,
	Home
}

public enum ObjectType
{
	Nothing,
	Door,
	Key,
	Table

}

public enum AudioType
{
	MBG,
	FX,
	Other
}

public enum FlipMode
{
	RightToLeft,
	LeftToRight
}

public enum AudioPlaying
{
	Jump,
	BigJump,
	Hit,
	DashDown,
	DashForward,
	GameOver,
	Crunch,
	MusicMenu,
	MusicGame,
	Bonus,
}
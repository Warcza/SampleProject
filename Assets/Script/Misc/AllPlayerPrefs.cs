﻿using UnityEngine;

public static class AllPlayerPrefs
{
	#region Public Methods
	public static int GetIntValue (string thisString, int defValue = 1)
	{
		return PlayerPrefs.GetInt (thisString, defValue);
	}

	public static float GetFloatValue (string thisString, float defValue = 1)
	{
		return PlayerPrefs.GetFloat (thisString, defValue);
	}
	#endregion
}
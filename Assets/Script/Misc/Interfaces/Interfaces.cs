﻿using UnityEngine;

public interface ClearStuffInterface
{
	void ClearEverything ( );
}

public interface GameInfo
{
	void SpeedGame (float newSpeed);
	void GameStatut (bool isEnable);
}

public interface PlayAudio
{
	AudioSource PlayeAudio (AudioPlaying thisAudio);
	void StopAudio (AudioPlaying thisAudio);
}
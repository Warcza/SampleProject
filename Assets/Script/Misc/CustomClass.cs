﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ------------------ ScriptLevelInfo
[System.Serializable]
public class RoomTypeSpawn 
{
	public RoomType ThisType;
	[Range (0,100)]
	public int PourcentageSpawn = 100;
	public int DepthStart = 0;
}
// ------------------

// ------------------ GameController
[System.Serializable]
public class RoomByType
{
	public int LevelRoom;
	public List<RoomsInfo> RoomSort;
}

[System.Serializable]
public class RoomsInfo 
{
	public RoomType ThisType;
	public List <ScriptRoomSetting> Rooms;
}
// ------------------



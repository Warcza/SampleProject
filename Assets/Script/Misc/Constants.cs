﻿using System.Collections.Generic;

using UnityEngine;

public static class Constants
{
	#region Scene
	#endregion

	#region Tag
	#endregion

	#region Layer
	#endregion

	#region others
	public const string _MainParentRoom = "MainParent";
	public const float F_RatioLess = 0.9965f;
	#endregion

	#region playerPrefs
	#endregion
}
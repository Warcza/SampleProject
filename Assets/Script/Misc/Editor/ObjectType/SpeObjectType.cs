﻿using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEditor;

using UnityEngine;

public class SpeObjectType : EditorWindow
{
	#region Variables
	Dictionary<ElementType, List<infoObject>> ElemList;
	Dictionary<TypeScriptable, List<PhysicMaterial>> selectObject;

	TypeScriptable [ ] allObjectType;
	newObjInfo [ ] ObjLoad;
	infoInput [ ] getInput;

	CurrentMenu thisMenu;
	ElementType currType;
	PhysicMaterial lastObj;
	Object SpecificPath;

	System.Array getArray;
	Editor gameObjectEditor;
	GUIStyle defaultGuiS;

	Vector2 scrollView1;
	Vector2 scrollView2;
	Vector2 scrollView3;
	Vector2 scrollView4;

	Vector2 [ ] scrollViewEnum;

	Color defColor;

	bool foldList1;
	bool foldList2;
	bool checkAsyncLoad;
	bool checkSetEnum;
	bool checkEnumSelect;

	string getPath;
	string getResearch;

	const string T_Obj = "TypeObject";
	const string SelectAll = "SelectAll";
	const string UnSelectAll = "UnSelectAll";
	const string PathComplete = "Assets/Resources/Scriptable";

	struct newObjInfo
	{
		public PhysicMaterial ThisObj;
		public bool SelectObj;
	}
	struct infoInput
	{
		public bool FoldEnum;
		public bool ButtonEnum;
		public bool EnumValidate;
		public string TextInfo;
	}

	struct infoObject
	{
		public TypeScriptable ObjectType;
		public bool [ ] FoldObject;

		public int NbrObj;
		public ElementType OldType;
		public PhysicMaterial [ ] OldGamObject;
	}

	enum CurrentMenu
	{
		Home,
		Add,
		Load
	}
	#endregion

	#region Editor
	[MenuItem ("Window/ObjectType")]
	public static void ShowWindow ( )
	{
		EditorWindow.GetWindow (typeof (SpeObjectType));
	}

	void OnEnable ( )
	{
		getResearch = "";
		defColor = GUI.backgroundColor;
		thisMenu = CurrentMenu.Home;
		getArray = System.Enum.GetValues (typeof (ElementType));

		ElemList = new Dictionary<ElementType, List<infoObject>> (getArray.Length);
		selectObject = new Dictionary<TypeScriptable, List<PhysicMaterial>> ( );
		scrollViewEnum = new Vector2 [getArray.Length];
		getInput = new infoInput [getArray.Length];
		ObjLoad = new newObjInfo [0];

		defaultGuiS = new GUIStyle ( );

		resetVariable ( );

		int getLength = getArray.Length;
		for (int a = 0; a < getLength; a++)
		{
			getInput [a].TextInfo = SelectAll;
			ElemList.Add ((ElementType)getArray.GetValue (a), new List<infoObject> ( ));
		}
	}

	void OnGUI ( )
	{
		checkFolder ( );

		if (thisMenu != CurrentMenu.Home)
		{
			backHomeMenu ( );
		}

		switch (thisMenu)
		{
			case CurrentMenu.Home:
				homeMenu ( );
				break;

			case CurrentMenu.Add:
				addMenu ( );
				break;

			case CurrentMenu.Load:
				loadMenu ( );
				break;
		}
	}
	#endregion

	#region Public Methodes

	#endregion

	#region Private Methodes
	#region Page
	void homeMenu ( )
	{
		EditorGUILayout.Space ( );

		if (GUILayout.Button ("Add New Object(s)"))
		{
			thisMenu = CurrentMenu.Add;
			ObjLoad = new newObjInfo [0];
			currType = (ElementType)getArray.GetValue (0);
			manualyReload ( );
		}

		EditorGUILayout.Space ( );
		EditorGUILayout.Space ( );

		if (GUILayout.Button ("Load Current Object(s)"))
		{
			resetLoadMenu ( );
			thisMenu = CurrentMenu.Load;
			minSize = new Vector3 (675, 300);
		}
	}

	void addMenu ( )
	{
		EditorGUILayout.Space ( );

		EditorGUILayout.BeginHorizontal (GUILayout.Width (position.width));

		EditorGUILayout.BeginVertical (GUILayout.Width (position.width * 0.5f));
		int newCount = ObjLoad.Length;
		newCount = EditorGUILayout.IntField ("Number Reference", newCount, GUILayout.Width (position.width * 0.45f));

		if (newCount != ObjLoad.Length)
		{
			ObjLoad = (newObjInfo [ ])updateArray<newObjInfo> (newCount, ObjLoad);
		}

		if (newCount > 0)
		{

			EditorGUILayout.BeginHorizontal (GUILayout.Width (position.width * 0.45f));
			foldList1 = EditorGUILayout.Foldout (foldList1, "GameObject List (" + newCount + ")", true);
			bool checkAll = true;

			string getText = "Select All";
			int getLength = ObjLoad.Length;
			for (int a = 0; a < getLength; a++)
			{
				if (!ObjLoad [a].SelectObj)
				{
					checkAll = false;
					break;
				}
			}

			if (checkAll)
			{
				GUI.backgroundColor = Color.green;
				getText = "UnSelect All";
			}

			if (GUILayout.Button (getText, GUILayout.Width (position.width * 0.2f)))
			{
				for (int a = 0; a < getLength; a++)
				{
					ObjLoad [a].SelectObj = !checkAll;
				}
			}
			GUI.backgroundColor = defColor;

			EditorGUILayout.EndHorizontal ( );
		}

		if (foldList1) // Display Gameobject List
		{
			EditorGUI.indentLevel = 1;

			string getName;
			scrollView1 = EditorGUILayout.BeginScrollView (scrollView1, GUILayout.Height (Mathf.Clamp (22 * newCount, 0, position.height * 0.4f)));
			for (int a = 0; a < newCount; a++)
			{
				EditorGUILayout.BeginHorizontal (GUILayout.Width (position.width * 0.45f));
				ObjLoad [a].ThisObj = (PhysicMaterial)EditorGUILayout.ObjectField (ObjLoad [a].ThisObj, typeof (PhysicMaterial), true, GUILayout.Width (position.width * 0.3f));

				if (ObjLoad [a].SelectObj)
				{
					getName = "Selected";
					GUI.backgroundColor = Color.green;
				}
				else
				{
					getName = "UnSelected";
				}

				if (GUILayout.Button (getName, GUILayout.Width (position.width * 0.15f)))
				{
					ObjLoad [a].SelectObj = !ObjLoad [a].SelectObj;
				}

				GUI.backgroundColor = defColor;
				EditorGUILayout.EndHorizontal ( );
			}
			EditorGUILayout.EndScrollView ( );
			EditorGUI.indentLevel = 0;
			EditorGUILayout.Space ( );
		}

		scrollView2 = EditorGUILayout.BeginScrollView (scrollView2);
		ElementType thisType;
		newCount = getArray.Length;
		for (int a = 0; a < newCount; a++) // Element type display
		{
			thisType = (ElementType)getArray.GetValue (a);
			if (currType == thisType)
			{
				GUI.backgroundColor = Color.green;
			}

			if (GUILayout.Button (thisType.ToString ( ), GUILayout.Width (position.width * 0.45f)))
			{
				currType = thisType;
			}

			GUI.backgroundColor = defColor;
		}
		EditorGUILayout.EndScrollView ( );

		EditorGUILayout.EndVertical ( );

		EditorGUILayout.BeginVertical (GUILayout.Width (position.width * 0.45f));
		SpecificPath = EditorGUILayout.ObjectField ("Specific folder :", SpecificPath, typeof (Object), true, GUILayout.Width (position.width * 0.45f));

		if (SpecificPath != null)
		{
			getPath = AssetDatabase.GetAssetOrScenePath (SpecificPath);
		}
		else
		{
			getPath = "";
		}

		if (GUILayout.Button ("Load Object", GUILayout.Width (position.width * 0.45f)) && !checkAsyncLoad)
		{
			string [ ] GUIDs;
			if (getPath != "")
			{
				if (getPath.EndsWith ("/"))
				{
					getPath = getPath.TrimEnd ('/');
				}

				GUIDs = AssetDatabase.FindAssets ("t:PhysicMaterial", new string [ ]
				{
					getPath
				});
			}
			else
			{
				GUIDs = AssetDatabase.FindAssets ("t:PhysicMaterial");
			}

			checkAsyncLoad = true;
			loadObject (GUIDs);
		}

		if (ObjLoad.Length > 0) // Set enum to object(s)
		{
			if (!checkSetEnum && GUILayout.Button ("Set Enum to objects", GUILayout.Width (position.width * 0.45f)))
			{
				checkSetEnum = true;
			}
			else if (checkSetEnum)
			{
				EditorGUILayout.Space ( );
				EditorGUILayout.BeginVertical (GUILayout.Width (position.width * 0.45f));
				EditorGUILayout.BeginHorizontal (GUILayout.Width (position.width * 0.45f));

				if (GUILayout.Button ("On new Scriptable", GUILayout.Width (position.width * 0.225f)))
				{
					checkSetEnum = false;

					TypeScriptable getNewTS = ScriptableObjectUtility.CreateScriptType (PathComplete + "/" + T_Obj, currType.ToString ( ));
					getNewTS.GroupeType = currType;
					getNewTS.ThisObj = returnNewObj ( );

					List<TypeScriptable> setNewOnList = new List<TypeScriptable> (allObjectType);
					setNewOnList.Add (getNewTS);
					allObjectType = setNewOnList.ToArray ( );
				}

				if (GUILayout.Button ("On current Scriptable", GUILayout.Width (position.width * 0.225f)) && allObjectType.Length > 0)
				{
					bool checkNew = true;

					newCount = allObjectType.Length;
					for (int a = 0; a < newCount; a++)
					{
						if (allObjectType [a].GroupeType == currType)
						{
							checkNew = false;
							TypeScriptable getNewTS = allObjectType [a];
							List<PhysicMaterial> addObj = new List<PhysicMaterial> (getNewTS.ThisObj);
							addObj.AddRange (returnNewObj ( ));
							getNewTS.ThisObj = addObj.ToArray ( );
							break;
						}
					}

					if (checkNew)
					{
						TypeScriptable getNewTS = ScriptableObjectUtility.CreateScriptType (PathComplete + "/" + T_Obj, currType.ToString ( ));
						getNewTS.GroupeType = currType;
						getNewTS.ThisObj = returnNewObj ( );
					}
					checkSetEnum = false;
				}

				EditorGUILayout.EndHorizontal ( );

				newCount = allObjectType.Length;
				newCount = EditorGUILayout.IntField ("Number Reference", newCount, GUILayout.Width (position.width * 0.45f));

				if (newCount != allObjectType.Length)
				{
					allObjectType = (TypeScriptable [ ])updateArray<TypeScriptable> (newCount, allObjectType);
				}

				if (newCount > 0)
				{
					foldList2 = EditorGUILayout.Foldout (foldList2, "TypeScriptable List (" + newCount + ")", true);
				}
				newCount = allObjectType.Length;

				if (foldList2)
				{
					scrollView3 = EditorGUILayout.BeginScrollView (scrollView3);
					for (int a = 0; a < newCount; a++)
					{
						allObjectType [a] = (TypeScriptable)EditorGUILayout.ObjectField (allObjectType [a], typeof (TypeScriptable), true, GUILayout.Width (position.width * 0.4f));
					}
					EditorGUILayout.EndScrollView ( );
				}
			}
		}
		else
		{
			checkSetEnum = false;
		}

		EditorGUILayout.EndVertical ( );
		EditorGUILayout.EndHorizontal ( );
	}

	void loadMenu ( )
	{
		if (GUILayout.Button ("Reload Resources", EditorStyles.miniButton))
		{
			resetVariable ( );
			resetLoadMenu ( );
		}

		EditorGUILayout.Space ( );

		int newCount = allObjectType.Length;

		EditorGUI.BeginChangeCheck ( );
		newCount = EditorGUILayout.IntField ("Number Reference", newCount, GUILayout.Width (position.width * 0.5f));
		getResearch = EditorGUILayout.TextField ("Research", getResearch, GUILayout.Width (position.width * 0.5f));
		if (EditorGUI.EndChangeCheck ( ))
		{
			updateList ( );
		}

		int getLength = newCount;

		if (newCount != allObjectType.Length)
		{
			allObjectType = (TypeScriptable [ ])updateArray<TypeScriptable> (newCount, allObjectType);
		}

		if (getLength > 0)
		{
			foldList1 = EditorGUILayout.Foldout (foldList1, "Type Scriptable List (" + allObjectType.Length + ")", true);
		}
		else
		{
			foldList1 = false;
		}

		if (foldList1) // TypeScriptable display
		{
			scrollView1 = EditorGUILayout.BeginScrollView (scrollView1, GUILayout.Height (Mathf.Clamp (allObjectType.Length * 20, 20, position.height * 0.25f)));
			EditorGUI.indentLevel = 1;
			EditorGUI.BeginChangeCheck ( );
			string getName;
			for (int a = 0; a < getLength; a++)
			{
				if (allObjectType [a] != null)
				{
					getName = "Type : " + allObjectType [a].GroupeType;
				}
				else
				{
					getName = "Type : ";
				}

				allObjectType [a] = (TypeScriptable)EditorGUILayout.ObjectField (getName, allObjectType [a], typeof (TypeScriptable), true);
			}

			if (EditorGUI.EndChangeCheck ( ))
			{
				updateList ( );
			}
			EditorGUI.indentLevel = 0;
			EditorGUILayout.EndScrollView ( );
		}
		EditorGUILayout.BeginVertical ( );

		infoTypeScript ( );

		EditorGUILayout.EndVertical ( );

	}
	#endregion

	#region ActionButton
	void backHomeMenu ( )
	{
		EditorGUILayout.Space ( );

		if (GUILayout.Button ("Back Home"))
		{
			resetVariable ( );
			minSize = new Vector3 (300, 100);
			thisMenu = CurrentMenu.Home;
			SpecificPath = null;
		}
	}

	void manualyReload ( )
	{
		Object [ ] getAllMenu = Resources.LoadAll ("Scriptable/" + T_Obj);
		List<TypeScriptable> getObjType = new List<TypeScriptable> (getAllMenu.Length);
		int getLength = getAllMenu.Length;

		for (int a = 0; a < getLength; a++)
		{
			if (getAllMenu [a].GetType ( ) == typeof (TypeScriptable))
			{
				getObjType.Add ((TypeScriptable)getAllMenu [a]);
			}
		}

		allObjectType = getObjType.ToArray ( );
	}

	void resetLoadMenu ( )
	{
		manualyReload ( );
		updateList ( );
	}

	void resetVariable ( )
	{
		allObjectType = new TypeScriptable [0];
		getResearch = "";
		selectObject.Clear ( );
		lastObj = null;

		checkSetEnum = false;
		foldList1 = false;
		foldList2 = false;
		checkEnumSelect = false;

		int getLength = getArray.Length;
		for (int a = 0; a < getLength; a++)
		{
			getInput [a].FoldEnum = false;
			getInput [a].ButtonEnum = false;
			getInput [a].EnumValidate = false;
			getInput [a].TextInfo = SelectAll;
		}
	}

	void setEnumToVariable (bool newScriptable)
	{
		Dictionary<ElementType, TypeScriptable> setUpdate = new Dictionary<ElementType, TypeScriptable> ( );
		List<PhysicMaterial> getObjList = new List<PhysicMaterial> ( );
		List<PhysicMaterial> oldList = new List<PhysicMaterial> ( );
		PhysicMaterial [ ] getPairValue;

		TypeScriptable getNewTS;

		if (!newScriptable)
		{
			List<infoObject> getInfo;
			if (ElemList.TryGetValue (currType, out getInfo))
			{
				setUpdate.Add (currType, getInfo [0].ObjectType);
			}
		}

		if (!setUpdate.TryGetValue (currType, out getNewTS))
		{
			getNewTS = ScriptableObjectUtility.CreateScriptType (PathComplete + "/" + T_Obj, currType.ToString ( ));
			getNewTS.GroupeType = currType;

			setUpdate.Add (currType, getNewTS);
		}

		foreach (KeyValuePair<TypeScriptable, List<PhysicMaterial>> pair in selectObject)
		{
			getPairValue = pair.Value.ToArray ( );

			getObjList.AddRange (getPairValue);

			oldList.AddRange (pair.Key.ThisObj);
			int getlength = getPairValue.Length;
			for (int a = 0; a < getlength; a++)
			{
				oldList.Remove (getPairValue [a]);
			}

			pair.Key.ThisObj = oldList.ToArray ( );
		}

		getNewTS.ThisObj = getObjList.ToArray ( );

		resetVariable ( );
		resetLoadMenu ( );
	}

	PhysicMaterial [ ] returnNewObj ( )
	{
		List<newObjInfo> reloadList = new List<newObjInfo> (ObjLoad);
		List<PhysicMaterial> newGameObject = new List<PhysicMaterial> ( );

		int newCount = reloadList.Count;
		for (int a = 0; a < newCount; a++)
		{
			if (reloadList [a].SelectObj)
			{
				newGameObject.Add (reloadList [a].ThisObj);
				reloadList.RemoveAt (a);
				a--;
				newCount--;
			}
		}
		ObjLoad = reloadList.ToArray ( );

		return newGameObject.ToArray ( );
	}

	async void loadObject (string [ ] GUIDs)
	{
		await System.Threading.Tasks.Task.Delay (0);

		List<newObjInfo> asset = new List<newObjInfo> ( );
		newObjInfo currInfo;
		string guid;
		string assetPath;
		for (int a = 0; a < GUIDs.Length; a++)
		{
			guid = GUIDs [a];
			assetPath = AssetDatabase.GUIDToAssetPath (guid);
			currInfo = new newObjInfo ( );
			currInfo.ThisObj = AssetDatabase.LoadAssetAtPath (assetPath, typeof (PhysicMaterial))as PhysicMaterial;
			asset.Add (currInfo);
		}

		ObjLoad = asset.ToArray ( );
		checkAsyncLoad = false;
	}
	#endregion

	#region InfoPage
	#region Load
	void infoTypeScript ( ) // TypeScript list (parent & children)
	{
		int getLength = 0;
		int getNbr;

		EditorGUILayout.Space ( );
		EditorGUILayout.Space ( );

		// Global bouton
		EditorGUILayout.BeginHorizontal ( );
		if (GUILayout.Button ("Expand all List", EditorStyles.miniButtonLeft))
		{
			getLength = 1;
		}
		else if (GUILayout.Button ("Shrink all List", EditorStyles.miniButtonRight))
		{
			getLength = -1;
		}
		EditorGUILayout.EndHorizontal ( );
		EditorGUILayout.Space ( );
		// Global bouton ---------------------

		getNbr = getLength;
		getLength = getInput.Length;
		if (getNbr != 0)
		{
			for (int a = 0; a < getLength; a++)
			{
				getInput [a].FoldEnum = getNbr == 1;
			}
		}

		int getFoldOpen = 0;
		for (int a = 0; a < getLength; a++)
		{
			if (getInput [a].FoldEnum)
			{
				getFoldOpen++;
			}
		}

		infoInput getInfInput;
		ElementType getType;
		List<infoObject> getList;

		int getTotal = 0;
		getLength = getArray.Length;

		EditorGUILayout.BeginHorizontal (GUILayout.Width (position.width));

		scrollView2 = EditorGUILayout.BeginScrollView (scrollView2, GUILayout.Width (position.width * 0.525f));
		EditorGUILayout.BeginVertical (GUILayout.Width (position.width * 0.5f));

		for (int a = 0; a < getLength; a++)
		{
			getType = (ElementType)getArray.GetValue (a);
			ElemList.TryGetValue (getType, out getList);

			getNbr = getList.Count;
			getInfInput = getInput [a];

			// Parent Enum Type display 
			EditorGUILayout.BeginHorizontal (GUILayout.Width (position.width * 0.5f));
			getTotal = 0;
			for (int b = 0; b < getNbr; b++)
			{
				getTotal += getList [b].ObjectType.ThisObj.Length;

				if (getResearch != "")
				{
					PhysicMaterial [ ] getObj = getList [b].ObjectType.ThisObj;
					int LengthObj = getObj.Length;
					for (int c = 0; c < LengthObj; c++)
					{
						if (getResearch != "" && (getResearch.Length > getObj [c].name.Length || getObj [c].name.Substring (0, getResearch.Length).ToLower ( ) != getResearch.ToLower ( )))
						{
							getTotal--;
						}
					}
				}
			}

			getInput [a].FoldEnum = EditorGUILayout.Foldout (getInfInput.FoldEnum, "Type : " + getType.ToString ( ) + " (" + getTotal.ToString ( ) + ")", true);

			if (getInfInput.ButtonEnum)
			{
				GUI.backgroundColor = Color.green;
			}

			// Select or UnSelect all child
			if (GUILayout.Button (getInfInput.TextInfo, GUILayout.Width (position.width * 0.25f)))
			{
				getInput [a].ButtonEnum = !getInfInput.ButtonEnum;

				List<PhysicMaterial> getListObj;
				PhysicMaterial [ ] getArrayObj;
				int getLength2 = getList.Count;
				int getLength3;
				bool getValue = getInput [a].ButtonEnum;
				bool [ ] currBool;

				if (getInfInput.ButtonEnum)
				{
					getInput [a].TextInfo = SelectAll;
				}
				else
				{
					getInput [a].TextInfo = UnSelectAll;
				}

				for (int b = 0; b < getLength2; b++)
				{
					getArrayObj = getList [b].ObjectType.ThisObj;
					currBool = getList [b].FoldObject;
					getLength3 = getList [b].FoldObject.Length;

					if (!selectObject.TryGetValue (getList [b].ObjectType, out getListObj))
					{
						getListObj = new List<PhysicMaterial> ( );
						selectObject.Add (getList [b].ObjectType, getListObj);
					}

					for (int c = 0; c < getLength3; c++)
					{
						if (currBool [c] != getValue)
						{
							getList [b].FoldObject [c] = getValue;
							if (getValue)
							{
								getListObj.Add (getArrayObj [c]);
							}
							else
							{
								if (getListObj.Count > 0 && getListObj.Contains (getArrayObj [c]))
								{
									getListObj.Remove (getArrayObj [c]);
								}

								if (lastObj == getArrayObj [c])
								{
									lastObj = null;
								}
							}
						}
					}
				}
			}
			// Select or UnSelect all child ---------------------
			// Parent Enum Type display ---------------------

			GUI.backgroundColor = defColor;

			EditorGUILayout.EndHorizontal ( );
			EditorGUILayout.BeginHorizontal (GUILayout.Width (position.width * 0.5f));

			checkNbrObj (getList.ToArray ( ));
			// Child Enum Type display 
			if (getInfInput.FoldEnum)
			{
				if (getFoldOpen == 0)
				{
					getFoldOpen = 1;
				}
				EditorGUI.indentLevel = 1;

				scrollViewEnum [a] = EditorGUILayout.BeginScrollView (scrollViewEnum [a], GUILayout.Width (position.width * 0.5f), GUILayout.Height (Mathf.Clamp (getTotal * 25, 30, position.height * 0.5f)));

				displayThisType (getList.ToArray ( ), a);

				EditorGUILayout.EndScrollView ( );
				EditorGUI.indentLevel = 0;

				EditorGUILayout.Space ( );
			}
			EditorGUILayout.EndHorizontal ( );

			// Child Enum Type display ---------------------

		}
		EditorGUILayout.EndVertical ( );
		EditorGUILayout.EndScrollView ( );

		// Update Enum Object
		EditorGUILayout.BeginVertical (GUILayout.Width (position.width * 0.45f));
		EditorGUILayout.LabelField ("Enum Available", GUILayout.Width (position.width * 0.25f));

		float getClamp = Mathf.Clamp (20 * (getArray.Length + 1), 25, position.height * 0.4f);

		scrollView3 = EditorGUILayout.BeginScrollView (scrollView3, GUILayout.Width (position.width * 0.45f), GUILayout.Height (getClamp));
		getLength = getArray.Length;
		for (int a = 0; a < getLength; a++)
		{
			EditorGUILayout.BeginHorizontal (GUILayout.Width (position.height * 0.4f));

			if (getInput [a].EnumValidate)
			{
				GUI.backgroundColor = Color.green;
			}

			// Enum select list
			if (GUILayout.Button (getArray.GetValue (a).ToString ( ), GUILayout.Width (position.width * 0.35f)))
			{
				int getLength2 = getInput.Length;
				for (int b = 0; b < getLength2; b++)
				{
					if (b != a)
					{
						getInput [b].EnumValidate = false;
					}
				}

				currType = (ElementType)getArray.GetValue (a);
				getInput [a].EnumValidate = !getInput [a].EnumValidate;
				checkEnumSelect = getInput [a].EnumValidate;
			}
			// Enum select list ---------------------

			GUI.backgroundColor = defColor;
			EditorGUILayout.EndHorizontal ( );
		}
		EditorGUILayout.EndScrollView ( );

		List<PhysicMaterial> getAllObj = new List<PhysicMaterial> ( );
		getLength = selectObject.Count;
		foreach (KeyValuePair<TypeScriptable, List<PhysicMaterial>> pair in selectObject)
		{
			getAllObj.AddRange (pair.Value.ToArray ( ));
		}

		if (getAllObj.Count > 0)
		{
			EditorGUILayout.Space ( );
			if (checkEnumSelect)
			{
				GUI.backgroundColor = Color.grey;
				if (!checkSetEnum && GUILayout.Button ("Set Enum to objects", GUILayout.Width (position.width * 0.4f)))
				{
					checkSetEnum = true;
				}
				else if (checkSetEnum)
				{
					EditorGUILayout.BeginHorizontal (GUILayout.Width (position.width * 0.4f));
					if (GUILayout.Button ("On new Scriptable", GUILayout.Width (position.width * 0.2f)))
					{
						checkSetEnum = false;
						setEnumToVariable (true);
					}

					if (GUILayout.Button ("On current Scriptable", GUILayout.Width (position.width * 0.2f)))
					{
						checkSetEnum = false;
						setEnumToVariable (false);
					}

					EditorGUILayout.EndHorizontal ( );
				}

				GUI.backgroundColor = defColor;
			}

			foldList2 = EditorGUILayout.Foldout (foldList2, "Objects selected", true);
		}

		if (foldList2) // GameoObject list 
		{
			EditorGUILayout.BeginHorizontal (GUILayout.Width (position.width * 0.45f));
			getClamp = Mathf.Clamp (20 * getAllObj.Count, 20, position.height * 0.4f);
			scrollView4 = EditorGUILayout.BeginScrollView (scrollView4, GUILayout.Width (position.width * 0.4f), GUILayout.Height (getClamp));

			getLength = getAllObj.Count;
			for (int a = 0; a < getLength; a++)
			{
				EditorGUILayout.ObjectField (getAllObj [a], typeof (PhysicMaterial), true, GUILayout.Width (position.width * 0.25f), GUILayout.Height (15));
			}

			EditorGUILayout.EndScrollView ( );

			EditorGUILayout.EndHorizontal ( );
		}
		else
		{
			getClamp = 0;
		}

		if (!foldList2 || getClamp < position.height * 0.3f)
		{
			if (lastObj != null)
			{
				displayObject (lastObj, Mathf.Clamp (position.width * 0.25f, 0, position.height * 0.4f - getClamp));
			}
			else if (getAllObj.Count > 0)
			{
				displayObject (getAllObj [getAllObj.Count - 1], Mathf.Clamp (position.width * 0.25f, 0, position.height * 0.4f - getClamp));
			}
		}

		EditorGUILayout.EndVertical ( );
		// Update Enum Object ---------------------
		EditorGUILayout.EndHorizontal ( );
	}

	int displayThisType (infoObject [ ] getList, int index)
	{
		bool [ ] getFoldObj;
		PhysicMaterial [ ] getAllObj;
		List<PhysicMaterial> getListObj;

		infoObject thisInfo;

		int getLength = getList.Length;
		int getLength2;
		int totalObj = 0;
		int b;

		for (int a = 0; a < getLength; a++)
		{
			thisInfo = getList [a];
			getAllObj = getList [a].ObjectType.ThisObj;
			getFoldObj = getList [a].FoldObject;

			getLength2 = getAllObj.Length;
			for (b = 0; b < getLength2; b++)
			{
				if (getAllObj [b] != null)
				{
					if (getResearch != "" && (getResearch.Length > getAllObj [b].name.Length || getAllObj [b].name.Substring (0, getResearch.Length).ToLower ( ) != getResearch.ToLower ( )))
					{
						continue;
					}

					totalObj++;
					EditorGUILayout.BeginHorizontal (GUILayout.Width (position.width * 0.47f));
					EditorGUILayout.ObjectField (getAllObj [b], typeof (PhysicMaterial), true, GUILayout.Width (position.width * 0.25f));
					if (!selectObject.TryGetValue (thisInfo.ObjectType, out getListObj))
					{
						getListObj = new List<PhysicMaterial> ( );
						selectObject.Add (getList [a].ObjectType, getListObj);
					}

					if (getFoldObj.Length > b && getFoldObj [b])
					{
						GUI.backgroundColor = Color.green;

						if (GUILayout.Button ("UnSelect", GUILayout.Width (position.width * 0.2f)))
						{
							if (lastObj == getAllObj [b])
							{
								lastObj = null;
							}
							getInput [index].TextInfo = SelectAll;
							getInput [index].ButtonEnum = false;

							thisInfo.FoldObject [b] = false;

							if (getListObj.Count > 0 && getListObj.Contains (getAllObj [b]))
							{
								getListObj.Remove (getAllObj [b]);
							}
						}

						GUI.backgroundColor = defColor;
					}
					else if (GUILayout.Button ("Selected", GUILayout.Width (position.width * 0.2f)))
					{
						if (getAllObj [b] != null)
						{
							lastObj = getAllObj [b];
						}
						getListObj.Add (getAllObj [b]);
						thisInfo.FoldObject [b] = true;
					}

					EditorGUILayout.EndHorizontal ( );
				}
			}
		}

		return totalObj;
	}
	#endregion
	#endregion

	#region Other 
	void checkFolder ( )
	{
		if (!AssetDatabase.IsValidFolder ("Assets/Resources"))
		{
			AssetDatabase.CreateFolder ("Assets", "Resources");
			AssetDatabase.CreateFolder ("Assets/Resources", "Scriptable");
			AssetDatabase.CreateFolder ("Assets/Resources/Scriptable", T_Obj);
		}
		else if (!AssetDatabase.IsValidFolder ("Assets/Resources/Scriptable"))
		{
			AssetDatabase.CreateFolder ("Assets/Resources", "Scriptable");
			AssetDatabase.CreateFolder ("Assets/Resources/Scriptable", T_Obj);
		}
		else if (!AssetDatabase.IsValidFolder ("Assets/Resources/Scriptable/" + T_Obj))
		{
			AssetDatabase.CreateFolder ("Assets/Resources/Scriptable", T_Obj);
		}
	}

	void updateList ( ) // GameObject List
	{
		List<infoObject> getList;

		int getLength = getArray.Length;
		for (int a = 0; a < getLength; a++)
		{
			ElemList.TryGetValue ((ElementType)getArray.GetValue (a), out getList);

			getList.Clear ( );

			getInput [a].FoldEnum = false;
			getInput [a].ButtonEnum = false;
			getInput [a].EnumValidate = false;
			getInput [a].TextInfo = SelectAll;
		}

		infoObject newInfo;
		getLength = allObjectType.Length;
		for (int a = 0; a < getLength; a++)
		{
			if (allObjectType [a] != null)
			{
				ElemList.TryGetValue (allObjectType [a].GroupeType, out getList);

				newInfo = new infoObject ( );
				newInfo.ObjectType = allObjectType [a];
				newInfo.FoldObject = new bool [newInfo.ObjectType.ThisObj.Length];
				newInfo.NbrObj = newInfo.ObjectType.ThisObj.Length;
				newInfo.OldType = newInfo.ObjectType.GroupeType;
				newInfo.OldGamObject = new List<PhysicMaterial> (newInfo.ObjectType.ThisObj).ToArray ( );

				getList.Add (newInfo);
			}
		}
	}

	void displayObject (PhysicMaterial thisObject, float getSize) // display current object
	{
		if (thisObject != null)
		{
			defaultGuiS.normal.background = new Texture2D ((int)getSize, (int)getSize);
			defaultGuiS.normal.background.alphaIsTransparency = true;

			if (defaultGuiS.normal.background != null)
			{
				gameObjectEditor = Editor.CreateEditor (thisObject);
				gameObjectEditor.OnPreviewGUI (GUILayoutUtility.GetRect (getSize, getSize, defaultGuiS, GUILayout.Width (getSize), GUILayout.Height (getSize)), defaultGuiS);
			}
		}
	}

	void checkNbrObj (infoObject [ ] thisScrit) // check list scriptable if there is an update
	{
		PhysicMaterial [ ] getFirstList;
		PhysicMaterial [ ] getSecondList;
		int getLength = thisScrit.Length;
		int getLength2;
		for (int a = 0; a < getLength; a++)
		{
			if (thisScrit [a].NbrObj != thisScrit [a].ObjectType.ThisObj.Length || thisScrit [a].OldType != thisScrit [a].ObjectType.GroupeType)
			{
				updateList ( );
			}
			else
			{
				getFirstList = thisScrit [a].OldGamObject;
				getSecondList = thisScrit [a].ObjectType.ThisObj;

				getLength2 = thisScrit [a].NbrObj;
				for (int b = 0; b < getLength2; b++)
				{
					if (getFirstList [b] != getSecondList [b])
					{
						updateList ( );
						break;
					}
				}
			}
		}
	}

	T [ ] updateArray<T> (int newCount, T [ ] thisArray)
	{
		T [ ] newArray = new T [newCount];

		int getLimite = thisArray.Length;
		if (getLimite < newCount)
		{
			newCount = getLimite;
		}
		for (int a = 0; a < newCount; a++)
		{
			newArray [a] = thisArray [a];
		}

		return newArray;
	}
	#endregion

	#endregion
}
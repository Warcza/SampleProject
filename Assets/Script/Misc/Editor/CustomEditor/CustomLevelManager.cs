﻿using System.Collections;
using System.Collections.Generic;

using UnityEditor;

using UnityEngine;

[CustomEditor (typeof (LevelManager))]
public class CustomLevelManager : Editor
{
	#region Variables
	bool currDisp = false;
	#endregion

	#region Mono

	#endregion TimeMaxDisplay

	#region Public
	public void OnEnable ( )
	{

	}

	public override void OnInspectorGUI ( )
	{
		LevelManager myTarget = (LevelManager)target;
		RoomControl [ ] getRooms;

		EditorGUILayout.Space ( );
		EditorGUILayout.Space ( );

		serializedObject.Update ( );

		DrawDefaultInspector ( );

		if (myTarget.ThisRC != null)
		{
			getRooms = myTarget.ThisRC.NearRoom;

			if (getRooms.Length > 0)
			{
				var buttonStyle = new GUIStyle (EditorStyles.miniButton);

				EditorGUILayout.Space ( );

				if (GUILayout.Button ("Display All Room", buttonStyle))
				{
					currDisp = !currDisp;
					myTarget.DisplayRooms (currDisp);
				}

				EditorGUILayout.Space ( );
				EditorGUILayout.Space ( );

				for (int a = 0; a < getRooms.Length; a++)
				{
					if (GUILayout.Button (getRooms [a].name, buttonStyle))
					{
						myTarget.UpdateRoom (getRooms [a]);
					}
				}

				EditorGUILayout.Space ( );
			}
			EditorGUILayout.Space ( );
		}

		serializedObject.ApplyModifiedProperties ( );
	}
	#endregion

	#region Private

	#endregion
}
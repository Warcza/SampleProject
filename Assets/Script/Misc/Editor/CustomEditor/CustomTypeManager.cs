﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor (typeof (TypeManager))]
public class CustomTypeManager : Editor 
{
	#region Variables
	SerializedProperty AllObjT;
	#endregion

	#region Mono
	public void OnEnable ( )
	{
		AllObjT = serializedObject.FindProperty ("AllObjT");
	}

	public override void OnInspectorGUI ( )
	{
		TypeManager myTarget = (TypeManager)target;

		EditorGUILayout.Space ( );
		EditorGUILayout.Space ( );

		serializedObject.Update ( );

		if (GUILayout.Button ("Update Scriptable Type"))
		{
			Object [ ] getAssets = Resources.LoadAll ("Scriptable/TypeObject", typeof (TypeScriptable));
			List<TypeManager.objTypeInf> allObj = new List<TypeManager.objTypeInf> (getAssets.Length);
			TypeScriptable currScript;
			TypeManager.objTypeInf currObj;

			int a;

			for (a = 0; a < getAssets.Length; a++)
			{
				currScript = getAssets [a] as TypeScriptable;

				currObj = new TypeManager.objTypeInf();
				currObj.ThisObj = currScript.ThisObj;
				currObj.GroupeType = currScript.GroupeType;

				allObj.Add (currObj);
			}

			myTarget.AllObjT = allObj.ToArray ( );
		}

		EditorGUILayout.Space ( );
		EditorGUILayout.PropertyField (AllObjT, true);
		EditorGUILayout.Space ( );

		serializedObject.ApplyModifiedProperties ( );
	}
	#endregion

	#region Public Methodes

	#endregion

	#region Private Methodes

	#endregion
}

﻿using System.Collections;
using System.Collections.Generic;

using UnityEditor;

using UnityEngine;

[CustomEditor (typeof (RoomSpawner))]
public class CustomGameController : Editor
{
	#region Variables
	SerializedProperty AllRoom;
	SerializedProperty DefaultRIS;
	SerializedProperty RoomInfoSpawn;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public void OnEnable ( )
	{
		AllRoom = serializedObject.FindProperty ("RoomSort");
		DefaultRIS = serializedObject.FindProperty ("DefaultRIS");
		RoomInfoSpawn = serializedObject.FindProperty ("RoomInfoSpawn");
	}

	public override void OnInspectorGUI ( )
	{
		RoomSpawner myTarget = (RoomSpawner)target;

		EditorGUILayout.Space ( );
		EditorGUILayout.Space ( );

		serializedObject.Update ( );

		if (GUILayout.Button ("Update Scriptable Room"))
		{
			Object [ ] getAssets = Resources.LoadAll ("Scriptable/Room", typeof (ScriptRoomSetting));
			List<ScriptRoomSetting> getAllRoomScript = new List<ScriptRoomSetting> (getAssets.Length);
			ScriptLevelInfo [ ] checkLevelInfo = myTarget.RoomInfoSpawn;

			int a;

			for (a = 0; a < checkLevelInfo.Length; a++)
			{
				if (checkLevelInfo [a] == null)
				{
					checkLevelInfo [a] = myTarget.DefaultRIS;
				}
			}

			//Recupération de tout les scriptable de Room
			for (a = 0; a < getAssets.Length; a++)
			{
				getAllRoomScript.Add (getAssets [a] as ScriptRoomSetting);
			}

			//Tri de tout les scriptable de Room par type et level
			List<RoomByType> selectRoomType = new List<RoomByType> ( );
			ScriptRoomSetting [ ] getAllRooms = getAllRoomScript.ToArray ( );
			GameObject [ ] getRooms;
			RoomsInfo [ ] getRI;
			RoomsInfo getCurrRI;
			RoomByType currRT;
			RoomControl getRC;

			bool checkRoom;
			bool checkTypeRoom;
			int b;
			int c;

			//System.Array ArrayEnum = System.Enum.GetValues (typeof (RoomType));

			/*for ( a = 0; a < ArrayEnum.Length; a++ )
			{
				selectRoomType.Add ( new RoomByType ( ) );
				selectRoomType [ a ].ThisType = ( RoomType ) ArrayEnum.GetValue ( a );
				selectRoomType [ a ].RoomSort = new List<RoomsInfo> ( );
			}*/

			for (a = 0; a < getAllRooms.Length; a++)
			{
				getRooms = getAllRooms [a].AllRoom;
				for (b = 0; b < getRooms.Length; b++)
				{
					getRC = getRooms [b].GetComponent<RoomControl> ( );

					getRC.ThisType = getAllRooms [a].ThisType;
					getRC.CurrentLevel = getAllRooms [a].LevelRoom;

					if (getRC.InsideComp != null)
					{
						getRC.InsideComp.SetActive (false);
					}

					if (getRC.OutSideComp != null)
					{
						getRC.OutSideComp.SetActive (false);
					}

					if (getRC.BothComp != null)
					{
						getRC.BothComp.SetActive (true);
					}

					getRooms [b].SetActive (false);
				}

				checkRoom = false;
				for (b = 0; b < selectRoomType.Count; b++)
				{
					if (selectRoomType [b].LevelRoom == getAllRooms [a].LevelRoom)
					{
						checkRoom = true;
						getRI = selectRoomType [b].RoomSort.ToArray ( );
						checkTypeRoom = false;

						for (c = 0; c < getRI.Length; c++)
						{
							if (getRI [c].ThisType == getAllRooms [a].ThisType)
							{
								checkTypeRoom = true;
								selectRoomType [b].RoomSort [c].Rooms.Add (getAllRooms [a]);
								break;
							}
						}

						if (!checkTypeRoom)
						{
							getCurrRI = new RoomsInfo ( );
							getCurrRI.ThisType = getAllRooms [a].ThisType;
							getCurrRI.Rooms.Add (getAllRooms [a]);
							selectRoomType [b].RoomSort.Add (getCurrRI);
						}

						break;
					}
				}

				if (!checkRoom)
				{
					currRT = new RoomByType ( );
					currRT.LevelRoom = getAllRooms [a].LevelRoom;
					currRT.RoomSort = new List<RoomsInfo> ( );

					getCurrRI = new RoomsInfo ( );
					getCurrRI.ThisType = getAllRooms [a].ThisType;
					getCurrRI.Rooms = new List<ScriptRoomSetting> ( );
					getCurrRI.Rooms.Add (getAllRooms [a]);

					currRT.RoomSort.Add (getCurrRI);
					selectRoomType.Add (currRT);
				}
			}

			int getMin;
			for (a = 0; a < selectRoomType.Count; a++)
			{
				getMin = selectRoomType [a].LevelRoom;
				c = a;

				for (b = 0; b < selectRoomType.Count; b++)
				{
					if (b > a && b != a && selectRoomType [b].LevelRoom < getMin)
					{
						c = b;
						getMin = selectRoomType [b].LevelRoom;
					}
				}

				currRT = selectRoomType [a];
				selectRoomType [a] = selectRoomType [c];
				selectRoomType [c] = currRT;
			}

			myTarget.RoomSort = selectRoomType.ToArray ( );

			List<Transform> SetParentRoom = new List<Transform> ( );
			Transform GetMain = myTarget.MainParent;
			GameObject NewMain;

			if (GetMain != null)
			{
				DestroyImmediate (GetMain.gameObject);
			}

			NewMain = new GameObject ( );
			NewMain.name = Constants._MainParentRoom;

			GetMain = NewMain.transform;
			GetMain.SetParent (myTarget.transform);

			myTarget.MainParent = GetMain;

			for (a = 0; a < selectRoomType.Count; a++)
			{
				NewMain = new GameObject ( );
				NewMain.transform.SetParent (GetMain);
				NewMain.name = a.ToString ( );
				NewMain.SetActive (false);

				SetParentRoom.Add (NewMain.transform);
			}

			myTarget.ParentsRoom = SetParentRoom.ToArray ( );

			List<ScriptLevelInfo> getSLI = new List<ScriptLevelInfo> (myTarget.RoomInfoSpawn);
			while (getSLI.Count < selectRoomType.Count)
			{
				getSLI.Add (myTarget.DefaultRIS);
			}

			myTarget.RoomInfoSpawn = getSLI.ToArray ( );
		}

		EditorGUILayout.PropertyField (AllRoom, true);

		EditorGUILayout.Space ( );
		EditorGUILayout.Space ( );

		EditorGUILayout.PropertyField (DefaultRIS);
		EditorGUILayout.PropertyField (RoomInfoSpawn, true);

		serializedObject.ApplyModifiedProperties ( );
	}
	#endregion

	#region Private Methodes
	#endregion
}

[CustomPropertyDrawer (typeof (ScriptLevelInfo))]
public class ArrayElementTitleDrawer : PropertyDrawer
{
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		if (property.type == "PPtr<$ScriptLevelInfo>" && property.displayName.Substring (0, 7)== "Element")
		{
			EditorGUI.PropertyField (position, property, new GUIContent ("Level" + property.displayName.Substring (7), label.tooltip), true);
		}
		else
		{
			EditorGUI.PropertyField (position, property, new GUIContent (label.text, label.tooltip), true);
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;

using UnityEditor;

using UnityEngine;

[CustomEditor (typeof (AudioManager))]
public class CustomAudioManager : Editor
{
	#region Variables
	SerializedProperty AllMF;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public void OnEnable ( )
	{
		AllMF = serializedObject.FindProperty ("AllMF");
	}

	public override void OnInspectorGUI ( )
	{
		AudioManager myTarget = (AudioManager)target;

		EditorGUILayout.Space ( );
		EditorGUILayout.Space ( );

		serializedObject.Update ( );

		if (GUILayout.Button ("Update Scriptable Audio"))
		{
			Object [ ] getAssets = Resources.LoadAll ("Scriptable/Audio", typeof (AudioScriptable));
			AudioScriptable thisAudScript;
			List<MusicFX> getAllMusicF = new List<MusicFX> ( );
			int a;

			for (a = 0; a < getAssets.Length; a++)
			{
				thisAudScript = (getAssets [a] as AudioScriptable);
				getAllMusicF.Add (thisAudScript.ThisMusic);
			}

			myTarget.AllMF = getAllMusicF.ToArray ( );
		}

		if (GUILayout.Button ("Random Audio") && Application.isPlaying)
		{
			AudioToken thisTok = new AudioToken ( );
			CustomString setString = new CustomString ( );
			setString.StringValue = -662733300;

			thisTok.AudioAct = AudioAction.Renew;
			thisTok.LoopAudio = false;
			thisTok.ThisType = AudioType.MBG;
			thisTok.CategorieName = setString;

			myTarget.OpenAudio (thisTok);
		}

		EditorGUILayout.Space ( );

		//EditorGUILayout.PropertyField (AllMF, true);

		EditorGUILayout.Space ( );

		serializedObject.ApplyModifiedProperties ( );

		DrawDefaultInspector ( );
	}
	#endregion

	#region Private Methodes

	#endregion
}
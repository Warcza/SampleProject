﻿using System.Collections;
using System.Collections.Generic;

using UnityEditor;

using UnityEngine;

[CustomEditor (typeof (ScriptLevelInfo))]
public class CustomLevelInfo : Editor
{
	#region Variables
	SerializedProperty NbrRoom;
	SerializedProperty RoomInfoSpawnable;
	SerializedProperty MultipleDoorUnUse;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public void OnEnable ( )
	{
		RoomInfoSpawnable = serializedObject.FindProperty ("RoomInfoSpawnable");
		NbrRoom = serializedObject.FindProperty ("NbrDepthRoom");
		MultipleDoorUnUse = serializedObject.FindProperty ("MultipleDoorUnUse");
	}

	public override void OnInspectorGUI ( )
	{
		ScriptLevelInfo myTarget = (ScriptLevelInfo)target;

		EditorGUILayout.Space ( );
		EditorGUILayout.Space ( );

		serializedObject.Update ( );

		System.Array getArray = System.Enum.GetValues (typeof (RoomType));

		if (myTarget.RoomInfoSpawnable != null && myTarget.RoomInfoSpawnable.Length != getArray.Length)
		{
			List<RoomTypeSpawn> getRIS = new List<RoomTypeSpawn> (myTarget.RoomInfoSpawnable);
			bool checkRoomType;
			int b;

			for (int a = 0; a < getArray.Length; a++)
			{
				checkRoomType = false;
				for (b = 0; b < myTarget.RoomInfoSpawnable.Length; b++)
				{
					if ((RoomType)getArray.GetValue (a)== myTarget.RoomInfoSpawnable [b].ThisType)
					{
						checkRoomType = true;
						break;
					}
				}

				if (!checkRoomType)
				{
					getRIS.Add (new RoomTypeSpawn ( ));
					getRIS [getRIS.Count - 1].ThisType = (RoomType)getArray.GetValue (a);
					getRIS [getRIS.Count - 1].PourcentageSpawn = 0;
				}
			}

			myTarget.RoomInfoSpawnable = getRIS.ToArray ( );
		}

		EditorGUILayout.PropertyField (NbrRoom);
		EditorGUILayout.PropertyField (MultipleDoorUnUse);
		EditorGUILayout.PropertyField (RoomInfoSpawnable, true);

		serializedObject.ApplyModifiedProperties ( );
	}
	#endregion

	#region Private Methodes
	#endregion
}
﻿using System.IO;

using UnityEditor;

using UnityEngine;

public static class ScriptableObjectUtility
{
	public static T CreateAsset<T> ( string otherPath = "", string nameAsset = "")where T : ScriptableObject
	{
		T asset = ScriptableObject.CreateInstance<T> ( );

		string path = otherPath;
		if(otherPath == "")
		{
			path = AssetDatabase.GetAssetPath (Selection.activeObject);
		}

		if(nameAsset == "")
		{
			nameAsset = "New " + typeof (T).ToString ( );
		}

		if (path == "")
		{
			path = "Assets";
		}
		else if (Path.GetExtension (path)!= "")
		{
			path = path.Replace (Path.GetFileName (AssetDatabase.GetAssetPath (Selection.activeObject)), "");
		}

		string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath (path + "/" + nameAsset + ".asset");

		AssetDatabase.CreateAsset (asset, assetPathAndName);

		AssetDatabase.SaveAssets ( );
		AssetDatabase.Refresh ( );
		EditorUtility.FocusProjectWindow ( );
		Selection.activeObject = asset;

		return asset;
	}

	[MenuItem ("CustomTools/Scriptable/Room")]
	public static void CreateRoom ( )
	{
		ScriptableObjectUtility.CreateAsset<ScriptRoomSetting> ( );
	}

	[MenuItem ("CustomTools/Scriptable/LevelInfo")]
	public static void CreateLevelInfo ( )
	{
		ScriptableObjectUtility.CreateAsset<ScriptLevelInfo> ( );
	}

	[MenuItem ("CustomTools/Scriptable/Script_Type")]
	public static TypeScriptable CreateScriptType ( string otherPath = "", string nameAsset = "" )
	{
		return ScriptableObjectUtility.CreateAsset<TypeScriptable> ( otherPath, nameAsset);
	}

	public static ScriptableList CreateListCopy ( )
	{
		return ScriptableObjectUtility.CreateAsset<ScriptableList> ( );
	}
}
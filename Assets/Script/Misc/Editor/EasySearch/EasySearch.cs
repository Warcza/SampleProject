﻿using System.Collections;
using System.Collections.Generic;

using UnityEditor;

using UnityEngine;

#region Enum
// i regroup all the enum i needed in this file only because i wanted to try to create a dll and that was faster for me to do it this way
public enum ResearcheType
{
	Object_Prefab,
	Component,
	Reference,
	MissingComp,
	Tag,
	Layer,
	Name
}

public enum TypePlace
{
	OnScene,
	OnProject,
	OnObject
}

public enum GlobalAction
{
	ChangePrefab,
	ChangeName,
	ChangeLayer,
	ChangeTag,
	UpdateComponent,
	UpdateRef,
	MissComp,
	CopyValue
}

public enum RefAction
{
	Replace,
	Remove
}

public enum CompAction
{
	Add,
	Replace,
	Remove,
	UpdateValue,
	AddIfEmpty,
	RemoveIfMany
}
#endregion

public class ScriptableList : ScriptableObject
{
	public GameObject[] AllObj;
}

public class EasySearch : EditorWindow
{
	#region Variable
	ResearcheType thisType;
	InfoResearch infResearch;
	TypePlace SearchType;

	GlobalAction GAction;
	RefAction RAction;
	CompAction CAction;

	string thisStringSearch;
	string newString;
	string specName;

	int thisNbr;
	int newLayer;
	int compDiff;
	int childDiff;

	bool CanRemove;
	bool DisplayOtherParent;
	bool foldLayoutScene;
	bool foldLayoutObject;
	bool foldLayoutProjet;
	bool foldOption;
	//bool foldApply;

	int aPageProj;
	List<bool> foldoutProj;
	List<int> bPageProj;
	bool childProj;
	bool onListProj;
	static bool endSearchProj;
	static int MaxCountProj;
	static int CurrCountProj;
	static int MaxAssetToLoad;
	static int NbrAssetLoad;

	int aPageScene;
	List<int> bPageScene;
	List<bool> foldoutScene;
	bool childScene;
	bool onListScene;
	static bool endSearchScene;
	static int MaxCountScene;
	static int CurrCountScene;

	int aPagePref;
	List<int> bPagePref;
	List<bool> foldoutPref;
	bool childPref;
	bool onListObj;
	static bool endSearchObj;
	static int MaxCountObj;
	static int CurrCountObj;

	bool getChildren;
	bool foldListPref;
	bool foldSamePref;
	bool ApproximateObj;
	bool approxName;
	bool advanceEnable;
	//bool foldComp;
	bool apply;
	bool setPrefb;
	static bool assetLoading;
	//bool replace;

	Object SpecificPath;
	Object objComp;
	Object objGA;
	Object otherObject;

	List<GameObject> thispref;
	List<System.Reflection.FieldInfo> getAdvanceComp;
	List<System.Reflection.FieldInfo> advanceCompOther;

	List<System.Reflection.FieldInfo> updateCompAdv;
	List<System.Reflection.FieldInfo> updateOtherCompAdv;

	//List<System.Reflection.PropertyInfo> getAdvanceProp;
	//List<System.Reflection.PropertyInfo> advancePropOther;

	Vector2 scrollPosProj;
	Vector2 scrollPosScene;
	Vector2 scrollPosPref;
	//Vector2 scrollPosSearch;

	Vector2 AdvancedResearch;
	Vector2 OtherAdvUpdate;

	static Dictionary<TypePlace, List<EditorCoroutine>> getAllCorou;
	List<ObjectParent> AllObjectProject;
	List<ObjectParent> AllObjectScene;
	List<ObjectParent> InfoOnPrefab;
	List<objectInfo> CompInfo;
	//List<Object> saveForUndo;
	#endregion

	void OnEnable ( )
	{
		thisType = ResearcheType.Object_Prefab;
		SearchType = TypePlace.OnScene;
		GAction = GlobalAction.ChangePrefab;
		RAction = RefAction.Replace;
		CAction = CompAction.Add;

		thisStringSearch = string.Empty;
		newString = string.Empty;
		SpecificPath = null;
		specName = string.Empty;

		objComp = null;
		objGA = null;
		otherObject = null;
		thisNbr = 0;
		newLayer = 0;
		getChildren = true;

		childProj = true;
		childScene = true;
		childPref = true;
		DisplayOtherParent = true;

		onListScene = false;
		onListProj = false;
		onListObj = false;

		endSearchProj = true;
		endSearchScene = true;
		endSearchObj = true;

		approxName = true;
		ApproximateObj = false;

		foldListPref = true;
		advanceEnable = false;
		CanRemove = true;
		foldLayoutScene = false;
		foldLayoutObject = false;
		foldLayoutProjet = false;
		foldOption = true;
		setPrefb = false;
		//foldApply = false;

		//foldComp = false;
		apply = false;
		assetLoading = false;
		//replace = true;

		aPageProj = 0;
		aPageScene = 0;
		aPagePref = 0;
		compDiff = 2;
		childDiff = 5;

		CurrCountScene = 0;
		CurrCountObj = 0;
		CurrCountProj = 0;
		MaxCountProj = 0;
		MaxCountScene = 0;
		MaxCountObj = 0;

		scrollPosProj = Vector2.zero;
		scrollPosScene = Vector2.zero;
		scrollPosPref = Vector2.zero;
		//scrollPosSearch = Vector2.zero;
		AdvancedResearch = Vector2.zero;
		OtherAdvUpdate = Vector2.zero;

		bPageProj = new List<int> ( );
		bPageScene = new List<int> ( );
		bPagePref = new List<int> ( );

		foldoutProj = new List<bool> ( );
		foldoutScene = new List<bool> ( );
		foldoutPref = new List<bool> ( );

		getAdvanceComp = new List<System.Reflection.FieldInfo> ( );
		advanceCompOther = new List<System.Reflection.FieldInfo> ( );

		updateCompAdv = new List<System.Reflection.FieldInfo> ( );
		updateOtherCompAdv = new List<System.Reflection.FieldInfo> ( );
		//getAdvanceProp = new List<System.Reflection.PropertyInfo> ( );
		//advancePropOther = new List<System.Reflection.PropertyInfo> ( );

		getAllCorou = new Dictionary<TypePlace, List<EditorCoroutine>> ( );
		AllObjectProject = new List<ObjectParent> ( );
		AllObjectScene = new List<ObjectParent> ( );
		InfoOnPrefab = new List<ObjectParent> ( );
		thispref = new List<GameObject> ( );
		CompInfo = new List<objectInfo> ( );
		//saveForUndo = new List<Object> ();
		infResearch = new InfoResearch ( );
	}

	[MenuItem ("Window/EasySearch")]
	public static void ShowWindow ( )
	{
		EditorWindow.GetWindow (typeof (EasySearch));
	}

	void OnGUI ( )
	{
		#region Research Config
		List<ObjectParent> getAllOnProj = AllObjectProject;
		List<ObjectParent> getAllOnScene = AllObjectScene;
		List<ObjectParent> getAllOnPrefab = InfoOnPrefab;
		List<int> bProj = bPageProj;
		List<int> bScene = bPageScene;
		List<int> bPref = bPagePref;

		List<bool> fPref = foldoutPref;
		List<bool> fScene = foldoutScene;
		List<bool> fProj = foldoutProj;

		int getPourcVal;
		int a;
		float sizeX = position.width;
		float currSize;
		bool cutSize;
		float otherSize;

		if (sizeX < 700)
		{
			currSize = sizeX;
			cutSize = true;
		}
		else
		{
			cutSize = false;
			currSize = sizeX / 1.5f;
		}

		GUILayout.Label ("Get object(s)", EditorStyles.boldLabel);

		if (cutSize || position.height < 500)
		{
			foldOption = EditorGUILayout.Foldout (foldOption, "Hide Research Option");
		}
		else
		{
			foldOption = true;
		}

		if (foldOption) // Research option 
		{
			if (cutSize)
			{
				SearchType = (TypePlace)EditorGUILayout.EnumPopup ("Place To Search:", SearchType, GUILayout.Width (currSize));
			}

			EditorGUILayout.BeginVertical ( );
			EditorGUILayout.BeginHorizontal ( );

			EditorGUI.BeginChangeCheck ( );

			thisType = (ResearcheType)EditorGUILayout.EnumPopup ("Research Type:", thisType, GUILayout.Width (currSize));

			if (EditorGUI.EndChangeCheck ( )) // reset if reserach type changed
			{
				getAdvanceComp.Clear ( );
				advanceCompOther.Clear ( );
				//getAdvanceProp.Clear ( );
				//advancePropOther.Clear ( );

				advanceEnable = false;

				objComp = null;
				objGA = null;
				otherObject = null;
				apply = false;

				thisStringSearch = string.Empty;
				newString = string.Empty;
				specName = string.Empty;

				thisNbr = 0;
				newLayer = 0;
				compDiff = 1;
				childDiff = 1;

				aPageProj = 0;
				aPageScene = 0;
				aPagePref = 0;

				//foldApply = false;

				approxName = true;
				ApproximateObj = false;

				GAction = GlobalAction.ChangePrefab;
				RAction = RefAction.Replace;
				CAction = CompAction.Add;

				switch (thisType)
				{
					case ResearcheType.Reference:
						CanRemove = false;
						DisplayOtherParent = true;
						break;
					case ResearcheType.Object_Prefab:
						CanRemove = true;
						DisplayOtherParent = true;
						break;
					default:
						DisplayOtherParent = false;
						CanRemove = false;
						break;
				}
			}

			EditorGUILayout.EndHorizontal ( );

			EditorGUI.indentLevel = 1;

			switch (thisType)
			{
				case ResearcheType.Tag:
					thisStringSearch = EditorGUILayout.TagField ("This Tag :", thisStringSearch, GUILayout.Width (currSize));
					break;
				case ResearcheType.Layer:
					thisNbr = EditorGUILayout.LayerField ("This Num Layer :", thisNbr, GUILayout.Width (currSize));
					thisStringSearch = thisNbr.ToString ( );
					break;
				case ResearcheType.Name:
					thisStringSearch = EditorGUILayout.TextField ("This Name :", thisStringSearch, GUILayout.Width (currSize));
					break;
				case ResearcheType.Component:
					Object checkObj = objComp;
					objComp = EditorGUILayout.ObjectField ("This component", objComp, typeof (Object), true, GUILayout.Width (currSize));

					if (objComp != null && objComp.GetType ( )== typeof (GameObject))
					{
						objComp = null;
					}

					if (objComp == null)
					{
						getAdvanceComp.Clear ( );
						advanceCompOther.Clear ( );
						//getAdvanceProp.Clear ( );
						//advancePropOther.Clear ( );
						advanceEnable = false;
					}
					else if (objComp != checkObj)
					{
						advanceCompOther.Clear ( );
						getAdvanceComp.Clear ( );
						//getAdvanceProp.Clear ( );
						//advancePropOther.Clear ( );

						getAdvanceComp.AddRange (objComp.GetType ( ).GetFields ( ));
						//getAdvanceProp.AddRange( objComp.GetType ( ).GetProperties( ));
					}
					break;
				case ResearcheType.Reference:
					objComp = EditorGUILayout.ObjectField ("This Object ref", objComp, typeof (Object), true, GUILayout.Width (currSize));
					break;
				case ResearcheType.Object_Prefab:
					EditorGUI.BeginChangeCheck ( );

					objComp = EditorGUILayout.ObjectField ("This Object", objComp, typeof (Object), true, GUILayout.Width (currSize));

					if (EditorGUI.EndChangeCheck ( ))
					{
						apply = false;
						CompInfo.Clear ( );
						List<objectInfo> getCI = CompInfo;

						if (objComp != null)
						{
							if (objComp.GetType ( )== typeof (GameObject))
							{
								foreach (GameObject thisObj in SearchObject.GetComponentsInChildrenOfAsset ((GameObject)objComp))
								{
									getCI.Add (new objectInfo ( ));
									getCI [getCI.Count - 1].ThisObj = thisObj;
									getCI [getCI.Count - 1].thoseComp = thisObj.GetComponents<Component> ( );
								}
							}
							else
							{
								objComp = null;
								Debug.LogError ("Cannont search this");
							}
						}
					}
					break;
			}

			foldSamePref = EditorGUILayout.Foldout (foldSamePref, "Advanced");

			if (foldSamePref) // Detail option display
			{
				advancedFilt (currSize);
			}
			EditorGUILayout.EndVertical ( );

			EditorGUILayout.Space ( );
			#endregion

			#region ActionResearch
			if (cutSize)
			{
				EditorGUILayout.BeginVertical ( );
			}
			else
			{
				EditorGUILayout.BeginHorizontal ( );
			}

			float otherSide2;
			float otherSide3;
			if (cutSize)
			{
				otherSize = sizeX;
				otherSide2 = sizeX;
				otherSide3 = sizeX;
			}
			else
			{
				otherSize = sizeX / 3;
				otherSide2 = sizeX / 3.1f;
				otherSide3 = sizeX / 3.15f;
			}

			EditorGUILayout.BeginVertical (GUILayout.Width (otherSize));

			if (!cutSize || SearchType == TypePlace.OnScene) // Scene Reasearch 
			{
				if (GUILayout.Button ("On Scene", GUILayout.Height (25))|| onListScene)
				{
					StopPlace (TypePlace.OnScene);

					aPageScene = 0;
					MaxCountScene = 0;
					CurrCountScene = 0;

					bPageScene = new List<int> ( );

					bScene = bPageScene;
					fScene = foldoutScene;
					childScene = getChildren;

					if (cutSize)
					{
						foldOption = false;
					}

					foldSamePref = false;
					endSearchScene = false;
					apply = false;
					objGA = null;
					otherObject = null;

					if (onListScene)
					{
						List<GameObject> getlist = new List<GameObject> ( );

						for (a = 0; a < getAllOnScene.Count; a++)
						{
							getlist.AddRange (getAllOnScene [a].AllObj);
						}

						getAllOnScene.Clear ( );
						AllObjectScene = new List<ObjectParent> ( );
						getAllOnScene = AllObjectScene;

						EditorCoroutine.start (SearchObject.LoadOnPrefab (getAllOnScene, thisType, objComp, getlist, false, setInfR ( ), TypePlace.OnScene), TypePlace.OnScene);
					}
					else
					{
						getAllOnScene.Clear ( );
						AllObjectScene = new List<ObjectParent> ( );
						getAllOnScene = AllObjectScene;

						EditorCoroutine.start (SearchObject.LoadAssetOnScenes (getAllOnScene, thisType, objComp, getChildren, setInfR ( )), TypePlace.OnScene);
					}

					onListScene = false;
				}

				if (!endSearchScene)
				{
					getPourcVal = (int)(((float)CurrCountScene / (float)MaxCountScene)* 100);
					GUILayout.HorizontalSlider (getPourcVal, 0, 100);

					if (getAllOnScene.Count == 0 && GUILayout.Button ("Stop search on Scene", EditorStyles.miniButton))
					{
						StopPlace (TypePlace.OnScene);
					}
				}
			}

			EditorGUILayout.EndVertical ( );

			EditorGUILayout.BeginVertical (GUILayout.Width (otherSize));
			if (!cutSize || SearchType == TypePlace.OnProject) // Project Reasearch 
			{
				if (GUILayout.Button ("On Project", GUILayout.Height (25))|| onListProj)
				{
					StopPlace (TypePlace.OnProject);

					aPageProj = 0;
					MaxCountProj = 0;
					CurrCountProj = 0;

					bPageProj = new List<int> ( );
					foldoutProj = new List<bool> ( );

					fProj = foldoutProj;
					bProj = bPageProj;
					childProj = getChildren;

					if (cutSize)
					{
						foldOption = false;
					}

					foldSamePref = false;
					endSearchProj = false;
					apply = false;
					objGA = null;
					otherObject = null;

					if (onListProj)
					{
						List<GameObject> getlist = new List<GameObject> ( );

						for (a = 0; a < getAllOnProj.Count; a++)
						{
							getlist.AddRange (getAllOnProj [a].AllObj);
						}

						getAllOnProj.Clear ( );
						AllObjectProject = new List<ObjectParent> ( );
						getAllOnProj = AllObjectProject;

						EditorCoroutine.start (SearchObject.LoadOnPrefab (getAllOnProj, thisType, objComp, getlist, false, setInfR ( ), TypePlace.OnProject), TypePlace.OnProject);
					}
					else
					{
						getAllOnProj.Clear ( );
						AllObjectProject = new List<ObjectParent> ( );
						getAllOnProj = AllObjectProject;

						EditorCoroutine.start (SearchObject.LoadAssetsInProject (getAllOnProj, thisType, objComp, getChildren, setInfR ( )), TypePlace.OnProject);
					}

					onListProj = false;
				}

				SpecificPath = EditorGUILayout.ObjectField ("Specific folder :", SpecificPath, typeof (Object), true, GUILayout.Width (otherSide2));

				if (!endSearchProj)
				{
					EditorGUILayout.BeginHorizontal (GUILayout.Width (otherSide2));
					string getText;
					if (assetLoading)
					{
						getText = "Loading Asset";
						getPourcVal = (int)(((float)NbrAssetLoad / (float)MaxAssetToLoad)* 100);
					}
					else
					{
						getText = "Searching Object";
						getPourcVal = (int)(((float)CurrCountProj / (float)MaxCountProj)* 100);
					}

					EditorGUILayout.PrefixLabel (getText);
					GUILayout.HorizontalSlider (getPourcVal, 0, 100);
					//EditorGUILayout.PrefixLabel ( (( int ) ( ( ( float ) CurrCountProj / ( float ) MaxCountProj ) * 100 )).ToString()  );
					EditorGUILayout.EndHorizontal ( );

					if (getAllOnProj.Count == 0 && GUILayout.Button ("Stop search on Project", EditorStyles.miniButton))
					{
						StopPlace (TypePlace.OnProject);
					}
				}
			}
			EditorGUILayout.EndVertical ( );

			EditorGUILayout.BeginVertical (GUILayout.Width (otherSide3));
			if (!cutSize || SearchType == TypePlace.OnObject) // On Object Reasearch 
			{
				if (GUILayout.Button ("On Object(s)", GUILayout.Height (25))&& thispref != null || onListObj)
				{
					StopPlace (TypePlace.OnObject);

					aPagePref = 0;
					MaxCountObj = 0;
					CurrCountObj = 0;

					bPagePref = new List<int> ( );

					bPref = bPagePref;
					fPref = foldoutPref;
					childPref = getChildren;

					objGA = null;
					otherObject = null;
					endSearchObj = false;
					foldSamePref = false;
					apply = false;

					if (cutSize)
					{
						foldOption = false;
					}

					if (onListObj)
					{
						List<GameObject> getlist = new List<GameObject> ( );

						for (a = 0; a < getAllOnPrefab.Count; a++)
						{
							getlist.AddRange (getAllOnPrefab [a].AllObj);
						}

						getAllOnPrefab.Clear ( );
						InfoOnPrefab = new List<ObjectParent> ( );
						getAllOnPrefab = InfoOnPrefab;

						EditorCoroutine.start (SearchObject.LoadOnPrefab (getAllOnPrefab, thisType, objComp, getlist, false, setInfR ( )), TypePlace.OnObject);
					}
					else
					{
						getAllOnPrefab.Clear ( );
						InfoOnPrefab = new List<ObjectParent> ( );
						getAllOnPrefab = InfoOnPrefab;

						EditorCoroutine.start (SearchObject.LoadOnPrefab (getAllOnPrefab, thisType, objComp, thispref, getChildren, setInfR ( )), TypePlace.OnObject);
					}

					onListObj = false;
				}

				var list = thispref;
				int newCount = Mathf.Max (0, EditorGUILayout.IntField ("Number Ref", list.Count));
				while (newCount < list.Count)
				{
					list.RemoveAt (list.Count - 1);
				}

				while (newCount > list.Count)
				{
					list.Add (null);
				}
				EditorGUILayout.BeginVertical ( );
				if (thispref.Count > 0)
				{
					foldListPref = EditorGUILayout.Foldout (foldListPref, "Object List");
				}

				if (foldListPref)
				{
					for (a = 0; a < thispref.Count; a++)
					{
						thispref [a] = (GameObject)EditorGUILayout.ObjectField ("This Object", thispref [a], typeof (GameObject), true);
					}
				}
				EditorGUILayout.EndVertical ( );
				if (!endSearchObj)
				{
					getPourcVal = (int)(((float)CurrCountObj / (float)MaxCountObj)* 100);

					GUILayout.HorizontalSlider (getPourcVal, 0, 10);
					if (getAllOnPrefab.Count == 0 && GUILayout.Button ("Stop search on Object", EditorStyles.miniButton))
					{
						StopPlace (TypePlace.OnObject);
					}
				}
			}
			EditorGUILayout.EndVertical ( );

			if (cutSize)
			{
				EditorGUILayout.EndVertical ( );
			}
			else
			{
				EditorGUILayout.EndHorizontal ( );
			}
			#endregion
		}
		else // Loading
		{
			EditorGUILayout.BeginVertical ( );
			if (!endSearchProj) 
			{
				EditorGUILayout.BeginHorizontal ( );
				string getText;
				if (assetLoading)
				{
					getText = "Project : Loading Asset";
					getPourcVal = (int)(((float)NbrAssetLoad / (float)MaxAssetToLoad)* 100);
				}
				else
				{
					getText = "Project : Searching Object";
					getPourcVal = (int)(((float)CurrCountProj / (float)MaxCountProj)* 100);
				}

				EditorGUILayout.PrefixLabel (getText);
				GUILayout.HorizontalSlider (getPourcVal, 0, 100);
				EditorGUILayout.EndHorizontal ( );
			}

			if (!endSearchScene)
			{
				EditorGUILayout.BeginHorizontal ( );

				getPourcVal = (int)(((float)CurrCountScene / (float)MaxCountScene)* 100);
				EditorGUILayout.PrefixLabel ("Scene : Searching Object");

				GUILayout.HorizontalSlider (getPourcVal, 0, 100);
				EditorGUILayout.EndHorizontal ( );
			}

			if (!endSearchObj)
			{
				EditorGUILayout.BeginHorizontal ( );

				getPourcVal = (int)(((float)CurrCountObj / (float)MaxCountObj)* 100);
				EditorGUILayout.PrefixLabel ("Object(s) : Searching Object");

				GUILayout.HorizontalSlider (getPourcVal, 0, 10);
				EditorGUILayout.EndHorizontal ( );
			}
			EditorGUILayout.EndVertical ( );
		}

		#region AfterResearch
		EditorGUILayout.Space ( );
		EditorGUILayout.Space ( );
		if ((getAllOnScene.Count > 0 || getAllOnProj.Count > 0 || getAllOnPrefab.Count > 0)&& endSearchObj && endSearchProj && endSearchScene)
		{
			EditorGUILayout.BeginHorizontal ( );
			EditorGUI.BeginChangeCheck ( );
			EditorGUI.indentLevel = 0;

			GAction = (GlobalAction)EditorGUILayout.EnumPopup (GAction, GUILayout.Width (currSize / 2));
			if (EditorGUI.EndChangeCheck ( ))
			{
				setPrefb = false;
				apply = false;
				objGA = null;
				otherObject = null;
				newString = string.Empty;
			}

			if (cutSize || GAction == GlobalAction.UpdateComponent || GAction == GlobalAction.UpdateRef)
			{
				EditorGUILayout.BeginVertical ( );
			}

			EditorGUILayout.BeginHorizontal ( );
			bool condTrue = false;
			bool noNeedApply = false;
			string textUpdate = "Apply Update";

			#region ActionOnList
			switch (GAction) 
			{
				case GlobalAction.ChangeLayer:
					newLayer = EditorGUILayout.LayerField (newLayer, GUILayout.Width (currSize / 2));
					condTrue = true;
					break;
				case GlobalAction.ChangeName:
					newString = EditorGUILayout.TextField (newString, GUILayout.Width (currSize / 2));

					if (newString != string.Empty)
					{
						condTrue = true;
					}
					break;
				case GlobalAction.ChangePrefab:
					if (objComp != null && objComp.GetType ( )== typeof (GameObject)&& objGA == null)
					{
						objGA = objComp;
					}

					objGA = EditorGUILayout.ObjectField (objGA, typeof (GameObject), true, GUILayout.Width (currSize / 2));

					if (objGA != null)
					{
						condTrue = true;
					}
					break;
				case GlobalAction.ChangeTag:
					newString = EditorGUILayout.TagField (newString, GUILayout.Width (currSize / 2));

					if (newString != string.Empty)
					{
						condTrue = true;
					}
					break;
				case GlobalAction.MissComp:
					textUpdate = "Delete";
					condTrue = true;
					noNeedApply = true;
					break;
				case GlobalAction.CopyValue:
					EditorGUILayout.BeginVertical ( GUILayout.Width (currSize / 2));
					
					newString = EditorGUILayout.TextField ("List Name", newString, GUILayout.Width (currSize / 2));
					Color getDef = GUI.backgroundColor ;

					if(setPrefb)
					{
						GUI.backgroundColor = Color.green;
					}

					if(GUILayout.Button ("Create prefab if null", GUILayout.Width (currSize / 2)))
					{
						setPrefb = !setPrefb;
					}

					GUI.backgroundColor = getDef;
					EditorGUILayout.EndVertical ( );

					textUpdate = "Copy List";
					condTrue = true;
					noNeedApply = true;
					break;
				case GlobalAction.UpdateComponent:
					if (objGA == null && objComp != null)
					{
						objGA = objComp;
						updateOtherCompAdv.Clear ( );
						updateCompAdv.Clear ( );
						updateCompAdv.AddRange (objGA.GetType ( ).GetFields ( ));
					}

					if (cutSize)
					{
						CAction = (CompAction)EditorGUILayout.EnumPopup (CAction, GUILayout.Width (currSize / 4));

						EditorGUI.BeginChangeCheck ( );

						objGA = EditorGUILayout.ObjectField (objGA, typeof (Object), true, GUILayout.Width (currSize / 3));

						if (EditorGUI.EndChangeCheck ( ))
						{
							updateOtherCompAdv.Clear ( );
							updateCompAdv.Clear ( );
							try
							{
								updateCompAdv.AddRange (objGA.GetType ( ).GetFields ( ));
							}
							catch
							{
								Debug.Log ("halo");

							}
						}

						if (CAction == CompAction.Replace)
						{
							EditorGUILayout.EndHorizontal ( );
							otherObject = EditorGUILayout.ObjectField (otherObject, typeof (Object), true);
							EditorGUILayout.BeginHorizontal ( );
						}
					}
					else
					{
						CAction = (CompAction)EditorGUILayout.EnumPopup (CAction, GUILayout.Width (currSize / 2));

						EditorGUI.BeginChangeCheck ( );

						objGA = EditorGUILayout.ObjectField (objGA, typeof (Object), true, GUILayout.Width (currSize / 2));

						if (EditorGUI.EndChangeCheck ( ))
						{
							updateOtherCompAdv.Clear ( );
							updateCompAdv.Clear ( );
							try
							{
								updateCompAdv.AddRange (objGA.GetType ( ).GetFields ( ));
							}
							catch
							{
								Debug.Log ("halo");
							}
						}

						if (CAction == CompAction.Replace)
						{
							EditorGUILayout.EndHorizontal ( );
							otherObject = EditorGUILayout.ObjectField ("To ", otherObject, typeof (Object), true);
							EditorGUILayout.BeginHorizontal ( );
						}
					}

					if (CAction == CompAction.UpdateValue)
					{
						int b = (updateCompAdv.Count + updateOtherCompAdv.Count)* 20; //+ getAdvanceProp.Count * 10;

						if (b > 100)
						{
							b = 100;
						}
						EditorGUILayout.EndHorizontal ( );

						OtherAdvUpdate = EditorGUILayout.BeginScrollView (OtherAdvUpdate, GUILayout.Height (b));
						for (b = 0; b < updateCompAdv.Count; b++)
						{
							EditorGUI.indentLevel = 2;

							EditorGUILayout.BeginHorizontal ( );
							EditorGUILayout.PrefixLabel (updateCompAdv [b].Name);
							EditorGUI.indentLevel = 5;

							if (GUILayout.Button ("Don't Update", EditorStyles.miniButton, GUILayout.Width (sizeX / 3)))
							{
								updateOtherCompAdv.Add (updateCompAdv [b]);
								updateCompAdv.RemoveAt (b);
								b--;
							}
							EditorGUILayout.EndHorizontal ( );
						}
						EditorGUI.indentLevel = 2;

						for (b = 0; b < updateOtherCompAdv.Count; b++)
						{
							EditorGUI.indentLevel = 2;

							EditorGUILayout.BeginHorizontal ( );
							EditorGUILayout.PrefixLabel (updateOtherCompAdv [b].Name);
							EditorGUI.indentLevel = 5;

							if (GUILayout.Button ("Update", EditorStyles.miniButton, GUILayout.Width (sizeX / 3)))
							{
								updateCompAdv.Add (updateOtherCompAdv [b]);
								updateOtherCompAdv.RemoveAt (b);
								b--;
							}
							EditorGUILayout.EndHorizontal ( );
						}
						EditorGUILayout.BeginHorizontal ( );
						EditorGUILayout.EndScrollView ( );
					}

					if (objGA != null && (CAction != CompAction.Replace || otherObject != null))
					{
						if (objGA.GetType ( )== typeof (GameObject))
						{
							objGA = null;
						}
						else
						{

							condTrue = true;
						}
					}
					break;
				case GlobalAction.UpdateRef:

					if (objGA == null && objComp != null)
					{
						objGA = objComp;
					}

					if (cutSize)
					{
						RAction = (RefAction)EditorGUILayout.EnumPopup (RAction, GUILayout.Width (currSize / 4));
						objGA = EditorGUILayout.ObjectField (objGA, typeof (Object), true, GUILayout.Width (currSize / 3));

						if (RAction == RefAction.Replace)
						{
							EditorGUILayout.EndHorizontal ( );
							otherObject = EditorGUILayout.ObjectField (otherObject, typeof (Object), true);
							EditorGUILayout.BeginHorizontal ( );
						}
					}
					else
					{
						RAction = (RefAction)EditorGUILayout.EnumPopup (RAction, GUILayout.Width (currSize / 2));
						objGA = EditorGUILayout.ObjectField (objGA, typeof (Object), true, GUILayout.Width (currSize / 2));

						if (RAction == RefAction.Replace)
						{
							EditorGUILayout.EndHorizontal ( );
							otherObject = EditorGUILayout.ObjectField ("To ", otherObject, typeof (Object), true);
							EditorGUILayout.BeginHorizontal ( );
						}
					}

					if (objGA != null && (RAction != RefAction.Replace || otherObject != null))
					{
						condTrue = true;
					}
					break;
				default:
					break;
			}

			if (cutSize || GAction == GlobalAction.UpdateComponent || GAction == GlobalAction.UpdateRef)
			{
				EditorGUILayout.EndHorizontal ( );
			}

			if (condTrue && !apply && GUILayout.Button (textUpdate, EditorStyles.miniButton))
			{
				apply = true;
			}

			if (!cutSize && GAction != GlobalAction.UpdateComponent && GAction != GlobalAction.UpdateRef)
			{
				EditorGUILayout.EndHorizontal ( );
			}
			else
			{
				EditorGUILayout.EndVertical ( );
			}
			EditorGUILayout.EndHorizontal ( );

			if (apply)
			{
				EditorGUILayout.BeginHorizontal ( );
				if (noNeedApply || GUILayout.Button ("Confirm", EditorStyles.miniButton))
				{
					apply = false;
					noNeedApply = false;
					switch (GAction)
					{
						case GlobalAction.ChangeLayer:
							modifLayer (getAllOnScene, newLayer);
							modifLayer (getAllOnProj, newLayer);
							modifLayer (getAllOnPrefab, newLayer);
							break;
						case GlobalAction.ChangeName:
							modifName (getAllOnScene, newString);
							modifName (getAllOnProj, newString);
							modifName (getAllOnPrefab, newString);
							break;
						case GlobalAction.ChangePrefab:
							modifPref (getAllOnScene, (GameObject)objGA);
							modifPref (getAllOnProj, (GameObject)objGA);
							modifPref (getAllOnPrefab, (GameObject)objGA);
							break;
						case GlobalAction.ChangeTag:
							modifTag (getAllOnScene, newString);
							modifTag (getAllOnProj, newString);
							modifTag (getAllOnPrefab, newString);
							break;
						case GlobalAction.UpdateComponent:
							if (CAction == CompAction.Replace)
							{
								modifComp (getAllOnScene, objGA, otherObject);
								modifComp (getAllOnProj, objGA, otherObject);
								modifComp (getAllOnPrefab, objGA, otherObject);
							}
							else
							{
								otherObject = null;
								modifComp (getAllOnScene, objGA);
								modifComp (getAllOnProj, objGA);
								modifComp (getAllOnPrefab, objGA);
							}
							break;
						case GlobalAction.UpdateRef:
							if (RAction == RefAction.Replace)
							{
								modifRef (getAllOnScene, objGA, otherObject);
								modifRef (getAllOnProj, objGA, otherObject);
								modifRef (getAllOnPrefab, objGA, otherObject);
							}
							else
							{
								otherObject = null;
								modifRef (getAllOnScene, objGA);
								modifRef (getAllOnProj, objGA);
								modifRef (getAllOnPrefab, objGA);
							}
							break;
						case GlobalAction.MissComp:
							deleteMiss (getAllOnScene);
							deleteMiss (getAllOnProj);
							deleteMiss (getAllOnPrefab);
							break;
						case GlobalAction.CopyValue:
							copyList(getAllOnScene, getAllOnProj, getAllOnPrefab);
							break;
						default:
							break;
					}
					//Undo.RecordObjects(saveForUndo.ToArray(), "test");
				}

				if (GUILayout.Button ("Cancel", EditorStyles.miniButton))
				{
					apply = false;
				}
				EditorGUILayout.EndHorizontal ( );
			}
			#endregion
		}

		int sizeLayout = 0;

		if (getAllOnScene.Count > 0)
		{
			sizeLayout++;
		}

		if (getAllOnProj.Count > 0)
		{
			sizeLayout++;
		}

		if (getAllOnPrefab.Count > 0)
		{
			sizeLayout++;
		}

		if (!cutSize)
		{
			EditorGUILayout.BeginHorizontal (GUILayout.Width (sizeX));
		}
		else
		{
			bool checkNew;

			EditorGUILayout.Space ( );
			EditorGUILayout.BeginHorizontal ( );

			if (getAllOnScene.Count > 0)
			{
				checkNew = foldLayoutScene;
				foldLayoutScene = EditorGUILayout.Foldout (foldLayoutScene, "Display Scene");

				if (sizeLayout == 1)
				{
					foldLayoutScene = true;
				}

				if (!checkNew && foldLayoutScene)
				{
					foldLayoutProjet = false;
					foldLayoutObject = false;
				}
			}

			if (getAllOnProj.Count > 0)
			{
				checkNew = foldLayoutProjet;
				foldLayoutProjet = EditorGUILayout.Foldout (foldLayoutProjet, "Display Project");

				if (sizeLayout == 1)
				{
					foldLayoutProjet = true;
				}

				if (!checkNew && foldLayoutProjet)
				{
					foldLayoutScene = false;
					foldLayoutObject = false;
				}
			}

			if (getAllOnPrefab.Count > 0)
			{
				checkNew = foldLayoutObject;
				foldLayoutObject = EditorGUILayout.Foldout (foldLayoutObject, "Display Object(s)");

				if (sizeLayout == 1)
				{
					foldLayoutObject = true;
				}

				if (!checkNew && foldLayoutObject)
				{
					foldLayoutProjet = false;
					foldLayoutScene = false;
				}
			}
			EditorGUILayout.EndHorizontal ( );

			EditorGUILayout.BeginVertical ( );

			sizeLayout = 1;
		}

		#region Scene Layout
		if (getAllOnScene.Count > 0)
		{
			EditorGUILayout.BeginHorizontal (GUILayout.Width (sizeX / sizeLayout));

			EditorGUILayout.BeginVertical ( );
			if (!cutSize || foldLayoutScene)
			{
				if (endSearchScene)
				{
					if (GUILayout.Button ("Search on List", EditorStyles.miniButton))
					{
						onListScene = true;
					}

					if (GUILayout.Button ("Clear Scene", EditorStyles.miniButton))
					{
						AllObjectScene = new List<ObjectParent> ( );
					}
				}
				else if (GUILayout.Button ("Stop search on Scene", EditorStyles.miniButton))
				{
					StopPlace (TypePlace.OnScene);
				}

				scrollPosScene = EditorGUILayout.BeginScrollView (scrollPosScene);
				aPageScene = LayoutSearch (getAllOnScene, bScene, fScene, aPageScene, childScene, MaxCountScene);

				EditorGUILayout.EndScrollView ( );
			}
			EditorGUILayout.EndVertical ( );
			EditorGUILayout.EndHorizontal ( );
		}
		#endregion

		#region Project layout
		if (getAllOnProj.Count > 0)
		{
			EditorGUILayout.BeginHorizontal (GUILayout.Width (sizeX / sizeLayout));

			EditorGUILayout.BeginVertical ( );
			if (!cutSize || foldLayoutProjet)
			{
				if (endSearchProj)
				{
					if (GUILayout.Button ("Search on List", EditorStyles.miniButton))
					{
						onListProj = true;
					}

					if (GUILayout.Button ("Clear Project", EditorStyles.miniButton))
					{
						AllObjectProject = new List<ObjectParent> ( );
					}
				}
				else if (GUILayout.Button ("Stop search on Project", EditorStyles.miniButton))
				{
					StopPlace (TypePlace.OnProject);
				}

				scrollPosProj = EditorGUILayout.BeginScrollView (scrollPosProj);
				aPageProj = LayoutSearch (getAllOnProj, bProj, fProj, aPageProj, childProj, MaxCountProj);

				EditorGUILayout.EndScrollView ( );
			}
			EditorGUILayout.EndVertical ( );
			EditorGUILayout.EndHorizontal ( );
		}
		#endregion

		#region Pref Layout
		if (getAllOnPrefab.Count > 0)
		{
			EditorGUILayout.BeginHorizontal (GUILayout.Width (sizeX / sizeLayout));

			EditorGUILayout.BeginVertical ( );
			if (!cutSize || foldLayoutObject)
			{
				if (endSearchObj)
				{
					if (GUILayout.Button ("Search on List", EditorStyles.miniButton))
					{
						onListObj = true;
					}

					if (GUILayout.Button ("Clear Object", EditorStyles.miniButton))
					{
						InfoOnPrefab = new List<ObjectParent> ( );
					}
				}
				else if (GUILayout.Button ("Stop search on Object", EditorStyles.miniButton))
				{
					StopPlace (TypePlace.OnObject);
				}

				scrollPosPref = EditorGUILayout.BeginScrollView (scrollPosPref);
				aPagePref = LayoutSearch (getAllOnPrefab, bPref, fPref, aPagePref, childPref, MaxCountObj);
				EditorGUILayout.EndScrollView ( );
			}
			EditorGUILayout.EndVertical ( );
			EditorGUILayout.EndHorizontal ( );
		}
		#endregion
		if (!cutSize)
		{
			EditorGUILayout.EndHorizontal ( );
		}
		else
		{
			EditorGUILayout.EndVertical ( );
		}
		#endregion
	}

	int LayoutSearch (List<ObjectParent> ObjAndParent, List<int> bPage, List<bool> fDout, int aPage, bool ifChild, int totalNbr)
	{
		EditorGUILayout.Space ( );
		EditorGUILayout.Space ( );

		List<GameObject> CurrList;
		//List<GameObject> addToList;
		//GameObject currIO;
		Transform currParent;
		Transform bigParent;
		Transform checkParent;

		float sizeX = position.width;

		//bool getParent = false;
		bool allResearch = false;
		//bool checkOAP;

		int a = 0;
		int b;
		//int c;
		int isParent = 0;
		//int countOAP;
		int getindentLevel = 0;
		int getMod;

		if (sizeX > 700)
		{
			if (AllObjectProject.Count > 0)
			{
				a++;
			}
			if (InfoOnPrefab.Count > 0)
			{
				a++;
			}
			if (AllObjectScene.Count > 0)
			{
				a++;
			}

			if (sizeX / a < 700)
			{
				allResearch = true;
			}
		}
		else
		{
			allResearch = true;
		}

		totalNbr = 0;
		for (a = 0; a < ObjAndParent.Count; a++)
		{
			totalNbr += ObjAndParent [a].AllObj.Count;
		}

		if (ObjAndParent.Count > 10)
		{
			getMod = ObjAndParent.Count % 10;
			if (getMod == 0)
			{
				getMod = 1;
			}
			else
			{
				getMod = 0;
			}

			EditorGUILayout.BeginHorizontal ( );
			EditorGUILayout.PrefixLabel ("Page Parent : " + (ObjAndParent.Count / 10 - 1).ToString ( ));
			aPage = EditorGUILayout.IntSlider (aPage, 0, ObjAndParent.Count / 10 - getMod);
			EditorGUILayout.EndHorizontal ( );
		}

		EditorGUILayout.PrefixLabel ("Total Found : " + totalNbr);
		EditorGUILayout.Space ( );

		for (a = aPage * 10; a < 10 * (aPage + 1); a++)
		{
			if (a >= ObjAndParent.Count)
			{
				break;
			}

			CurrList = ObjAndParent [a].AllObj;

			while (CurrList.Count > 0 && CurrList [0] == null)
			{
				CurrList.RemoveAt (0);
			}

			if (CurrList.Count == 0)
			{
				ObjAndParent.RemoveAt (a);
				a--;
				if (!allResearch)
				{
					EditorGUILayout.BeginHorizontal ( );
				}

				continue;
			}
			else if (!allResearch)
			{
				EditorGUILayout.BeginHorizontal ( );
			}

			EditorGUI.indentLevel = 0;

			if (CurrList [0].transform.parent == null)
			{
				EditorGUILayout.BeginHorizontal ( );
				EditorGUILayout.ObjectField ("Also Base", CurrList [0], typeof (GameObject), true);
				EditorGUILayout.EndHorizontal ( );
				isParent = 1;
			}
			else
			{
				isParent = 0;
				EditorGUILayout.ObjectField ("Base Parent", ObjAndParent [a].CurrParent.gameObject, typeof (GameObject), true);
			}

			if (CanRemove && GUILayout.Button ("Remove this List", EditorStyles.miniButton))
			{
				CurrList.Clear ( );

				if (!allResearch)
				{
					EditorGUILayout.EndHorizontal ( );
				}
				a--;
				continue;
			}
			else if (!allResearch)
			{
				EditorGUILayout.EndHorizontal ( );
			}

			while (bPage.Count - 1 < a)
			{
				bPage.Add (0);
				fDout.Add (false);
			}

			//getParent = false;

			if (ifChild)
			{
				if (allResearch)
				{
					EditorGUILayout.BeginVertical ( );
				}
				else
				{
					EditorGUILayout.BeginHorizontal ( );
				}

				if (allResearch)
				{
					EditorGUILayout.EndVertical ( );
				}
				else
				{
					EditorGUILayout.EndHorizontal ( );
				}

				//getParent = true;
				if (CurrList.Count > isParent)
				{
					EditorGUI.indentLevel = 1;
					fDout [a] = EditorGUILayout.Foldout (fDout [a], "Display Children : " + (CurrList.Count).ToString ( ));
				}

				EditorGUI.indentLevel = 2;
			}
			else
			{
				fDout [a] = true;
			}

			if (fDout [a])
			{
				EditorGUILayout.BeginVertical ( );

				if (allResearch)
				{
					EditorGUILayout.BeginVertical ( );
				}
				else
				{
					EditorGUILayout.BeginHorizontal ( );
				}

				if (allResearch)
				{
					EditorGUILayout.EndVertical ( );
				}
				else
				{
					EditorGUILayout.EndHorizontal ( );
				}

				if (CurrList.Count > 10)
				{
					getMod = CurrList.Count % 10;
					if (getMod == 0)
					{
						getMod = 1;
					}
					else
					{
						getMod = 0;
					}
					EditorGUILayout.BeginHorizontal ( );
					EditorGUILayout.PrefixLabel ("Page Child : " + (CurrList.Count / 10).ToString ( ));
					bPage [a] = EditorGUILayout.IntSlider (bPage [a], 0, CurrList.Count / 10 - getMod);
					EditorGUILayout.EndHorizontal ( );
				}
				else
				{
					bPage [a] = 0;
				}

				bigParent = CurrList [0].transform;
				currParent = bigParent;
				while (bigParent.parent != null)
				{
					bigParent = bigParent.parent;
				}

				getindentLevel = EditorGUI.indentLevel;

				for (b = bPage [a] * 10 + isParent; b < 10 * (bPage [a] + 1); b++)
				{
					if (b >= CurrList.Count)
					{
						break;
					}

					if (CurrList [b] == null)
					{
						CurrList.RemoveAt (b);
						b--;
						continue;
					}

					EditorGUILayout.BeginVertical ( );
					checkParent = CurrList [b].transform.parent;

					EditorGUI.indentLevel = getindentLevel + 1;
					if (DisplayOtherParent && checkParent != bigParent && (b == 0 || currParent != checkParent))
					{
						if (b != bPage [a] * 10)
						{
							EditorGUILayout.Space ( );
							EditorGUILayout.Space ( );
						}

						currParent = checkParent;
						EditorGUILayout.ObjectField ("Current Parent", currParent.gameObject, typeof (GameObject), true);
					}
					EditorGUI.indentLevel = getindentLevel + 2;

					EditorGUILayout.BeginHorizontal ( );
					EditorGUILayout.ObjectField (CurrList [b], typeof (GameObject), true);

					if (CanRemove && GUILayout.Button ("Remove From List", EditorStyles.miniButton))
					{
						CurrList.RemoveAt (b);

						EditorGUILayout.EndHorizontal ( );
						EditorGUILayout.EndVertical ( );
						b--;
						continue;
					}

					EditorGUILayout.EndHorizontal ( );
					EditorGUILayout.EndVertical ( );
				}
				EditorGUILayout.EndVertical ( );
			}

			EditorGUILayout.Space ( );
			EditorGUILayout.Space ( );
		}

		return aPage;
	}

	void copyList(List<ObjectParent> listScene, List<ObjectParent> listProject, List<ObjectParent> listObj)
	{
		List<GameObject> AllObj = new List<GameObject>();
		AllObj.AddRange(returnObj(listProject));
		AllObj.AddRange(returnObj(listScene));
		AllObj.AddRange(returnObj(listObj));

		if(AllObj.Count == 0)
		{
			return;
		}

		ScriptableList thisScriptable =	ScriptableObjectUtility.CreateListCopy ( );

		thisScriptable.AllObj = AllObj.ToArray();

		if(newString == string.Empty )
		{
			newString = "Default Name";
		}

		AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(thisScriptable), newString);
	}

	GameObject[] returnObj (List<ObjectParent> thisList)
	{
		List<GameObject> getAllObj = new List<GameObject>();
		List<GameObject> currList;
		GameObject thisObj;

		int getLenght = thisList.Count;

		for (int a = 0; a < getLenght; a++)
		{
			currList = thisList[a].AllObj;
			for (int b = 0; b < currList.Count; b++)
			{
				if(currList[b] == null)
				{
					continue;
				}
		
				thisObj = currList[b];

				if( EditorUtility.IsPersistent(thisObj))
				{
					getAllObj.Add(thisObj);
				}
				else if ( PrefabUtility.FindPrefabRoot(thisObj)!= null && PrefabUtility.GetPrefabParent(thisObj) != null )
				{
					getAllObj.Add((GameObject) PrefabUtility.GetPrefabParent(thisObj));
				}
				else if (setPrefb )
				{	
					Transform getParent =  thisObj.transform;
					while ( getParent.parent != null)
					{
						getParent = thisObj.transform.parent;
					}

					if(!AssetDatabase.IsValidFolder("Assets/NewPrefabList"))
					{
						AssetDatabase.CreateFolder("Assets","NewPrefabList" );
					}

					GameObject getPrefab = PrefabUtility.CreatePrefab("Assets/NewPrefabList/"+getParent.name+".prefab", getParent.gameObject);
					PrefabUtility.ConnectGameObjectToPrefab(getParent.gameObject, getPrefab);

					getAllObj.Add (getPrefab);
				}
			}
		}

		return getAllObj.ToArray();
	}

	void modifLayer (List<ObjectParent> listSearch, int thisLayer)
	{
		ObjectParent [ ] getList = listSearch.ToArray ( );
		GameObject [ ] currList;

		int a;
		int b;

		for (a = 0; a < getList.Length; a++)
		{
			currList = getList [a].AllObj.ToArray ( );

			for (b = 0; b < currList.Length; b++)
			{
				currList [b].layer = thisLayer;
			}
		}
	}

	void modifTag (List<ObjectParent> listSearch, string thisTag)
	{
		ObjectParent [ ] getList = listSearch.ToArray ( );
		GameObject [ ] currList;

		int a;
		int b;

		for (a = 0; a < getList.Length; a++)
		{
			currList = getList [a].AllObj.ToArray ( );

			for (b = 0; b < currList.Length; b++)
			{
				currList [b].tag = thisTag;
			}
		}
	}

	void modifName (List<ObjectParent> listSearch, string thisName)
	{
		ObjectParent [ ] getList = listSearch.ToArray ( );
		GameObject [ ] currList;

		int a;
		int b;
		string getAssetPath;
		for (a = 0; a < getList.Length; a++)
		{
			currList = getList [a].AllObj.ToArray ( );

			for (b = 0; b < currList.Length; b++)
			{
				if (currList [b].transform.parent == null)
				{
					getAssetPath = AssetDatabase.GetAssetPath ((Object)currList [b]);

					if (getAssetPath != string.Empty && getAssetPath != null)
					{
						Debug.LogWarning ("Cannont change the name on Prefab Source : " + currList [b].name);
					}
					else
					{
						currList [b].name = thisName;
					}
				}
				else
				{
					currList [b].name = thisName;
				}
			}
		}
	}

	void modifComp (List<ObjectParent> listSearch, Object thisObj, Object otherObj = null)
	{
		ObjectParent [ ] getList = listSearch.ToArray ( );
		GameObject [ ] currList;
		Component [ ] thoseComp;
		//Component currComp;
		CompAction thisAct = CAction;

		System.Reflection.FieldInfo [ ] getField = updateCompAdv.ToArray ( );
		System.Reflection.FieldInfo [ ] getCurrField;

		int a;
		int b;
		int c;
		int d;
		int e;
		int countThis = 0;

		for (a = 0; a < getList.Length; a++)
		{
			currList = getList [a].AllObj.ToArray ( );

			for (b = 0; b < currList.Length; b++)
			{
				try
				{
					switch (thisAct)
					{
						case CompAction.Add:
							UnityEditorInternal.ComponentUtility.CopyComponent (thisObj as Component);
							UnityEditorInternal.ComponentUtility.PasteComponentAsNew (currList [b]);
							break;
						case CompAction.Remove:
							thoseComp = currList [b].GetComponents<Component> ( );

							for (c = 0; c < thoseComp.Length; c++)
							{
								if (thoseComp [c].GetType ( )== thisObj.GetType ( ))
								{
									DestroyImmediate (thoseComp [c], true);
								}
							}
							break;
						case CompAction.Replace:
							thoseComp = currList [b].GetComponents<Component> ( );

							for (c = 0; c < thoseComp.Length; c++)
							{
								if (thoseComp [c].GetType ( )== thisObj.GetType ( ))
								{
									UnityEditorInternal.ComponentUtility.CopyComponent (otherObj as Component);
									UnityEditorInternal.ComponentUtility.PasteComponentAsNew (currList [b]);
									DestroyImmediate (thoseComp [c], true);
								}
							}
							break;
						case CompAction.RemoveIfMany:
							thoseComp = currList [b].GetComponents<Component> ( );
							countThis = 0;
							for (c = 0; c < thoseComp.Length; c++)
							{
								if (thoseComp [c].GetType ( )== thisObj.GetType ( ))
								{
									if (countThis > 0)
									{
										UnityEditorInternal.ComponentUtility.CopyComponent (otherObj as Component);
										UnityEditorInternal.ComponentUtility.PasteComponentAsNew (currList [b]);
										DestroyImmediate (thoseComp [c], true);
									}
									else
									{
										countThis++;
									}
								}
							}
							break;
						case CompAction.AddIfEmpty:
							thoseComp = currList [b].GetComponents<Component> ( );
							countThis = 0;
							for (c = 0; c < thoseComp.Length; c++)
							{
								if (thoseComp [c].GetType ( )== thisObj.GetType ( ))
								{
									countThis++;
								}
							}

							if (countThis == 0)
							{
								UnityEditorInternal.ComponentUtility.CopyComponent (thisObj as Component);
								UnityEditorInternal.ComponentUtility.PasteComponentAsNew (currList [b]);
							}
							break;
						case CompAction.UpdateValue:
							thoseComp = currList [b].GetComponents<Component> ( );

							if (getField.Length == 0)
							{
								break;
							}

							for (c = 0; c < thoseComp.Length; c++)
							{
								if (thoseComp [c] != null && thoseComp [c].GetType ( )== objGA.GetType ( ))
								{
									getCurrField = thoseComp [c].GetType ( ).GetFields ( );
									for (d = 0; d < getField.Length; d++)
									{
										for (e = 0; e < getCurrField.Length; e++)
										{
											if (getField [d].GetType ( )== getCurrField [e].GetType ( )&& getField [d].Name == getCurrField [e].Name)
											{
												getField [d].SetValue (thoseComp [c], getCurrField [e].GetValue (thisObj));
											}
										}
									}
								}
							}
							break;
					}
				}
				/*catch
				{
					Debug.LogError ( "Cannot apply on this Object : " + currList [ b ].name );
				}*/
				catch (System.Reflection.TargetInvocationException f)
				{
					Debug.LogError (" Exception: " + f.InnerException.Message + "\n" + f.InnerException.StackTrace);
				}

			}
		}
	}

	void modifRef (List<ObjectParent> listSearch, Object thisObj, Object otherObj = null)
	{
		ObjectParent [ ] getList = listSearch.ToArray ( );
		GameObject [ ] currList;
		Component [ ] thoseComp;

		object currField;

		int a;
		int b;
		int c;

		for (a = 0; a < getList.Length; a++)
		{
			currList = getList [a].AllObj.ToArray ( );

			for (b = 0; b < currList.Length; b++)
			{
				thoseComp = currList [b].GetComponents<Component> ( );
				for (c = 0; c < thoseComp.Length; c++)
				{
					if (thoseComp [c] == null)
					{
						continue;
					}

					if (thoseComp [c].GetType ( ).GetFields ( ).Length > 0)
					{
						foreach (var field in thoseComp [c].GetType ( ).GetFields ( ))
						{
							try
							{
								currField = field.GetValue (thoseComp [c])as object;

								if (currField.GetType ( ).IsGenericType)
								{
									RefOnList (currField as System.Collections.ICollection, thisObj, otherObj);
								}
								else if (currField == thisObj)
								{
									try
									{
										field.SetValue (thoseComp [b], otherObj);
									}
									catch
									{
										Debug.LogError ("Cannot apply on this Object : " + thoseComp [c].GetType ( ).Name);
									}
								}
							}
							catch
							{ }
						}
					}
				}
			}
		}
	}

	static void RefOnList (System.Collections.ICollection thisList, Object thisOjb, Object otherObj = null)
	{
		object thisVal;
		//string thisName;
		//int a;

		foreach (var thisValue in thisList)
		{
			foreach (var field in thisValue.GetType ( ).GetFields ( ))
			{
				try
				{
					thisVal = field.GetValue (thisValue)as object;

					//thisName = thisVal.ToString ();

					if (thisVal.GetType ( ).IsGenericType)
					{
						RefOnList (thisVal as System.Collections.ICollection, thisOjb, otherObj);
					}
					else if (thisVal == thisOjb)
					{
						try
						{
							field.SetValue (thisValue, otherObj);
						}
						catch
						{
							Debug.LogError ("Cannot apply on this Object : " + thisValue.GetType ( ).Name);
						}
					}
				}
				catch
				{ }
			}
		}
	}

	void deleteMiss (List<ObjectParent> listSearch)
	{
		ObjectParent [ ] getList = listSearch.ToArray ( );
		GameObject [ ] currList;
		Component [ ] components;
		//		GameObject CurrObj;

		int a;
		int b;
		int c;
		int currInd;

		for (a = 0; a < getList.Length; a++)
		{
			currList = getList [a].AllObj.ToArray ( );

			for (b = 0; b < currList.Length; b++)
			{
				var serializedObject = new SerializedObject (currList [b]);
				serializedObject.Update ( );

				var prop = serializedObject.FindProperty ("m_Component");
				components = currList [b].GetComponents<Component> ( );
				currInd = 0;

				for (c = 0; c < components.Length; c++)
				{
					if (components [c] == null)
					{
						prop.DeleteArrayElementAtIndex (c - currInd);

						currInd++;
					}
				}

				serializedObject.ApplyModifiedProperties ( );
			}
		}
	}

	void modifPref (List<ObjectParent> listSearch, GameObject thisObj)
	{
		ObjectParent [ ] getList = listSearch.ToArray ( );
		//List<objectInfo> allComp;
		//Component [] m_List;
		//Transform allCompTrans;
		//Transform listChildTrans;
		Quaternion getCurrRot;
		Vector3 getCurr;

		Transform getBasePart;

		GameObject getNewObj;
		GameObject getNewParent;

		GameObject [ ] currList;
		//GameObject [] listChild;

		Component [ ] components;
		Component [ ] componentsPref;

		List<InfoParent> parentUpdate = new List<InfoParent> ( );

		int a;
		int b;
		int c;
		//int d;
		//int e;
		int countList;

		bool checkComp;
		//bool checkChild;

		string getAssetPath;

		//		allComp = CompInfo;

		getBasePart = thisObj.transform;

		while (getBasePart.parent != null)
		{
			getBasePart = getBasePart.parent;
		}

		for (a = 0; a < getList.Length; a++)
		{
			currList = getList [a].AllObj.ToArray ( );
			countList = currList.Length - 1;

			getBasePart = getList [a].CurrParent;
			getAssetPath = AssetDatabase.GetAssetPath ((Object)getBasePart);

			if (getAssetPath != string.Empty && getAssetPath != null)
			{
				getNewParent = (GameObject)Instantiate (getList [a].CurrParent.gameObject);
				parentUpdate.Add (new InfoParent ( ));
				parentUpdate [parentUpdate.Count - 1].ThisObj = getNewParent;
				parentUpdate [parentUpdate.Count - 1].ThisParent = getBasePart;

				foreach (Transform currT in getNewParent.GetComponentsInChildren<Transform> (true))
				{
					if (!currT)
					{
						continue;
					}

					for (b = 0; b < countList + 1; b++)
					{
						if ((currT.name == currList [b].name || currT.name == currList [b].name + "(Clone)")&& !currT.Equals (thisObj))
						{
							components = currT.GetComponents<Component> ( );
							componentsPref = currList [b].GetComponents<Component> ( );

							if (components.Length != componentsPref.Length || currT.localPosition != currList [b].transform.localPosition)
							{
								continue;
							}

							checkComp = true;

							for (c = 0; c < components.Length; c++)
							{
								if (components [c] == null || componentsPref [c] == null)
								{
									if (components [c] != null || componentsPref [c] != null)
									{
										checkComp = false;
										break;
									}
								}
								else if (components [c].GetType ( )!= componentsPref [c].GetType ( ))
								{
									checkComp = false;
									break;
								}
							}

							if (!checkComp)
							{
								continue;
							}

							getCurr = currT.localPosition;
							getCurrRot = currT.localRotation;

							if (currT.parent != null)
							{
								getNewObj = (GameObject)Instantiate (thisObj, currT.parent);
								getNewObj.name = thisObj.name;
								getNewObj.transform.localPosition = getCurr;
								getNewObj.transform.localRotation = getCurrRot;

								DestroyImmediate (currT.gameObject, true);
							}
							else
							{
								getNewObj = (GameObject)Instantiate (thisObj);
								getNewObj.transform.localPosition = getCurr;
								getNewObj.transform.localRotation = getCurrRot;

								Debug.LogWarning ("Cannot change the name on Source Prefab : " + getNewObj.name);

								parentUpdate [parentUpdate.Count - 1].ThisObj = getNewObj;

								DestroyImmediate (getNewParent.gameObject, true);
							}
							break;
						}
					}
				}
			}
			else
			{
				for (b = 0; b < countList + 1; b++)
				{
					if (currList [b] == null)
					{
						getList [a].AllObj.RemoveAt (b);
						b--;
						continue;
					}
					else if (!currList [b].Equals (thisObj))
					{
						getCurr = currList [b].transform.localPosition;
						getCurrRot = currList [b].transform.localRotation;

						getNewObj = (GameObject)Instantiate (thisObj, currList [b].transform.parent);
						DestroyImmediate (currList [b].gameObject, true);

						getNewObj.name = thisObj.name;
						getNewObj.transform.localPosition = getCurr;
						getNewObj.transform.localRotation = getCurrRot;
						getList [a].AllObj [b] = getNewObj;
					}
					// opti sur l'instanciation / destruction d'object : vérifier que le prochain obj n'as pas le meme parent sinon ne pas détruire, etc
					// donner une option qui permet à l'utilisateur de : tout remplacer / choisir les composants a mettre a jour / choisir les fields a mettre a jour

					/*if ( replace )
				{
					if ( !listSearch [ a ].Equals ( allComp ) )
					{
						Debug.Log ( AssetDatabase.GetAssetPath ( listSearch [ a ] [ b ] ) );
						getCurr = listSearch [ a ] [ b ].transform.localPosition;
						getCurrRot = listSearch [ a ] [ b ].transform.localRotation;

						if ( getAssetPath != null && getAssetPath != string.Empty )
						{
						}
						else
						{
							getNewObj = ( GameObject ) Instantiate ( thisObj, listSearch [ a ] [ b ].transform.parent );
							DestroyImmediate ( listSearch [ a ] [ b ].gameObject, true );
						}

						getNewObj.name = thisObj.name;
						getNewObj.transform.localPosition = getCurr;
						getNewObj.transform.localRotation = getCurrRot;
					}
				}*/
					/*else
					{
						listChild = SearchObject.GetComponentsInChildrenOfAsset ( listSearch [ a ] [ b ] );

						for ( c = 0; c < allComp.Count; c++ )
						{
							checkChild = false;
							for ( d = 0; d < listChild.Length; d++ )
							{
								checkParent = false;
								if ( listChild [ d ].name.Length >= allComp [ c ].ThisObj.name.Length && allComp [ c ].ThisObj.name == listChild [ d ].name.Substring ( 0, allComp [ c ].ThisObj.name.Length ) )
								{
									allCompTrans = allComp [ c ].ThisObj.transform;
									listChildTrans = listChild [ d ].transform;

									if ( allCompTrans.parent == null )
									{
										checkParent = true;
									}
									else if ( allCompTrans.parent != null && listChildTrans.parent != null )
									{
										if ( listChildTrans.parent.name.Length >= allCompTrans.parent.name.Length && allCompTrans.parent.name == listChildTrans.parent.name.Substring ( 0, allCompTrans.parent.name.Length ) )
										{
											checkParent = true;
										}
									}

									if ( checkParent )
									{
										checkChild = true;
										EditorUtility.SetDirty ( listChild [ d ] );

										getCurr = listChildTrans.localPosition;
										getCurrRot = listChildTrans.localRotation;

										m_List = listChildTrans.GetComponents<Component>();

										for ( e = 0; e < allComp [ c ].thoseComp.Length; e++ )
										{
											if ( listChildTrans.GetComponent ( allComp [ c ].thoseComp [ e ].GetType ( ) ) == null )
											{
												listChildTrans.gameObject.AddComponent ( allComp [ c ].thoseComp [ e ].GetType ( ) );
											}

											UnityEditorInternal.ComponentUtility.CopyComponent ( allComp [ c ].thoseComp [ e ] );
											UnityEditorInternal.ComponentUtility.PasteComponentValues ( listChild [ d ].GetComponent ( allComp [ c ].thoseComp [ e ].GetType ( ) ) );
										}

										for ( e = 0; e < m_List.Length; e++ )
										{
											if ( allComp [ c ].ThisObj.GetComponent ( m_List [ e ].GetType ( ) ) == null )
											{
												DestroyImmediate ( listChild [ d ].GetComponent ( m_List [ e ].GetType ( ) ), true );
											}
										}

										listChildTrans.localPosition = getCurr;
										listChildTrans.localRotation = getCurrRot;
										break;
									}
								}
							}

							if ( !checkChild )
							{
								getCurr = allComp [ c ].ThisObj.transform.localPosition;
								getCurrRot = allComp [ c ].ThisObj.transform.localRotation;

								getNewObj = ( GameObject ) Instantiate ( allComp [ c ].ThisObj, listSearch [ a ] [ b ].transform );

								getNewObj.name = allComp [ c ].ThisObj.name;
								getNewObj.transform.localPosition = getCurr;
								getNewObj.transform.localRotation = getCurrRot;
								listChild = SearchObject.GetComponentsInChildrenOfAsset ( listSearch [ a ] [ b ] );
							}
						}
					}*/
				}
			}
		}

		for (a = 0; a < parentUpdate.Count; a++)
		{
			if (parentUpdate [a].ThisObj == null || parentUpdate [a].ThisParent == null)
			{
				continue;
			}

			PrefabUtility.ReplacePrefab (parentUpdate [a].ThisObj, parentUpdate [a].ThisParent.gameObject, ReplacePrefabOptions.ReplaceNameBased);
			DestroyImmediate (parentUpdate [a].ThisObj, true);
		}
		listSearch.Clear ( );
	}

	void advancedFilt (float sizeX)
	{
		var buttonStyle = new GUIStyle (EditorStyles.miniButton);
		RectOffset thisOf = buttonStyle.margin;

		EditorGUI.indentLevel = 2;
		Color defColor = GUI.backgroundColor;

		switch (thisType)
		{
			case ResearcheType.Component:

				if (advanceEnable)
				{
					GUI.backgroundColor = Color.green;
				}

				buttonStyle.margin = new RectOffset (35, thisOf.right, thisOf.top, thisOf.bottom);

				if (getAdvanceComp.Count > 0 && GUILayout.Button ("Same value", buttonStyle, GUILayout.Width (sizeX / 3 - 0.13f)))
				{
					advanceEnable = !advanceEnable;

					if (advanceEnable)
					{
						advanceCompOther.Clear ( );
						getAdvanceComp.Clear ( );
						//getAdvanceProp.Clear ( );
						//advancePropOther.Clear ( );

						if (objComp != null)
						{
							getAdvanceComp.AddRange (objComp.GetType ( ).GetFields ( ));

							/*if ( getAdvanceComp.Count == 0 )
							{
								getAdvanceProp.AddRange ( objComp.GetType ( ).GetProperties ( ) );
							}*/
						}
					}
				}

				GUI.backgroundColor = defColor;

				if (advanceEnable)
				{
					int a = (advanceCompOther.Count + getAdvanceComp.Count)* 15; //+ getAdvanceProp.Count * 10;

					if (a > 100)
					{
						a = 100;
					}

					AdvancedResearch = EditorGUILayout.BeginScrollView (AdvancedResearch, GUILayout.Height (a));
					if (getAdvanceComp.Count > 0)
					{
						for (a = 0; a < getAdvanceComp.Count; a++)
						{
							EditorGUI.indentLevel = 2;

							EditorGUILayout.BeginHorizontal ( );
							EditorGUILayout.PrefixLabel (getAdvanceComp [a].Name);
							EditorGUI.indentLevel = 5;

							if (GUILayout.Button ("Don't check", EditorStyles.miniButton, GUILayout.Width (sizeX / 3)))
							{
								advanceCompOther.Add (getAdvanceComp [a]);
								getAdvanceComp.RemoveAt (a);
								a--;
							}
							EditorGUILayout.EndHorizontal ( );
						}
						EditorGUI.indentLevel = 2;

						for (a = 0; a < advanceCompOther.Count; a++)
						{
							EditorGUI.indentLevel = 2;

							EditorGUILayout.BeginHorizontal ( );
							EditorGUILayout.PrefixLabel (advanceCompOther [a].Name);
							EditorGUI.indentLevel = 5;

							if (GUILayout.Button ("Check", EditorStyles.miniButton, GUILayout.Width (sizeX / 3)))
							{
								getAdvanceComp.Add (advanceCompOther [a]);
								advanceCompOther.RemoveAt (a);
								a--;
							}
							EditorGUILayout.EndHorizontal ( );
						}
					}
					/*else
					{
						for ( a = 0; a < getAdvanceProp.Count; a++ )
						{
							EditorGUI.indentLevel = 2;

							EditorGUILayout.BeginHorizontal ( );
							EditorGUILayout.PrefixLabel ( getAdvanceProp [ a ].Name );
							EditorGUI.indentLevel = 5;

							if ( GUILayout.Button ( "Don't check", EditorStyles.miniButton, GUILayout.Width ( sizeX / 3 ) ) )
							{
								advancePropOther.Add ( getAdvanceProp [ a ] );
								getAdvanceProp.RemoveAt ( a );
								a--;
							}
							EditorGUILayout.EndHorizontal ( );
						}
						EditorGUI.indentLevel = 2;

						for ( a = 0; a < advancePropOther.Count; a++ )
						{
							EditorGUI.indentLevel = 2;

							EditorGUILayout.BeginHorizontal ( );
							EditorGUILayout.PrefixLabel ( advancePropOther [ a ].Name );
							EditorGUI.indentLevel = 5;

							if ( GUILayout.Button ( "Check", EditorStyles.miniButton, GUILayout.Width ( sizeX / 3 ) ) )
							{
								getAdvanceProp.Add ( advancePropOther [ a ] );
								advancePropOther.RemoveAt ( a );
								a--;
							}
							EditorGUILayout.EndHorizontal ( );
						}
					}*/
					EditorGUILayout.EndScrollView ( );
				}
				break;
			case ResearcheType.Reference:
				EditorGUILayout.Space ( );

				if (advanceEnable)
				{
					GUI.backgroundColor = Color.green;
				}

				buttonStyle.margin = new RectOffset (35, thisOf.right, thisOf.top, thisOf.bottom);

				if (GUILayout.Button ("Search on Properties", buttonStyle, GUILayout.Width (sizeX / 3)))
				{
					advanceEnable = !advanceEnable;
				}
				break;
			case ResearcheType.Object_Prefab:
				EditorGUILayout.BeginVertical ( );

				EditorGUILayout.BeginHorizontal ( );
				if (ApproximateObj)
				{
					GUI.backgroundColor = Color.green;
				}

				buttonStyle.margin = new RectOffset (35, thisOf.right, thisOf.top, thisOf.bottom);

				if (GUILayout.Button ("Approximate components", buttonStyle, GUILayout.Width (sizeX / 2 - 17.5f)))
				{
					ApproximateObj = !ApproximateObj;
				}

				buttonStyle = new GUIStyle (EditorStyles.miniButton);

				GUI.backgroundColor = defColor;

				if (approxName)
				{
					GUI.backgroundColor = Color.green;
				}

				if (GUILayout.Button ("Approximate name", buttonStyle, GUILayout.Width (sizeX / 2 - 17.5f)))
				{
					approxName = !approxName;
				}
				EditorGUILayout.EndHorizontal ( );

				GUI.backgroundColor = defColor;

				specName = EditorGUILayout.TextField ("Other Name ?", specName, GUILayout.Width (sizeX));

				if (ApproximateObj)
				{
					compDiff = (int)EditorGUILayout.IntSlider ("Max component gap", compDiff, 0, 10, GUILayout.Width (sizeX));
					childDiff = (int)EditorGUILayout.IntSlider ("Max child gap", childDiff, 0, 500, GUILayout.Width (sizeX));
				}

				EditorGUILayout.EndVertical ( );
				break;

			case ResearcheType.Name:
				buttonStyle = new GUIStyle (EditorStyles.miniButton);

				if (approxName)
				{
					GUI.backgroundColor = Color.green;
				}

				buttonStyle.margin = new RectOffset (35, thisOf.right, thisOf.top, thisOf.bottom);

				if (GUILayout.Button ("Approximate name", buttonStyle, GUILayout.Width (sizeX / 2)))
				{
					approxName = !approxName;
				}

				break;
		}
		EditorGUILayout.Space ( );

		GUI.backgroundColor = defColor;

		buttonStyle = new GUIStyle (EditorStyles.miniButton);

		GUI.backgroundColor = defColor;

		EditorGUILayout.BeginHorizontal ( );
		if (getChildren)
		{
			GUI.backgroundColor = Color.green;
		}

		buttonStyle.margin = new RectOffset (35, thisOf.right, thisOf.top, thisOf.bottom);

		if (GUILayout.Button ("Search On Children", buttonStyle, GUILayout.Width (sizeX / 3 - 13f)))
		{
			getChildren = !getChildren;
		}

		GUI.backgroundColor = defColor;
		buttonStyle = new GUIStyle (EditorStyles.miniButton);

		if (CanRemove)
		{
			GUI.backgroundColor = Color.green;
		}

		if (GUILayout.Button ("Custom List", buttonStyle, GUILayout.Width (sizeX / 3 - 13f)))
		{
			CanRemove = !CanRemove;
		}

		GUI.backgroundColor = defColor;

		if (DisplayOtherParent)
		{
			GUI.backgroundColor = Color.green;
		}

		if (GUILayout.Button ("Detail list", buttonStyle, GUILayout.Width (sizeX / 3 - 13f)))
		{
			DisplayOtherParent = !DisplayOtherParent;
		}

		GUI.backgroundColor = defColor;

		EditorGUILayout.EndHorizontal ( );
		EditorGUI.indentLevel = 0;
	}

	InfoResearch setInfR ( )
	{
		InfoResearch currResearch = infResearch;

		currResearch.StringSearch = thisStringSearch;
		currResearch.NbrCompDiff = compDiff;
		currResearch.NbrChildDiff = childDiff;
		currResearch.OtherName = specName;
		currResearch.SameObj = !ApproximateObj;
		currResearch.LargeName = approxName;
		//currResearch.properCheck = getAdvanceProp;
		currResearch.fieldCheck = getAdvanceComp;
		currResearch.checkFieldComp = advanceEnable;

		if (SpecificPath != null)
		{
			currResearch.FolderProject = AssetDatabase.GetAssetOrScenePath (SpecificPath);
		}

		return currResearch;
	}

	public static void SetCorout (EditorCoroutine thisCorou, TypePlace thisPlace)
	{
		List<EditorCoroutine> getList;
		if (getAllCorou.TryGetValue (thisPlace, out getList))
		{
			getList.Add (thisCorou);
		}
		else
		{
			getList = new List<EditorCoroutine> ( );
			getList.Add (thisCorou);
			getAllCorou.Add (thisPlace, getList);
		}
	}

	public static void AddCount (TypePlace thisPlace)
	{
		switch (thisPlace)
		{
			case TypePlace.OnProject:
				CurrCountProj++;
				if (CurrCountProj == MaxCountProj)
				{
					EndResearch (thisPlace);
				}
				break;
			case TypePlace.OnScene:
				CurrCountScene++;
				if (CurrCountScene == MaxCountScene)
				{
					EndResearch (thisPlace);
				}
				break;
			case TypePlace.OnObject:
				CurrCountObj++;
				if (CurrCountObj == MaxCountObj)
				{
					EndResearch (thisPlace);
				}
				break;
		}
	}

	public static void AssetLoading (bool LoadAsset, int maxToLoad)
	{
		NbrAssetLoad = 0;
		MaxAssetToLoad = maxToLoad;
		assetLoading = LoadAsset;
	}

	public static void PlusAssetLoaded ( )
	{
		NbrAssetLoad++;
	}

	public static void MaxCount (int maxNbr, TypePlace thisPlace)
	{
		switch (thisPlace)
		{
			case TypePlace.OnProject:
				MaxCountProj = maxNbr;
				break;
			case TypePlace.OnScene:
				MaxCountScene = maxNbr;
				break;
			case TypePlace.OnObject:
				MaxCountObj = maxNbr;
				break;
		}
	}

	public static void DeleteCorout (EditorCoroutine thisCorou, TypePlace thisPlace)
	{
		List<EditorCoroutine> getList;
		if (getAllCorou.TryGetValue (thisPlace, out getList))
		{
			getList.Remove (thisCorou);
		}
	}

	public static void StopPlace (TypePlace thisPlace)
	{
		List<EditorCoroutine> getList;
		if (getAllCorou.TryGetValue (thisPlace, out getList))
		{
			for (int a = 0; a < getList.Count; a++)
			{
				getList [a].stop ( );
			}
		}

		EndResearch (thisPlace);
	}

	public static void EndResearch (TypePlace thisPlace)
	{
		switch (thisPlace)
		{
			case TypePlace.OnProject:
				CurrCountProj = 0;
				endSearchProj = true;
				break;
			case TypePlace.OnScene:
				CurrCountScene = 0;
				endSearchScene = true;
				break;
			case TypePlace.OnObject:
				CurrCountObj = 0;
				endSearchObj = true;
				break;
		}
	}

	public void StopAll ( )
	{
		foreach (List<EditorCoroutine> allCorou in getAllCorou.Values)
		{
			for (int a = 0; a < allCorou.Count; a++)
			{
				allCorou [a].stop ( );
			}
		}
	}
}

public class objectInfo
{
	public GameObject ThisObj;
	public Component [ ] thoseComp;
}

public class InfoParent
{
	public Transform ThisParent;
	public GameObject ThisObj;
}

public class InfoResearch
{
	public string FolderProject = "";
	public string StringSearch = "";
	public int NbrCompDiff = 2;
	public int NbrChildDiff = 2;
	public string OtherName = "";
	public bool TryGetProperty = false;
	public bool SameObj = false;
	public bool LargeName = false;
	public bool checkFieldComp = false;
	public List<System.Reflection.PropertyInfo> properCheck;
	public List<System.Reflection.FieldInfo> fieldCheck;
}

public class EditorCoroutine
{
	public static EditorCoroutine start (IEnumerator _routine, TypePlace thisPlace)
	{
		ThisPlace = thisPlace;
		EditorCoroutine coroutine = new EditorCoroutine (_routine);
		EasySearch.SetCorout (coroutine, thisPlace);
		thisEdit = coroutine;
		coroutine.start ( );
		return coroutine;
	}

	static TypePlace ThisPlace;
	static EditorCoroutine thisEdit;
	readonly IEnumerator routine;

	EditorCoroutine (IEnumerator _routine)
	{
		routine = _routine;
	}

	void start ( )
	{
		EditorApplication.update += update;
	}

	public void stop ( )
	{
		EditorApplication.update -= update;
	}

	void update ( )
	{
		if (!routine.MoveNext ( ))
		{
			EasySearch.DeleteCorout (thisEdit, ThisPlace);

			stop ( );
		}
	}
}

public class ObjectParent
{
	public Transform CurrParent;
	public List<GameObject> AllObj;
}
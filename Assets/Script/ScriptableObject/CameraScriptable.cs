﻿using UnityEngine;

[CreateAssetMenu (fileName = "CameraScript", menuName = "Scriptable/Camera")]
public class CameraScriptable : ScriptableObject
{
	public Vector3 MaxDefGap;
	public float SmoothFollow;
	public float MaxSpeed;
	
}

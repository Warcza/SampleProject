﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[CreateAssetMenu (fileName = "LevelInfo", menuName = "Scriptable/LevelInfo")]
public class ScriptLevelInfo : ScriptableObject
{
	[Tooltip ("Profondeur pour ce level")]
	public int NbrDepthRoom = 10;
	[Range (0, 100)] [Tooltip ("Pourcentage qui s'addition aux autres pour en selectionner un")]
	public int MultipleDoorUnUse = 80;
	public RoomTypeSpawn [ ] RoomInfoSpawnable;
}
﻿using System.Collections.Generic;

using UnityEngine;

[CreateAssetMenu (fileName = "AudioScript", menuName = "Scriptable/Audio")]
public class AudioScriptable : ScriptableObject
{
	#region Variable
	public MusicFX ThisMusic;
	#endregion

	#region Mono

	#endregion

	#region Public
	#endregion

	#region Private
	#endregion
}

[System.Serializable]
public struct MusicFX
{
	public CustomString CategorieName;
	public AudioType ThisType;
	public AllAudio [ ] SetAudio;
}

[System.Serializable]
public struct AllAudio
{
	public CustomString AudioName;
	[Range (0, 1)] public float Volume;
	[Range (0, 3)] public float Pitch;
	public AudioClip[] Audio;
	public bool AudioLoop;
	public bool StartPlay;
}
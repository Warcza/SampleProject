﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[CreateAssetMenu (fileName = "Room", menuName = "Scriptable/Room")]
public class ScriptRoomSetting : ScriptableObject
{
	public RoomType ThisType;
	public int LevelRoom;
	public GameObject [ ] AllRoom;
}
﻿using UnityEngine;

[CreateAssetMenu (fileName = "Script_Type", menuName = "Scriptable/Script_Type")]
public class TypeScriptable : ScriptableObject 
{
	#region Variables
	public ElementType GroupeType = ElementType.Nothing;
	public GameObject[] ThisObj;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	#endregion

	#region Private Methodes
	#endregion
}

public enum ElementType
{
	Nothing,
	Wood,
	Stone,
	Dirt
}
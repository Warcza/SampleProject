﻿using System.Collections.Generic;

using UnityEngine;

public class CameraFollow : MonoBehaviour
{
	#region Variables
	[SerializeField] CameraScriptable camCharac;
	[SerializeField] Transform Target;

	Transform thisTrans;
	Transform camTrans;
	Vector3 velocity = Vector3.zero;

	bool FollowPlayer = true;
	#endregion

	#region Mono
	void Awake ( )
	{
		thisTrans = transform;
		camTrans = thisTrans.GetChild (0);
	}

	void FixedUpdate ( )
	{
		if (!FollowPlayer)
		{
			return;
		}

		Vector3 currTarg = Target.localPosition;
		currTarg += camCharac.MaxDefGap;

		thisTrans.localPosition = Vector3.SmoothDamp (thisTrans.localPosition, currTarg, ref velocity, camCharac.SmoothFollow * Time.deltaTime, camCharac.MaxSpeed);
	}
	#endregion

	#region Public Methodes
	#endregion

	#region Private Methodes
	#endregion
}
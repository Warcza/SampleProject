﻿using Unity.Entities;

using UnityEngine;

public class CharacterGravity : MonoBehaviour
{
	#region Variables
	public CharacterGravityScriptable thisCharaGrav;
	public bool OnGround;

	[HideInInspector] public Vector3 NormalGround;
	[HideInInspector] public float currFall = 0;
	[HideInInspector] public bool lastGround = false;
	[HideInInspector] public bool isActive = false;

	Transform thisTrans;
	Rigidbody thisRig;

	Vector2 rangeCurveFall;

	int nbrEnable = 0;
	#endregion

	#region Mono
	void Awake ( )
	{
		thisTrans = transform;
		thisRig = GetComponent<Rigidbody> ( );
		rangeCurveFall = new Vector2 (thisCharaGrav.curvefall.keys [0].time, thisCharaGrav.curvefall.keys [thisCharaGrav.curvefall.keys.Length - 1].time);
	}
	#endregion

	#region Public Methodes
	public void ResetGravity (bool enable)
	{
		bool checkReset = false;
		if (enable)
		{
			nbrEnable++;

			if (nbrEnable >= 0)
			{
				checkReset = true;
			}
		}
		else
		{
			thisRig.useGravity = false;
			thisRig.velocity = Vector3.zero;

			checkReset = true;
			nbrEnable--;
		}

		if (checkReset)
		{
			isActive = enable;
			currFall = 0;
		}
	}
	#endregion

	#region Private Methodes

	#endregion
}

class GravitySystem : ComponentSystem
{
	struct Components
	{
		public Transform ThisTrans;
		public Rigidbody ThisRig;
		public CharacterGravity ThisChara;
	}

	protected override void OnUpdate ( )
	{
		ComponentGroupArray<Components> getComps = GetEntities<Components> ( );
		if (getComps.Length == 0)
		{
			getComps.Dispose ( );
			return;
		}

		float getTime = Time.deltaTime;
		int lenght = getComps.Length;

		CharacterGravity getChara;
		Transform getTrans;
		Rigidbody getRig;
		for (int a = 0; a < lenght; a++)
		{
			getChara = getComps [a].ThisChara;
			if (!getChara.isActive)
			{
				continue;
			}

			getTrans = getComps [a].ThisTrans;
			getRig = getComps [a].ThisRig;

			RaycastHit hit;
			bool grounded = Physics.Raycast (getTrans.localPosition, -Vector3.up, out hit, 0.75f, 1 << LayerMask.NameToLayer ("Ground"));
			//GameManger.LAYER_GROUND);

			getChara.OnGround = grounded;
			Debug.DrawLine (getTrans.localPosition, getTrans.localPosition - Vector3.up, Color.red, 1);

			if (getChara.lastGround != grounded)
			{
				getChara.lastGround = grounded;
				if (grounded)
				{
					getChara.currFall = 0;
				}

				getRig.useGravity = grounded;
			}

			if (grounded)
			{
				getChara.NormalGround = hit.normal;
			}
			else
			{
				getChara.currFall += Time.fixedDeltaTime;

				getChara.NormalGround = Vector3.zero;
				getRig.velocity -= Vector3.up * Time.deltaTime * getChara.thisCharaGrav.curvefall.Evaluate (getChara.currFall) * getChara.thisCharaGrav.ForceFall;
			}
		}

		getComps.Dispose ( );
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;

public class RoomSpawner : MonoBehaviour
{
	#region Variables
	public RoomByType [ ] RoomSort;
	public ScriptLevelInfo [ ] RoomInfoSpawn;
	public Transform [ ] ParentsRoom;

	public Transform MainParent;
	public ScriptLevelInfo DefaultRIS;

	List<RoomControl> AllInstObj;
	List<GameObject> parentObj;

	#endregion

	#region Mono

	#endregion

	#region Public Methodes
	public void InitGameCont ( )
	{
		parentObj = new List<GameObject> ( );
		AllInstObj = new List<RoomControl> ( );
	}
	public void ActiveNewLevel (int thisLevel)
	{
		ParentsRoom [thisLevel].gameObject.SetActive (true);
	}

	public void DeActiveNewLevel (int thisLevel)
	{
		ParentsRoom [thisLevel].gameObject.SetActive (false);
	}

	public void DisplayAllRooms (bool displayRoom, float minDist, RoomControl thisRoom, float maxTime, bool randDisplay = true, bool deActivatedNear = true)
	{
		RoomControl [ ] getAllRC = AllInstObj.ToArray ( );
		RoomControl [ ] getNearRoom = thisRoom.NearRoom;
		RoomControl currRoomCont;
		Vector3 getPos = thisRoom.transform.position;

		int getSecond = (int)(maxTime * 1000);

		if (displayRoom)
		{
			for (int a = 0; a < ParentsRoom.Length; a++)
			{
				if (!ParentsRoom [a].gameObject.activeSelf)
				{
					ParentsRoom [a].gameObject.SetActive (true);
					parentObj.Add (ParentsRoom [a].gameObject);
				}
			}
		}
		else
		{
			for (int a = 0; a < parentObj.Count; a++)
			{
				parentObj [a].SetActive (false);
			}
			parentObj.Clear ( );
		}

		int b;
		bool checkRoom;

		for (int a = 0; a < getAllRC.Length; a++)
		{
			currRoomCont = getAllRC [a];
			checkRoom = true;
			for (b = 0; b < getNearRoom.Length; b++)
			{
				if (getNearRoom [b] == currRoomCont)
				{
					checkRoom = false;
					break;
				}
			}

			if (!checkRoom)
			{
				continue;
			}

			if (Vector3.Distance (currRoomCont.ThisTrans.position, getPos)> minDist)
			{
				if (randDisplay)
				{
					currRoomCont.RoomOutSide (displayRoom, getSecond);
				}
				else
				{
					currRoomCont.RoomOutSide (displayRoom, getSecond * a, false);
				}
			}
			else if (currRoomCont.gameObject == thisRoom.gameObject && deActivatedNear)
			{
				currRoomCont.OutSideDisplay (displayRoom);
			}
		}
	}

	public void SpawnRooms (RoomControl defTrans, int currentDepth = 0, int currentLevel = 0, int recurTime = 0)
	{

		List<RoomControl> instObj = AllInstObj;
		RoomByType [ ] getRoomSort = RoomSort;
		RoomByType getCurrRoomSort;

		ScriptLevelInfo [ ] getRoomInfoSpawn = RoomInfoSpawn;
		ScriptLevelInfo getCurrLevelInfo;

		List<Transform> exitUnUse = Manager.Garbage.ReInitList<Transform>(typeof(Transform));

		if (currentLevel >= getRoomSort.Length)
		{
			currentLevel = getRoomSort.Length - 1;
		}

		getCurrRoomSort = getRoomSort [currentLevel];

		if (getRoomInfoSpawn.Length > getCurrRoomSort.LevelRoom)
		{
			getCurrLevelInfo = getRoomInfoSpawn [getCurrRoomSort.LevelRoom];
		}
		else
		{
			getCurrLevelInfo = DefaultRIS;
		}

		recursiveSpawn (currentLevel, currentDepth, getCurrLevelInfo, defTrans, Manager.Garbage.ReInitList<RoomTypeSpawn>(typeof(RoomTypeSpawn)), exitUnUse, instObj, recurTime);
	}
	#endregion

	#region Private Methodes
	void recursiveSpawn (int currLevel, int currentDepth, ScriptLevelInfo currLevelInfo, RoomControl currRoomControl, List<RoomTypeSpawn> availableSpawn, List<Transform> exitUnUse, List<RoomControl> instObj, int recurTime = 0)
	{
		if (currentDepth++ < currLevelInfo.NbrDepthRoom)
		{
			RoomsInfo [ ] getListRoom = RoomSort [currLevel].RoomSort.ToArray ( );
			RoomTypeSpawn [ ] currRTS = currLevelInfo.RoomInfoSpawnable;

			List<RoomControl> AddNearRoom = Manager.Garbage.ReInitList<RoomControl>(typeof(RoomControl));
			List<RoomControl> lastRoomControl = Manager.Garbage.ReInitList<RoomControl>(typeof(RoomControl));
			lastRoomControl.Add (currRoomControl);

			RoomControl NewRoomC;
			RoomTypeSpawn thisRTS;

			GameObject newRoom;
			GameObject thisDepth = new GameObject ( );

			Transform getCurrRoom;
			Transform getParent = ParentsRoom [currLevel];

			currRoomControl.SpawnOther = true;

			if (getParent.childCount < currentDepth || getParent.GetChild (currentDepth - 1).name != currentDepth.ToString ( ))
			{
				thisDepth.name = currentDepth.ToString ( );
				thisDepth.transform.SetParent (getParent);
				getParent = thisDepth.transform;

				thisDepth = new GameObject ( );
				thisDepth.name = getParent.childCount.ToString ( );
				thisDepth.transform.SetParent (getParent);
				getParent = thisDepth.transform;
			}
			else
			{
				getParent = getParent.GetChild (currentDepth - 1);
				thisDepth.name = getParent.childCount.ToString ( );
				thisDepth.transform.SetParent (getParent);
				getParent = thisDepth.transform;
			}

			int cal;
			int calRslt;
			int b;

			for (int a = 0; a < currRoomControl.Exit.Length; a++)
			{
				availableSpawn.Clear ( );
				thisRTS = null;
				cal = 0;

				if (currRoomControl.Exit.Length > 1 && a > 0)
				{
					cal += currLevelInfo.MultipleDoorUnUse;
				}

				if (currRoomControl.ThisType == RoomType.Standard)
				{
					for (b = 0; b < currRTS.Length; b++)
					{
						if (currRTS [b].DepthStart < currentDepth)
						{
							availableSpawn.Add (currRTS [b]);
						}
					}

					for (b = 0; b < availableSpawn.Count; b++)
					{
						cal += availableSpawn [b].PourcentageSpawn;
					}

					calRslt = Random.Range (0, cal);
					cal = 0;

					for (b = 0; b < availableSpawn.Count; b++)
					{
						cal += availableSpawn [b].PourcentageSpawn;
						if (cal > calRslt)
						{
							thisRTS = availableSpawn [b];
							break;
						}
					}
				}
				else
				{
					for (b = 0; b < currRTS.Length; b++)
					{
						if (currRTS [b].ThisType == RoomType.Standard)
						{
							cal += currRTS [b].PourcentageSpawn;
							break;
						}
					}

					calRslt = Random.Range (0, cal);
					if (currRTS [b].PourcentageSpawn > calRslt)
					{
						thisRTS = currRTS [b];
					}
				}

				if (thisRTS != null)
				{
					for (b = 0; b < getListRoom.Length; b++)
					{
						if (getListRoom [b].ThisType == thisRTS.ThisType)
						{
							cal = Random.Range (0, getListRoom [b].Rooms.Count);
							calRslt = Random.Range (0, getListRoom [b].Rooms [cal].AllRoom.Length);

							newRoom = (GameObject)Instantiate (getListRoom [b].Rooms [cal].AllRoom [calRslt], getParent);

							getCurrRoom = currRoomControl.Exit [a].transform;
							newRoom.name = "Depth :" + currentDepth.ToString ( )+ " : " + getCurrRoom.name + " - " + a.ToString ( );

							NewRoomC = newRoom.GetComponent<RoomControl> ( );
							NewRoomC.NearRoom = lastRoomControl.ToArray ( );
							NewRoomC.ThisTrans = newRoom.transform;

							newRoom.transform.localPosition = Vector3.zero;
							newRoom.transform.rotation = getCurrRoom.rotation;
							newRoom.transform.localPosition = getCurrRoom.position - NewRoomC.Entry.position;

							NewRoomC.DepthLevel = currentDepth;
							NewRoomC.CurrentLevel = currLevel;

							instObj.Add (NewRoomC);
							AddNearRoom.Add (NewRoomC);

							if (recurTime > 0)
							{
								recursiveSpawn (currLevel, currentDepth, currLevelInfo, NewRoomC, availableSpawn, exitUnUse, instObj, recurTime - 1);
							}
							else
							{
								Manager.Garbage.AddGarbageClass(availableSpawn);
							}
							break;
						}
					}
				}
				else
				{
					exitUnUse.Add (currRoomControl.Exit [a]);
				}
			}

			AddNearRoom.AddRange (currRoomControl.NearRoom);
			currRoomControl.NearRoom = AddNearRoom.ToArray ( );
			
			Manager.Garbage.AddGarbageClass(AddNearRoom);
			Manager.Garbage.AddGarbageClass(lastRoomControl);
		}
		else
		{
			SpawnRooms (currRoomControl, 0, currLevel + 1, recurTime);
		}

	}
	#endregion
}
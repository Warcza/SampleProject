﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
	#region Variables
	public RoomSpawner getGM;

	public RoomControl ThisRC;

	[Header ("Default Value")]
	public float DistDisplay = 5;
	public float TimeMaxDisplay = 5;
	public bool RandomDisplayRoom = true;
	public bool DeactivateNearRoom = true;
	
	#if UNITY_EDITOR
	[Header ("Auto Move")]
	[SerializeField] bool autoChangeRoom;
	[SerializeField] float timeBetweenChange;
	[SerializeField] float timeDisplayAll;
	[SerializeField] int nbrToDisplayAll;

	IEnumerator saveAutoWait;

	int nbrDisplay = 0;
	bool checkAutoMove;
	#endif

	int getLevel = 0;
	int lastLvl = 0;
	#endregion

	#region Mono
	void Start ( )
	{		
		if (ThisRC == null)
		{
			ThisRC = FindObjectOfType<RoomControl> ( );

			if (ThisRC == null)
			{
				Debug.LogWarning ("LevelManager need at least one Room Control for start");
				return;
			}
		}

		getGM.InitGameCont ( );

		// pour démarrer le premier spawn dès l'initialisation
		ThisRC.CurrentLevel = 0;
		getGM.ActiveNewLevel (0);
		UpdateRoom (ThisRC);
		ThisRC.RoomActivate ( );

		#if UNITY_EDITOR
		checkAutoMove = autoChangeRoom;
		if(autoChangeRoom)
		{
			saveAutoWait = waitAutoMove();
			StartCoroutine(saveAutoWait);
		}
		#endif
	}

	#if UNITY_EDITOR
	void Update ()
	{
		if(checkAutoMove != autoChangeRoom)
		{
			checkAutoMove = autoChangeRoom;
			if (ThisRC != null)
			{
				if(checkAutoMove)
				{
					saveAutoWait = waitAutoMove();
					StartCoroutine(saveAutoWait);
				}
				else
				{
					DisplayRooms (false);
					StopCoroutine(saveAutoWait);
				}
			}
		}
	}
	#endif
	#endregion

	#region Public Methods
	public void CheckLevel (int thisLevel, bool newLevel)
	{
		if (newLevel && lastLvl != thisLevel)
		{
			getGM.ActiveNewLevel (thisLevel);
			getLevel = thisLevel;
		}
		else if (!newLevel && lastLvl != thisLevel)
		{
			getGM.DeActiveNewLevel (lastLvl);
			lastLvl = getLevel;
		}
	}

	public void UpdateRoom (RoomControl newRoom)
	{
		if (!newRoom.SpawnOther)
		{
			getGM.SpawnRooms (newRoom, newRoom.DepthLevel, newRoom.CurrentLevel, 0);
		}
		else if (ThisRC == newRoom)
		{
			return;
		}
		newRoom.RoomActivate ( );

		ThisRC.disableRoom(newRoom.gameObject);
		ThisRC = newRoom;
	}

	public void DisplayRooms (bool display, float distToDisplay = -1, float timeDispMax = -1, bool randDispInt = false, bool deActNear = false)
	{
		bool randDisp = RandomDisplayRoom;
		bool deANear = DeactivateNearRoom;

		if (distToDisplay == -1)
		{
			distToDisplay = DistDisplay;
		}

		if (timeDispMax == -1)
		{
			timeDispMax = TimeMaxDisplay;
		}

		//randDisp = randDispInt;

		//deANear = deActNear;

		getGM.DisplayAllRooms (display, distToDisplay, ThisRC, timeDispMax, randDisp, deANear);
	}
	#endregion

	#region Private Methods

	#if UNITY_EDITOR
	IEnumerator waitAutoMove()
	{
		yield return new WaitForSeconds(timeBetweenChange);

		nbrDisplay++;

		if(nbrDisplay != nbrToDisplayAll)
		{
			UpdateRoom(ThisRC.NearRoom[Random.Range(0,ThisRC.NearRoom.Length)]);
		}
		else
		{
			nbrDisplay = 0;
			DisplayRooms (true);
			yield return new WaitForSeconds(timeDisplayAll);
			DisplayRooms (false);
		}

		saveAutoWait = waitAutoMove();
		StartCoroutine(saveAutoWait);
	}
	#endif
	#endregion
}
﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;

public class RoomControl : MonoBehaviour
{
	#region Variables
	public GameObject InsideComp;
	public GameObject OutSideComp;
	public GameObject BothComp;

	public Transform Entry;
	public Transform [ ] Exit;
	[HideInInspector]
	public Transform ThisTrans;

	[Header ("Info 'Auto' Room")]
	public RoomControl [ ] NearRoom;
	public RoomType ThisType;

	[Header ("Debug Stuff")]
	public int DepthLevel = 0;
	public bool SpawnOther = false;
	public int CurrentLevel = 0;

	bool checkAsync = false;
	#endregion

	#region Mono
	#endregion

	#region Public Methodes
	public void RoomActivate ( )
	{		
		RoomControl [ ] getNearRoom = NearRoom;
		bool checkNew = false;
		int getNew = CurrentLevel;

		gameObject.SetActive (true);
		for (int a = 0; a < getNearRoom.Length; a++)
		{
			if (getNearRoom [a].CurrentLevel != CurrentLevel)
			{
				checkNew = true;
				getNew = getNearRoom [a].CurrentLevel;
			}
		}

		disableRoom(gameObject, true, true, false, true);

		Manager.Scene.LvlManager.CheckLevel (getNew, checkNew);
	}

	public void OutSideDisplay (bool display)
	{
		RoomControl [ ] getNearRoom = NearRoom;
		disableRoom(gameObject, true, false, true);
	}

	public void disableRoom ( GameObject ThisObj, bool roomActivated = false, bool inside = false, bool outside = false, bool both = false)
	{
		RoomControl [ ] getNearRoom = NearRoom;
		for (int a = 0; a < getNearRoom.Length; a++)
		{
			if (getNearRoom [a].gameObject != ThisObj)
			{
				getNearRoom [a].gameObject.SetActive (roomActivated);
				getNearRoom [a].InsideComp.SetActive (inside);
				getNearRoom [a].OutSideComp.SetActive (outside);
				getNearRoom [a].BothComp.SetActive (both);
			}
		}
	}

	//Disable or Enable the room with a delay 
	public async void RoomOutSide (bool activated, int maxTime, bool random = true)
	{
		if (checkAsync)
		{
			await Task.Delay (20);
			RoomOutSide (activated, maxTime, random);
			return;
		}

		checkAsync = true;

		if (random)
		{
			await Task.Delay (Random.Range (0, maxTime));
		}
		else
		{
			await Task.Delay (maxTime);
		}

		if (this != null)// vérification pour si jamais on arrete l'éditeur avant que les async soivent terminer
		{
			gameObject.SetActive (activated);
			OutSideComp.SetActive (activated);
			BothComp.SetActive(activated);
			checkAsync = false;
		}
	}
	#endregion

	#region Private Methodes
	#endregion
}